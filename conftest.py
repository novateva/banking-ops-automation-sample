import pytest  # type: ignore


# This allows to pass --use_xvfb=0 or 1 in pytest command.
# Apparently, it's documented in... stack overflow :upside-down-smiley:
def pytest_addoption(parser):
    """Define command-line options."""
    parser.addoption("--use_xvfb", action="store", default="1")
    parser.addoption("--pw_from_env", action="store", default="0")
