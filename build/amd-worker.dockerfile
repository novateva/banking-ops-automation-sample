FROM debian:buster

ENV TZ=America/Caracas
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt update -y && apt install -y python3-pip python3-dev \
            chromium chromium-driver \
            build-essential libssl-dev libffi-dev python-dev
RUN pip3 install --upgrade pip


COPY requirements.txt /tmp/requirements.txt
RUN pip3 install --no-cache-dir -r /tmp/requirements.txt


RUN groupadd -g 999 worker && \
    useradd -r -u 999 -g worker worker && \
    mkdir /home/worker && chown worker:worker /home/worker
USER worker

COPY --chown=worker:worker . /venebankservice

WORKDIR /venebankservice

CMD ["python3", "-m", "venebankservice.worker.main"]
