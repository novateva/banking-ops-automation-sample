FROM ubuntu:latest

# RUN echo Updating existing packages, installing and upgrading python and pip.
RUN apt-get update -y && apt-get install -y python3-pip python3-dev build-essential
RUN pip3 install --upgrade pip

# RUN echo Installing Python packages listed in requirements.txt
COPY requirements.txt /tmp/requirements.txt
RUN pip install --no-cache-dir -r /tmp/requirements.txt

WORKDIR /app

COPY . .

# RUN echo Starting python and starting the Flask service...
CMD ["gunicorn", "--bind", "0.0.0.0:5000", "venebankservice.master.main:create_app()", "--timeout", "180"]

# docker run -i -p 5000:5000 --env CRED_ENCR_PW=$CRED_ENCR_PW mesada-venebank-master:latest
