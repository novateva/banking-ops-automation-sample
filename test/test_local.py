import logging
from concurrent.futures import ThreadPoolExecutor, wait

from venebankservice.venebank import Bank, BankAccount
from venebankservice.venebank import xvfb_wrap, XvfbServerManager
from venebankservice.venebank import QueryInterval, Transaction
from venebankservice.venebank import Transfer, TransferSuccess, TransferError
from venebankservice.venebank.testing_recipients import (sara_mercantil, papa_banesco,
                                         augusto_pago_movil_mercantil,
                                         augusto_mercantil,
                                         marina_pago_movil_banesco)

import pytest  # type: ignore
import subprocess
import os
import time
from getpass import getpass

import datetime


def zenity_get_token():
    """Zenity dialog to input token, pytest redirects stdin/stdout."""
    my_env = os.environ.copy()
    my_env['DISPLAY'] = ":0"
    return subprocess.run(["zenity", "--entry", "--text=Token:"],
                          capture_output=True,
                          env=my_env,
                          text=True).stdout.strip()


def correct_query(res):
    def get_number(bal):
        return float(bal.replace('.', '').replace(',', '.'))

    (bal1, bal2, bal3), trans = res
    assert (get_number(bal1) >= 0)
    assert (get_number(bal2) >= 0)
    assert (get_number(bal3) >= 0)
    for t in trans:
        assert (isinstance(t.date, datetime.date))
        assert (t.description)
        assert (t.reference)
        assert (get_number(t.amount) >= 0)
        assert (t.direction in ['+', '-'])


def correct_transfer_result(res, ln):
    assert (len(res) == ln)
    for tr in res:
        assert (isinstance(tr, TransferSuccess))
        assert (tr.reference)


@pytest.fixture
def use_xvfb(pytestconfig):
    return (pytestconfig.getoption("use_xvfb") == "1")


@pytest.fixture(scope="session")
def pw(pytestconfig):
    pw_from_env = pytestconfig.getoption("pw_from_env") == "1"
    if pw_from_env:
        return os.environ['CRED_ENCR_PW']
    return getpass()


@pytest.mark.local_augusto
@pytest.mark.banesco
def test_banesco_query(use_xvfb, pw):
    b = BankAccount(Bank.BANESCO_PERSONAS,
                    'credentials/credentials_banesco.yaml.encr', pw)
    res = b.get_balance_and_transactions(QueryInterval.LAST_TWO_DAYS, use_xvfb,
                                         logging.DEBUG)
    correct_query(res)


@pytest.mark.local_augusto
@pytest.mark.mercantil
def test_mercantil_query(use_xvfb, pw):
    m = BankAccount(Bank.MERCANTIL_PERSONAS,
                    'credentials/credentials_mercantil.yaml.encr', pw)
    res = m.get_balance_and_transactions(QueryInterval.LAST_TWO_DAYS, use_xvfb,
                                         logging.DEBUG)
    correct_query(res)


@pytest.mark.local_augusto
@pytest.mark.slow
def test_bank_accounts(use_xvfb, pw):
    xvfb_man = XvfbServerManager()
    b = BankAccount(Bank.BANESCO_PERSONAS,
                    'credentials/credentials_banesco.yaml.encr', pw, xvfb_man)
    m = BankAccount(Bank.MERCANTIL_PERSONAS,
                    'credentials/credentials_mercantil.yaml.encr', pw,
                    xvfb_man)
    m2 = BankAccount(Bank.MERCANTIL_PERSONAS,
                     'credentials/credentials_sara_mercantil.yaml.encr', pw,
                     xvfb_man)

    def aux(account: BankAccount):
        return account.get_balance_and_transactions(
            QueryInterval.LAST_TWO_MONTHS, use_xvfb, logging.DEBUG)

    with ThreadPoolExecutor() as executor:
        merc_future = executor.submit(aux, m)
        merc2_future = executor.submit(aux, m2)
        bane_future = executor.submit(aux, b)
        wait([merc_future, merc2_future, bane_future])
        correct_query(merc_future.result())
        correct_query(merc2_future.result())
        correct_query(bane_future.result())


@pytest.mark.local_augusto
@pytest.mark.transfer
@pytest.mark.banesco
def test_full_one_banesco(use_xvfb, pw):
    b = BankAccount(Bank.BANESCO_PERSONAS,
                    'credentials/credentials_banesco.yaml.encr', pw)
    t = Transfer(amount='123,69', recipient=papa_banesco, concept='prueba1')
    t1 = Transfer(amount='123,12',
                  recipient=augusto_pago_movil_mercantil,
                  concept='prueba1')
    t2 = Transfer(amount='125,12',
                  recipient=augusto_pago_movil_mercantil,
                  concept='prueba2')
    t3 = Transfer(amount='125,12',
                  recipient=augusto_mercantil,
                  concept='prueba1')
    t4 = Transfer(amount='123,12',
                  recipient=augusto_mercantil,
                  concept='prueba2')

    with xvfb_wrap(b.xvfb_server_manager, use_xvfb), b.session() as session:
        # queryres1 = session.get_balance_and_transactions(
            # QueryInterval.LAST_TWO_DAYS)

        def get_token():
            if get_token.token is None:
                get_token.token = zenity_get_token()
            return get_token.token

        get_token.token = None

        tres1 = session.make_same_bank_transfer([t], get_token)
        tres2 = session.make_mobile_transfer([t1, t2], get_token)
        tres3 = session.make_other_bank_transfer([t3, t4], get_token)

        queryres2 = session.get_balance_and_transactions(
            QueryInterval.LAST_TWO_DAYS)

    # correct_query(queryres1)
    correct_query(queryres2)
    correct_transfer_result(tres1 + tres2 + tres3, 5)


@pytest.mark.local_augusto
@pytest.mark.transfer
@pytest.mark.mercantil
def test_full_one_mercantil(use_xvfb, pw):
    m = BankAccount(Bank.MERCANTIL_PERSONAS,
                    'credentials/credentials_mercantil.yaml.encr', pw)
    t1 = Transfer(amount='123,69', recipient=sara_mercantil, concept='prueba1')
    t2 = Transfer(amount='125,12', recipient=sara_mercantil, concept='prueba2')

    t3 = Transfer(amount='123,69',
                  recipient=marina_pago_movil_banesco,
                  concept='prueba1')
    t4 = Transfer(amount='125,12',
                  recipient=marina_pago_movil_banesco,
                  concept='prueba2')
    t5 = Transfer(amount='123,69', recipient=papa_banesco, concept='prueba1')
    t6 = Transfer(amount='125,12', recipient=papa_banesco, concept='prueba2')

    with xvfb_wrap(m.xvfb_server_manager, use_xvfb), m.session() as session:
        # queryres1 = session.get_balance_and_transactions(
            # QueryInterval.LAST_TWO_DAYS)

        def get_token():
            return zenity_get_token()

        tres1 = session.make_same_bank_transfer([t1, t2], get_token)
        tres2 = session.make_mobile_transfer([t3, t4], get_token)
        tres3 = session.make_other_bank_transfer([t5, t6], get_token)

        queryres2 = session.get_balance_and_transactions(
            QueryInterval.LAST_TWO_DAYS)

    # correct_query(queryres1)
    correct_query(queryres2)
    correct_transfer_result(tres1 + tres2 + tres3, 6)
