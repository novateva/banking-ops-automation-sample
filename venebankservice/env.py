"""Define environment variables used."""

import os

# MASTER AND WORKERS
DISCORD = os.environ.get('DISCORD_LOG', '0') == '1'
CRED_ENCR_PW = os.environ.get('CRED_ENCR_PW', None)
