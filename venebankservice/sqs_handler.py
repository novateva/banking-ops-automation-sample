import json
import time

import boto3

from typing import Optional, Callable


class SQSHandler(object):
    # TODO doc is outdated
    """Handle SQS message sending and receiving.

    The methods included in the class assume that you are only going to
    be sending/receiving one message at the time.

    If at any point there is more than on message and `receive_message`
    is called, the last received message will be returned and the rest will
    be deleted.

    Be aware that `receive_message` produces a `TimeoutError` if no messages
    are received before `receive_timeout` seconds have passed.

    Args
    ----
    queue_name: str
        Name of the queue to create/lookup
    queue_url: str
        Url of th queue to send/receive from
    receive_timeout: int
        Amount of seconds to wait for a message, default is 20

    Example
    -------
    sqshandler = SQSHandler('<queue_name>')
    msg = {'myCrucialData': 'eatcoconuts'}
    sqshandler.send_message(msg)

    try:
        received_msg = sqshandler.receive_message()
    except TimeoutError as err:
        fail_gracefully(err)  # Maybe retry?

    TODO
    ----
    Pretty much evrything you do with this class (even instantiating it)
    can raise a RuntimeError if a request fails, a retry mechanism should
    be implemented
    """
    AWS_ACCESS_KEY_ID = 'AKIAUV7OTHD5WDWCOY5G'
    AWS_SECRET_ACCESS_KEY = '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'

    def __init__(self,
                 queue_name: str = None,
                 queue_url: str = None,
                 receive_timeout: int = 20):

        if not (queue_name or queue_url):
            err_msg = 'You must provide either a queue_name or a queue_url'
            raise ValueError(err_msg)

        self.queue_name = queue_name
        self.queue_url = queue_url
        self.receive_timeout = receive_timeout

        # Creating a session should fix the errors caused by running several
        # threads. Althoug it makes it slower.
        session = boto3.session.Session()
        self.client = session.client(
            'sqs',
            region_name='us-east-1',
            aws_access_key_id=self.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=self.AWS_SECRET_ACCESS_KEY)
        """
        # client creation is not thread safe, sometimes this throws
        # KeyError: 'endpoint_resolver'
        # maybe using a lock, maybe using only one client...
        #
        while True:
            try:
                self.client = boto3.client(
                    'sqs',
                    region_name='us-east-1',
                    aws_access_key_id=self.AWS_ACCESS_KEY_ID,
                    aws_secret_access_key=self.AWS_SECRET_ACCESS_KEY)
                break
            except KeyError:
                pass
            """

        if queue_name and not queue_url:
            self.queue_url = self._get_queue_url()

    def _sqs_request(self, request_func, **kwargs):
        """Perform a request to the SQS API handling possible errors."""
        response = request_func(**kwargs)

        if response['ResponseMetadata']['HTTPStatusCode'] != 200:
            raise RuntimeError('Received non 200 HTTP status')

        return response

    def _get_queue_url(self):
        """Return the url of `self.queue_name` queue."""
        response = self._sqs_request(self.client.list_queues,
                                     QueueNamePrefix=self.queue_name)

        if 'QueueUrls' not in response:
            response = self._sqs_request(
                self.client.create_queue,
                QueueName=self.queue_name,
            )
            queue_url = response['QueueUrl']

        else:
            queue_url = response['QueueUrls'][0]

        return queue_url

    def _delete_messages(self, messages: list):
        """Delete all `messages` from defined queue."""
        delete_msg_entries = []

        for msg in messages:
            delete_msg_entries.append({
                'Id': msg['MessageId'],
                'ReceiptHandle': msg['ReceiptHandle']
            })

        self._sqs_request(self.client.delete_message_batch,
                          QueueUrl=self.queue_url,
                          Entries=delete_msg_entries)

    def receive_message(self) -> Optional[dict]:
        """Return last message extraced received from queue.

        The rest of received messages are deleted from the queue.
        """
        message = None

        t0 = time.time()
        while message is None:

            response = self._sqs_request(self.client.receive_message,
                                         QueueUrl=self.queue_url,
                                         MaxNumberOfMessages=10,
                                         WaitTimeSeconds=5)

            messages = response.get('Messages', [])

            if len(messages) > 0:
                message = json.loads(messages[-1]['Body'])
                self._delete_messages(messages)

            if (time.time() - t0) > self.receive_timeout:
                return message

        return message

    def receive_ONE_message(self) -> Optional[dict]:
        """Return last message extraced received from queue."""
        message = None

        t0 = time.time()
        while message is None:

            response = self._sqs_request(self.client.receive_message,
                                         QueueUrl=self.queue_url,
                                         MaxNumberOfMessages=1,
                                         WaitTimeSeconds=5)

            messages = response.get('Messages', [])

            if len(messages) > 0:
                message = json.loads(messages[0]['Body'])
                self._delete_messages([messages[0]])
                return message

            if (time.time() - t0) > self.receive_timeout:
                return message

        return message

    def receive_message_with_predicate(
            self, predicate: Callable[[dict], bool]) -> Optional[dict]:
        """Return a message that satisfies the given predicate.

        Return None if we don't get one before timeout.

        The queue is cleared.
        """
        message = None

        t0 = time.time()
        while message is None:
            response = self._sqs_request(self.client.receive_message,
                                         QueueUrl=self.queue_url,
                                         MaxNumberOfMessages=10,
                                         WaitTimeSeconds=5)

            messages = response.get('Messages', [])
            print(messages)

            if len(messages) > 0:
                for m in messages:
                    print(m['Body'])
                    body = json.loads(m['Body'])
                    if predicate(body):
                        message = body
                self._delete_messages(messages)
                if len(messages) < 10 and message is not None:
                    return message

            if (time.time() - t0) > self.receive_timeout:
                return message

        self.clear_queue()
        return message

    def clear_queue(self) -> None:
        """Clear all messages from the queue."""
        while True:
            response = self._sqs_request(self.client.receive_message,
                                         QueueUrl=self.queue_url,
                                         MaxNumberOfMessages=10,
                                         WaitTimeSeconds=5)
            messages = response.get('Messages', [])

            if len(messages) > 0:
                self._delete_messages(messages)
            else:
                break

    def send_message(self, message: dict):
        """Send `message` o the queue."""
        message_body = json.dumps(message)
        self._sqs_request(self.client.send_message,
                          QueueUrl=self.queue_url,
                          MessageBody=message_body)


if __name__ == '__main__':
    import random

    sqshandler = SQSHandler('otp-mx-movements')

    otp = ''.join(random.sample('0123456789', 10))
    sqshandler.send_message({'otp': otp})
    received_message = sqshandler.receive_message()
    received_otp = received_message['otp']

    print(otp, received_otp)
    print(otp == received_otp)
