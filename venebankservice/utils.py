"""General utilities for the service."""
import logging
import requests
from discord_webhook import DiscordWebhook

from .env import CRED_ENCR_PW, DISCORD
from cryptography.fernet import Fernet
from .venebank.credentials import _get_key_from_pw

################################################################################
# Cryptography
# TODO organize
################################################################################


def decrypt(encr: str) -> str:
    """Decrypt given string.

    Args:
        encr (str): String as base64 encrypted message.

    Returns:
        str: decrypted message.
    """
    pw = CRED_ENCR_PW
    assert (pw is not None)
    key = _get_key_from_pw(pw)
    fernet = Fernet(key)
    return fernet.decrypt(encr.encode()).decode()


################################################################################
# Discord stuff
################################################################################

_whurl_personal = (
    'https://discordapp.com/api/webhooks/739266256020045875/'
    'YApJ32Z2yXz5S6Ws1I9AzJh8Va_8XW_eweupovsyTKqd9rdLeWqI_X-amrKueC79tX18')

_whurl = (
    'https://discordapp.com/api/webhooks/752579404370018375/'
    '7HScGxsb-j8z5uZAPg4H-1vZH4wfegl7Vs-P1TweopGJ6NBkz4GVn-ERsJ6xKtAFR9Ku')


def send_discord_message(msg: str) -> None:
    """Send message to private server as a quick way to log."""
    try:
        webhook = DiscordWebhook(url=_whurl, content=msg[:1000])
        webhook.execute()
    except Exception:
        pass


class DiscordLoggingHandler(logging.Handler):
    """Log messages to discord."""

    def __init__(self):
        """Get Discord handler."""
        logging.Handler.__init__(self, logging.INFO)

    def emit(self, record):
        """Do whatever it takes to actually log the specified logging record."""
        if DISCORD:
            msg = self.format(record)
            send_discord_message(msg)


################################################################################


def add_console_handler(logger: logging.Logger):
    logger.setLevel(logging.DEBUG)
    c_handler = logging.StreamHandler()
    c_handler.setLevel(logging.DEBUG)
    c_format = logging.Formatter(
        '%(name)s [%(levelname)s] %(asctime)s: %(message)s', datefmt='%H:%M:%S')
    c_handler.setFormatter(c_format)
    logger.addHandler(c_handler)


def add_discord_handler(logger: logging.Logger):
    discord_handler = DiscordLoggingHandler()
    c_format = logging.Formatter(
        '%(name)s [%(levelname)s] %(asctime)s: %(message)s', datefmt='%H:%M:%S')
    discord_handler.setFormatter(c_format)
    logger.addHandler(discord_handler)
