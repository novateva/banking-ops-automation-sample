"""Logic for using backend's API."""
import requests

import json
import time
import logging
from typing import List, Optional, Tuple

from .venebank import (Transaction, TransferResult, TransferSuccess,
                       TransferError, TransferNotStarted, TransferNotCompleted)


def get_accounts():
    for tries in range(5):
        try:
            get_accounts_url = ('http://mesada-webapp-prod.eba-wvhnim3u.us-east'
                                '-1.elasticbeanstalk.com/account/')
            return requests.get(get_accounts_url).json()
        except Exception:
            if tries == 4:
                raise
            time.sleep(5)


def get_bank_and_device(
        account_id: int) -> Tuple[str, str, Optional[str], Optional[str]]:
    r = get_accounts()
    bank, bank_type, device_token, sms_hint = None, None, None, None
    for rbank, rdict in r.items():
        for pre_account in rdict['accounts']:
            if pre_account['id'] == account_id:
                bank = rbank
                bank_type = pre_account['account_type']
                device_token = pre_account['device']
                if pre_account['sms_hint'] == '\"\"':
                    pre_account['sms_hint'] = None
                sms_hint = pre_account['sms_hint']

    assert (bank is not None and bank_type is not None)

    if bank == 'MERCANTIL' and bank_type == 'JURIDICA':
        bank = 'BUSINESS_MERCANTIL'

    return bank, bank_type, device_token, sms_hint


def is_account_active(account_id: int, logger: logging.Logger = None) -> bool:
    """Get account is active or not.

    Args:
        account_id (int)

    Returns:
        bool
    """
    r = None
    try:
        is_active_url = ('http://mesada-webapp-prod.eba-wvhnim3u.us-east-1.'
                         f'elasticbeanstalk.com/account/active/{account_id}')
        r = requests.get(is_active_url)
        return r.json()['active']
    except json.decoder.JSONDecodeError:
        if logger is not None:
            logger.warning(f'active endpoint not working, returned {r}')
        return False


def _to_transfer_dict(trans_result: List[TransferResult], logger=None) -> dict:
    successful_transfers = []
    failed_transfers = []
    unknown_transfers = []
    for t in trans_result:
        if isinstance(t, TransferSuccess):
            successful_transfers.append({
                'id': t.id,
                'reference': t.reference,
                'screenshot': t.screenshot
            })
            if logger is not None:
                if t.screenshot is not None:
                    logger.debug(f'screenshot: {t.screenshot[:20]}')
                else:
                    logger.debug('screenshot is None in succesful transfer')

        elif isinstance(t, TransferError):
            failed_transfers.append({
                'id': t.id,
                'msg': t.error_msg,
                'screenshot': t.screenshot
            })
            if logger is not None:
                if t.screenshot is not None:
                    logger.debug(f'screenshot: {t.screenshot[:20]}')
                else:
                    logger.debug('screenshot is None in failed transfer')
        elif isinstance(t, TransferNotStarted):
            failed_transfers.append({
                'id': t.id,
                'msg': t.msg,
                'screenshot': None
            })
            if logger is not None:
                if t.screenshot is not None:
                    logger.debug(f'screenshot: {t.screenshot[:20]}')
                else:
                    logger.debug('screenshot is None in not started transfer')
        elif isinstance(t, TransferNotCompleted):
            unknown_transfers.append({
                'id': t.id,
                'msg': t.msg,
                'screenshot': None
            })
            if logger is not None:
                if t.screenshot is not None:
                    logger.debug(f'screenshot: {t.screenshot[:20]}')
                else:
                    logger.debug('screenshot is None in not completed transfer')
        else:
            # FIXME log some error
            pass

    return {
        'successful_transfers': successful_transfers,
        'failed_transfers': failed_transfers,
        'unknown_transfers': unknown_transfers
    }


def _ve_float(bs: str):
    """Convert venezuelan bank amount format to float.

    Args:
        bs (str): Venezuelan amount: 123.666,12

    Returns:
        [type]: float value
    """
    return float(bs.replace('.', '').replace(',', '.'))


def _to_query_dict(bals: Optional[Tuple[str, str, str]],
                   movs: Optional[List[Transaction]]) -> dict:
    bal, deferred, blocked = None, None, None
    if bals:
        bal, deferred, blocked = map(_ve_float, bals)

    def to_movement_dict(transaction: Transaction):
        return {
            'amount': _ve_float(transaction.amount),
            'direction': transaction.direction,
            'reference': transaction.reference,
            'description': transaction.description,
            'date': transaction.date.strftime('%d/%m/%Y')
        }

    movements = []
    if movs:
        movements = list(map(to_movement_dict, movs))
    return {
        'balance': bal,
        'deferred': deferred,
        'blocked': blocked,
        'movements': movements
    }


def send_results(id: int,
                 bals: Optional[Tuple[str, str, str]],
                 movs: Optional[List[Transaction]],
                 trans_result: List[TransferResult],
                 logger=None):
    """Send results to backend.

    Args:
        id (int): bank account id
        bals (Optional[Tuple[str, str, str]])
        movs (Optional[List[Transaction]])
        trans_result (List[TransferResult])
    """
    url = ('http://mesada-webapp-prod.eba-wvhnim3u.us-east-1.'
           'elasticbeanstalk.com/movements/load_transfers_and_movements')

    query_dict = _to_query_dict(bals, movs)
    transfer_dict = _to_transfer_dict(trans_result, logger)
    results = {'account_id': id, **query_dict, **transfer_dict}

    suc = results['successful_transfers']
    fai = results['failed_transfers']
    unk = results['unknown_transfers']

    for tries in range(5):
        try:
            r = requests.post(url, json=results)
            break
        except Exception:
            if tries == 4:
                raise
            time.sleep(5)

    ln = None
    if movs is not None:
        ln = len(movs)
    if logger is not None:
        logger.info(f'{bals} - {ln} movements\n{r}')
        logger.info(f'successful_transfers: {len(suc)}\n'
                    f'failed_transfers: {len(fai)}\n'
                    f'unknown_transfers: {len(unk)}')
