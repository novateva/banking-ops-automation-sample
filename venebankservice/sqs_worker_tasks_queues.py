from enum import Enum, auto
from typing import List, Optional

from .venebank import Transfer
from .sqs_handler import SQSHandler


def get_queue(worker_name: str):
    """Return SQSHandler to handle the task queue of the given worker."""
    return SQSHandler(f'worker-{worker_name}', receive_timeout=10)


class TaskType(Enum):
    Query = auto()
    Transfers = auto()


class Task():
    task_id: str
    account_id: int
    task_type: TaskType
    trasfers: Optional[List[Transfer]]

    def __init__(self,
                 task_id: str,
                 account_id: int,
                 task_type: TaskType,
                 transfers: List[Transfer] = None):
        self.task_id = task_id
        self.account_id = account_id
        self.task_type = task_type
        assert ((task_type == TaskType.Query) == (transfers is None))
        self.transfers = transfers

    def __repr__(self):
        return f'Task(account={self.account_id}, type={self.task_type})'


def send_transfers(queue: SQSHandler, account_id: int,
                   transfers: List[Transfer], task_id: str) -> None:
    queue.send_message({
        'task_id': task_id,
        'account_id': account_id,
        'task_type': TaskType.Transfers.name,
        'transfers': list(map(lambda t: t.to_JSON(), transfers))
    })


def send_query(queue: SQSHandler, account_id: int, task_id: str) -> None:
    queue.send_message({
        'task_id': task_id,
        'account_id': account_id,
        'task_type': TaskType.Query.name,
    })


def get_task(queue: SQSHandler) -> Optional[Task]:
    msg = queue.receive_ONE_message()
    if msg is None:
        return None
    task_type = TaskType[msg['task_type']]
    if task_type == TaskType.Query:
        return Task(msg['task_id'], msg['account_id'], task_type)
    else:
        return Task(msg['task_id'], msg['account_id'], task_type,
                    list(map(Transfer.from_JSON, msg['transfers'])))
