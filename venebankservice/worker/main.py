import time
import json
from typing import List
from venebankservice.venebank.query import QueryInterval
import requests
import threading
import logging

from .. import sqs_worker_tasks_queues as worker_queues
from .env import MASTER_URL, WORKER_NAME, WORKER_AUTH_PASSWORD
from .. import utils
from ..utils import decrypt
from ..venebank import Credentials, Bank, BankAccount
from ..sqs_worker_tasks_queues import TaskType, Task
from .. import backend_api

from ..otp_interaction import check_ack, get_get_otp
from ..backend_status import report_status
from ..venebank.status import ACKStatus, Status


def register_to_master() -> None:
    url = MASTER_URL + '/workers'
    auth = (WORKER_NAME, WORKER_AUTH_PASSWORD)
    logger = logging.getLogger(__name__).getChild(WORKER_NAME)
    tries = 0
    while True:
        try:
            data = {'name': WORKER_NAME}
            r = requests.post(url, json=data, auth=auth)
            if r.status_code != 200:
                if tries % 15 == 0:
                    logger.warning(
                        f'Master seems to be offline ({r.status_code})')
                tries += 1
            else:
                if tries > 0:
                    logger.info('Master is back online')
                logger.debug(f'Master returned {r.status_code}')
                tries = 0
        except requests.exceptions.ConnectionError as e:
            logger.error(f'Exception on post to master: {e}')
        except Exception:
            logger.exception('Unhandled exception on post to master')
        time.sleep(20)


def _get_credentials(id: int, bank: str) -> Credentials:
    logger = logging.getLogger(__name__).getChild(WORKER_NAME)
    logger.debug(f'getting {id} credentials')
    get_cred_url = ('http://mesada-webapp-prod.eba-wvhnim3u.us-east-1.'
                    f'elasticbeanstalk.com/account/credentials/{id}')
    r = requests.get(get_cred_url)
    json_string = r.text
    d = json.loads(json_string)
    d['uid'] = decrypt(d.pop('username'))
    d['pw'] = decrypt(d.pop('password'))
    d['answers'] = d.pop('questions')
    if (d['mobile_pw'] is not None):
        d['mobile_pw'] = decrypt(d['mobile_pw'])

    def to_answer(d):
        p = (decrypt(d['question']), decrypt(d['answer']))
        return p

    d['answers'] = list(map(to_answer, d['answers']))
    c = Credentials(**d)
    return c


def post_task_done_to_master(task_id: str) -> None:
    url = MASTER_URL + '/workers/task'
    auth = (WORKER_NAME, WORKER_AUTH_PASSWORD)
    logger = logging.getLogger(__name__).getChild(WORKER_NAME)
    for tries in range(4):
        try:
            data = {'name': WORKER_NAME, 'task_id': task_id}
            r = requests.post(url, json=data, auth=auth)
            logger.debug(f'post finish task to master {r}')
            if r.status_code != 200:
                logger.error(
                    f'Error on post finished task to master ({r.status_code})')
            break
        except requests.exceptions.ConnectionError as e:
            logger.error(f'Exception on post finished task to master: {e}')
        except Exception:
            logger.exception('Unhandled exception on post finished'
                             'task to master')
        time.sleep(5)


def execute_task(task: Task, logger: logging.Logger) -> None:
    logger = logger.getChild(str(task.account_id))
    (bank, bank_type, device_token,
     sms_hint) = backend_api.get_bank_and_device(task.account_id)
    if bank == 'MERCANTIL' and bank_type == 'JURIDICA':
        bank = 'BUSINESS_MERCANTIL'
    credentials = _get_credentials(task.account_id, bank)
    if 'BANESCO' in bank:
        bank_account = BankAccount(Bank.BANESCO_PERSONAS, credentials)
    elif 'BUSINESS_MERCANTIL' == bank:
        bank_account = BankAccount(Bank.MERCANTIL_JURIDICO, credentials)
    elif 'MERCANTIL' == bank:
        bank_account = BankAccount(Bank.MERCANTIL_PERSONAS, credentials)
    elif 'VENEZUELA' in bank:
        bank_account = BankAccount(Bank.BDV_PERSONAS, credentials)
    elif 'BBVA_PROVINCIAL' in bank:
        bank_account = BankAccount(Bank.PROVINCIAL_JURIDICO, credentials)
    else:
        raise ValueError('Bank not recognized')

    # TODO: this is ugly
    def report_status_log(ts: List[str], s: Status) -> None:
        report_status(ts, s, logger)

    get_otp = get_get_otp(task.account_id, bank, device_token, sms_hint, logger)
    if task.task_type == TaskType.Query:
        bal, movs = bank_account.get_balance_and_transactions(
            get_otp, report_status_log, QueryInterval.LAST_TWO_DAYS)
        backend_api.send_results(task.account_id, bal, movs, [], logger)
    else:
        assert (device_token is not None and task.transfers is not None)

        logger.debug('going to report ack status')
        # pylint: disable=too-many-function-args
        report_status_log(list(map(lambda t: t.id, task.transfers)),
                          ACKStatus(1, WORKER_NAME))
        # pylint: enable=too-many-function-args

        check_ack(task.account_id, bank, device_token, sms_hint, logger)
        trans_res, (bal, movs) = bank_account.get_transfers_and_movements(
            task.transfers, get_otp, report_status_log)
        backend_api.send_results(task.account_id, bal, movs, trans_res, logger)
    post_task_done_to_master(task.task_id)


def main():
    register_to_master_thread = threading.Thread(target=register_to_master)
    register_to_master_thread.start()

    logger = logging.getLogger(__name__).getChild(WORKER_NAME)
    if not logger.hasHandlers():
        utils.add_console_handler(logger)
        utils.add_discord_handler(logger)
        logger.info(f'set {__name__} logger.')

    # start work loop
    queue = worker_queues.get_queue(WORKER_NAME)
    logger.info('Start to wait for tasks')
    while True:
        try:
            task = worker_queues.get_task(queue)
            if task is None:
                logger.debug('no task')
                time.sleep(15)
                continue

            logger.info(task)
            task_thread = threading.Thread(target=execute_task,
                                           args=[task, logger])
            task_thread.start()
        except Exception:
            logger.exception('unhandled exception in worker task look')


if __name__ == '__main__':
    main()
