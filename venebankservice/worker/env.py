"""Define environment variables used."""

import os

# WORKER
MASTER_URL = os.environ['MASTER_URL']
WORKER_NAME = os.environ['WORKER_NAME']
WORKER_AUTH_PASSWORD = os.environ['WORKER_AUTH_PASSWORD']