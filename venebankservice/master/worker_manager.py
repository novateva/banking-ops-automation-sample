"""Manage alive workers, and assign tasks to them."""

from datetime import datetime, timedelta, timezone
from dataclasses import dataclass, field
from typing import Dict, List, Tuple, Set
import time
import random
from threading import Lock
import logging
from functools import wraps

from flask import Flask, request
from flask_restful import Api, Resource, reqparse, abort

from .. import sqs_worker_tasks_queues as worker_queues
from ..venebank.transfer import Transfer

from .env import WORKERS_AUTH_PASSWORDS


@dataclass
class Worker:
    """Worker."""

    name: str


WORKER_TIMEOUT = 30


@dataclass
class State:
    """State of a worker."""

    last_heart_beat: datetime = field(
        default_factory=lambda: datetime.now(timezone.utc))
    tasks: Set[str] = field(default_factory=set)

    def alive(self) -> bool:
        return datetime.now(timezone.utc) - self.last_heart_beat < timedelta(
            seconds=WORKER_TIMEOUT)

    def upd(self):
        """Update the current state with the given information."""
        self.last_heart_beat = datetime.now(timezone.utc)


@dataclass
class WorkersState():
    """Mantain state of every known worker.

    """

    _state: Dict[str, State] = field(default_factory=lambda: {})
    lock: Lock = Lock()

    def update(self, worker_name: str):
        """Update the current state of the worker with the given information."""
        with self.lock:
            if self._state.get(worker_name) is None:
                self._state[worker_name] = State()

            self._state[worker_name].upd()

    def get_alive_snapshot(self):
        """Get current state of workers."""
        nret = {}
        with self.lock:
            for worker, state in self._state.items():
                if state.alive():
                    nret[worker] = state
        return nret

    def get_snapshot_dict(self):
        """Get current state of workers."""
        ret = {}
        with self.lock:
            for worker, state in self._state.items():
                ret[worker] = {
                    'alive':
                        state.alive(),
                    'last_heart_beat':
                        (state.last_heart_beat -
                         timedelta(hours=4)).strftime("%H:%M:%S"),
                    'tasks':
                        str(state.tasks)
                }

        return ret


def assign_task_and_wait(account_id: int, transfers: List[Transfer],
                         workers_state: WorkersState, logger: logging.Logger):
    logger = logger.getChild('worker_manager')
    while len(workers_state.get_alive_snapshot()) == 0:
        print("no worker, waiting for one")
        time.sleep(10)

    task_id = str(random.randrange(100000)) + str(datetime.today().timestamp())
    best_worker = None
    best_worker_state = None
    develop_alive = False
    tries = 0
    while True:
        with workers_state.lock:
            for worker, state in workers_state._state.items():
                if not state.alive():
                    continue
                if worker == 'develop':
                    develop_alive = True
                if best_worker is None:
                    best_worker = worker
                    best_worker_state = state
                elif len(state.tasks) < len(best_worker_state.tasks):
                    best_worker = worker
                    best_worker_state = state
            if best_worker is not None:
                break

        if tries % 10 == 0:
            logger.warning("No worker alive, waiting for one")
            tries += 1
        time.sleep(10)

    if develop_alive:
        best_worker = 'develop'

    workers_state._state[best_worker].tasks.add(task_id)

    logger.info(f'Sending task ({task_id}) to {best_worker}')
    queue = worker_queues.get_queue(best_worker)
    if transfers == []:
        worker_queues.send_query(queue, account_id, task_id)
    else:
        worker_queues.send_transfers(queue, account_id, transfers, task_id)

    TASK_TIMEOUT = timedelta(minutes=12)
    start_time = datetime.now()
    while True:
        # check if still task is running

        with workers_state.lock:
            if task_id not in workers_state._state[best_worker].tasks:
                break

        if datetime.now() - start_time > TASK_TIMEOUT:
            logger.error(f'task {task_id} timeout')
            with workers_state.lock:
                if task_id in workers_state._state[best_worker].tasks:
                    workers_state._state[best_worker].tasks.remove(task_id)
                break

        time.sleep(10)


def requires_auth(f):
    """Decorate a function so it requires authentication."""

    @wraps(f)
    def decorated(*args, **kwargs) -> Tuple[str, int]:
        auth = request.authorization
        if not auth:
            abort(401)
        auth_ok = auth.password == WORKERS_AUTH_PASSWORDS[auth.username]
        if not auth_ok:
            abort(401)
        return f(*args, **kwargs)

    return decorated


class WorkerEndpoint(Resource):
    workers_state: WorkersState

    def __init__(self, workers_state):
        self.workers_state = workers_state

    def get(self):
        return str(self.workers_state.get_snapshot_dict()), 200

    @requires_auth
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str)
        args = parser.parse_args()
        self.workers_state.update(args['name'])
        return None, 200


class WorkerTaskEndpoint(Resource):
    workers_state: WorkersState

    def __init__(self, workers_state):
        self.workers_state = workers_state

    @requires_auth
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str)
        parser.add_argument('task_id', type=str)
        args = parser.parse_args()
        name = args['name']
        task_id = args['task_id']
        with self.workers_state.lock:
            if task_id in self.workers_state._state[name].tasks:
                self.workers_state._state[name].tasks.remove(task_id)

        return None, 200


def get_worker_manager_api_app() -> Tuple[WorkersState, Flask]:
    workers_state = WorkersState()
    app = Flask(__name__)
    api = Api(app)
    api.add_resource(WorkerEndpoint,
                     '/workers',
                     resource_class_args=[workers_state])
    api.add_resource(WorkerTaskEndpoint,
                     '/workers/task',
                     resource_class_args=[workers_state])
    return workers_state, app
