"""Module for communication with command queues.

Get the commands from backend.
"""

from typing import Optional, List
import json

from ..venebank import Transfer
from ..venebank import correct_amount_format
from ..sqs_handler import SQSHandler


def get_queue(bank: str, id: int) -> SQSHandler:
    """Return SQSHandler to handle the command queue of the given account."""
    return SQSHandler(f'{bank}-{id}', receive_timeout=10)


def _get_transfers_from_msg(msg) -> List[Transfer]:
    trans = msg['transactions']
    ret = []
    for t in trans:
        # TODO: we can build Transfers directly, and not use from_JSON
        t['id'] = t.pop('transfer_id')
        amount = t['amount']
        t['amount'] = f'{amount:.2f}'.replace('.', ',')
        assert (correct_amount_format(t['amount']))
        t['recipient']['bank'] = 'B' + t['recipient']['bank']
        t['recipient'] = json.dumps(t['recipient'])
        ret.append(Transfer.from_JSON(json.dumps(t)))
    return ret


def get_transfers(transfer_queue: SQSHandler) -> Optional[List[Transfer]]:
    """Get transfers to do from a SQSHandler."""
    transfers_msg = transfer_queue.receive_ONE_message()
    if transfers_msg is None:
        return None
    return _get_transfers_from_msg(transfers_msg)
