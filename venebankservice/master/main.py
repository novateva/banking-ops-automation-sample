"""Entry point of the main service, see create_app()."""

from flask import Flask

from .worker_manager import get_worker_manager_api_app
from . import account_watcher


def create_app() -> Flask:
    """Entry point of the master service.

    Returns:
        Flask: Flask app.
    """
    workers_state, app = get_worker_manager_api_app()
    account_watcher.main(workers_state)

    return app


# if __name__ == '__main__':
#     app = create_app()
#     app.run()
#
