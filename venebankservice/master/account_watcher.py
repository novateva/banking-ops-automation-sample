"""Logic for watching accounts.

Get the commands to be done in each account, and send the tasks to assignment.
Schedule periodic querires.
"""

from dataclasses import dataclass
from datetime import datetime, timedelta
import logging
import time
import threading
from typing import Optional, List, Tuple

from . import sqs_command_queues as command_queues
from .worker_manager import WorkersState, assign_task_and_wait

from .. import utils, backend_api
from ..backend_status import report_status
from ..otp_interaction import check_ack
from ..venebank import (Transfer, TransferError, TransferResult)
from ..venebank.status import ACKStatus, ACKSuccess

################################################################################

START_WITH_QUERY = True
QUERY_INTERVAL = 35


@dataclass
class Account:
    """Account that the service watches for commands."""

    bank: str
    id: int
    username: str
    sms_hint: Optional[str]
    device_token: Optional[str] = None

    def get_logger(self):
        """Get account logger."""
        return logging.getLogger(
            __name__.split('.')[-1]).getChild(f'{self.username}_{self.id}')


def _need_otp(transfers: List[Transfer]) -> bool:
    """Check if we might need a otp.

    Based on the bank and the type of transfers, we can tell if we might need
    otp or not.
    """
    # TODO
    return len(transfers) > 0


def _check_device(account: Account,
                  transfers: List[Transfer]) -> Tuple[bool, str]:

    def is_all_cancelled(stats):
        cancelled = True
        for t in stats:
            cancelled = cancelled and (t == 'Cancelada')
        return cancelled

    (account.bank, _, account.device_token,
     account.sms_hint) = backend_api.get_bank_and_device(account.id)
    if account.device_token is None:
        return False, 'El dispositivo no está asociado.'

    alogger = account.get_logger()
    alogger.info('check ack with device')

    tries, ack_good, msg = 0, True, ''
    # pylint: disable=too-many-function-args
    # pylint bugged? ACKStatus complains for 2 arguments...
    s = report_status(list(map(lambda t: t.id, transfers)),
                      ACKStatus(1, 'el maestro'))
    if is_all_cancelled(s):
        return False, 'Operación cancelada.'

    while not check_ack(account.id, account.bank, account.device_token,
                        account.sms_hint, alogger):
        alogger.info('ack failed')
        if tries < 3:
            tries += 1
            alogger.info('retry ack')
            s = report_status(list(map(lambda t: t.id, transfers)),
                              ACKStatus(1 + tries, 'el maestro'))
            if is_all_cancelled(s):
                return False, 'Operación cancelada.'
        else:
            ack_good = False
            msg = 'No hay comunicacion con el dispositivo.'
            break

    if ack_good:
        s = report_status(list(map(lambda t: t.id, transfers)),
                          ACKSuccess('el maestro'))
        if is_all_cancelled(s):
            return False, 'Operación cancelada.'

    return ack_good, msg
    # pylint: enable=too-many-function-args


def _catch_soon_error(account: Account, transfers: List[Transfer],
                      error_msg: str) -> None:
    trans_result: List[TransferResult] = [
        TransferError(None, t.id, error_msg) for t in transfers
    ]
    account.get_logger().info(
        f'Error before sending task to worker: {error_msg}')
    backend_api.send_results(account.id, None, None, trans_result,
                             account.get_logger())


def watch_account(account: Account, workers_state: WorkersState):
    """Thread that takes care of one account."""
    alogger = account.get_logger()

    alogger.debug('start to watch account')

    command_queue = command_queues.get_queue(account.bank, account.id)
    alogger.debug('got transfer sqs handler')

    last_query = datetime.now()
    if START_WITH_QUERY:
        last_query = datetime.now() - timedelta(minutes=QUERY_INTERVAL)

    alogger.debug('start loop')
    active = False
    while True:
        try:
            if not backend_api.is_account_active(account.id):
                if active:
                    alogger.info('account got DEactivated. '
                                 'Going to check every 15 sec for change.')
                active = False
                time.sleep(15)
                continue
            if not active:
                alogger.info('account got activated.')
                active = True

            alogger.debug(f'{account.username} - waiting for command')

            ask_query = False
            try:
                transfers = command_queues.get_transfers(command_queue)
                if transfers is None:
                    transfers = []
                elif len(transfers) == 0:
                    ask_query = True
            except Exception:
                alogger.exception('couldn\'t get command message from sqs')
                continue

            if len(transfers) > 0:
                alogger.info(f'received {len(transfers)} transfers')
                alogger.debug(transfers)

                if _need_otp(transfers):
                    ack_good, error_msg = _check_device(account, transfers)
                    if not ack_good:
                        _catch_soon_error(account, transfers, error_msg)
                        continue

                # send task
                alogger.info('doing transfers')
                assign_task_and_wait(account.id, transfers, workers_state,
                                     alogger)

                last_query = datetime.now()

            if ask_query or datetime.now() - last_query >= timedelta(
                    minutes=QUERY_INTERVAL):
                # send task
                if ask_query:
                    alogger.info('asked forced consulta')
                else:
                    alogger.info('time to consulta')
                assign_task_and_wait(account.id, [], workers_state, alogger)

                last_query = datetime.now()

        except Exception:
            alogger.exception('Unhandled exception on account watcher thread.')
            continue


################################################################################


def _get_accounts() -> List[Account]:
    # TODO should be moved to backend_api
    logger = logging.getLogger(__name__.split('.')[-1])
    logger.debug('getting accounts')

    SUPPORTED_BANKS = ['BANESCO', 'MERCANTIL', 'VENEZUELA', 'BBVA_PROVINCIAL']
    r = backend_api.get_accounts()
    accounts: List[Account] = []
    for bank, d in r.items():
        if bank not in SUPPORTED_BANKS:
            continue

        for pre_account in d['accounts']:
            pre_account['bank'] = bank
            pre_account['device_token'] = pre_account.pop('device')
            pre_account['username'] = utils.decrypt(pre_account['username'])
            if pre_account['sms_hint'] == '\"\"':
                pre_account['sms_hint'] = None
            pre_account.pop('account_owner')
            pre_account.pop('account_type')
            accounts.append(Account(**pre_account))
    return accounts


def main(workers_state: WorkersState) -> None:
    """Get functions to watch and start watching them.

    TODO Consume the endpoint periodically and take proper actions
        - Account added/erased/modified
    """
    logger = logging.getLogger(__name__.split('.')[-1])
    if not logger.hasHandlers():
        utils.add_console_handler(logger)
        utils.add_discord_handler(logger)
        logger.info(f'set {__name__} logger.')

    accounts: List[Account] = _get_accounts()
    usernames = list(map(lambda a: f'{a.username}_{a.id}', accounts))
    logger.info(f'got {len(accounts)} accounts: {usernames}')

    # NOTE because of ~reasons~ ThreadPoolExecutor won't work when run gunicorn.
    # vanilla threads seem to work fine.
    for account in accounts:
        t = threading.Thread(target=watch_account,
                             args=[account, workers_state])
        t.start()
