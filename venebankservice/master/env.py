"""Environment variables for master."""

import json
import os

WORKERS_AUTH_PASSWORDS = json.loads(os.environ['WORKERS_AUTH_PASSWORDS'])
