"""Class to represent and handle credentials.

And functinality to encrypt/decrypt those credentials
"""

from dataclasses import dataclass
import yaml
import json
from typing import List, Tuple, Optional

from getpass import getpass
from cryptography.fernet import Fernet

import base64
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC

from .utils import hide


@dataclass
class Credentials:
    """A movement in one of the managed accounts."""

    uid: str
    pw: str
    answers: List[Tuple[str, str]]
    mobile_pw: Optional[str] = None  # special password used in mercantil

    def to_JSON(self) -> str:
        """Dump instance into json."""
        return json.dumps(self.__dict__)

    def __repr__(self) -> str:
        ans = list(map(lambda qa: (qa[0], '***'), self.answers))
        return "Credentials(uid=%r, pw='***', answers=%s)" % (hide(
            self.uid), ans)

    @classmethod
    def _from_dict(cls, loaded) -> "Credentials":
        uid = loaded['uid']
        pw = loaded['pw']
        answers = []
        for q, a in loaded['answers']:
            answers.append((q, a))
        mobile_pw = loaded.get('mobile_pw')
        return Credentials(uid, pw, answers, mobile_pw)

    @classmethod
    def from_YAML_file(cls, yaml_file: str) -> "Credentials":
        """Load a yaml file."""
        with open(yaml_file, 'r') as f:
            loaded = yaml.safe_load(f)
            return cls._from_dict(loaded)

    @classmethod
    def from_encrypted_YAML_file(cls,
                                 encr_file: str,
                                 pw: str = None) -> "Credentials":
        data = _decrypt_file(encr_file, pw)
        loaded = yaml.safe_load(data)
        return cls._from_dict(loaded)

    @classmethod
    def from_JSON(cls, json_string: str) -> "Credentials":
        """Create instance from json."""
        c = Credentials(**json.loads(json_string))
        c.answers = [(q, a) for (q, a) in c.answers]  # lists to tuples
        return c


def _get_key_from_pw(pw: str) -> bytes:
    password = pw.encode()
    salt = b'e3PVPgf5wcpWcZkuXrOHLQ=='  # base 64 so my editor won't break xD
    kdf = PBKDF2HMAC(algorithm=hashes.SHA256(),
                     length=32,
                     salt=base64.b64decode(salt),
                     iterations=100000,
                     backend=default_backend())
    return base64.urlsafe_b64encode(kdf.derive(password))


def _decrypt_file(encr_file: str, pw: str = None) -> bytes:
    if pw is None:
        pw = getpass()
    with open(encr_file, 'rb') as f:
        data = f.read()
    key = _get_key_from_pw(pw)
    fernet = Fernet(key)
    return fernet.decrypt(data)


def _ask_pw():
    pw = getpass()
    pwc = getpass('Confirm Password: ')
    if pw != pwc:
        raise Exception('Password and confirmation are different.')
    return pw


def encrypt_file(yaml_file: str, pw: str = None) -> None:
    with open(yaml_file, 'rb') as f:
        data: bytes = f.read()

    inter = (pw is None)
    if pw is None:
        pw = _ask_pw()
    key = _get_key_from_pw(pw)
    fernet = Fernet(key)
    encrypted = fernet.encrypt(data)
    encr_file = yaml_file + '.encr'
    # TODO check if file is already there
    with open(encr_file, 'wb') as f:
        f.write(encrypted)

    if _decrypt_file(encr_file, pw) != data:
        raise Exception(
            ('Decription of encrypted file is not the same as original.'
             'Possible bug'))

    if inter:
        print('Encrypted file saved: {}'.format(encr_file))

    # TODO erase the original?


def decrypt_file(encr_file: str, destination_file: str, pw: str = None) -> None:
    decrypted = _decrypt_file(encr_file, pw)
    # TODO check if file is already there
    with open(destination_file, 'wb') as f:
        f.write(decrypted)
    if pw is None:
        print('File was decrypted and saved as {}'.format(destination_file))
