"""Classes to represent transfers to be done and their results."""
from dataclasses import dataclass
from enum import Enum, auto
from typing import Optional, Union
from abc import ABC

import json

from .utils import hide


class DestBank(Enum):
    """Different possible destination banks."""

    B0001 = 'BANCO CENTRAL DE VENEZUELA'
    B0007 = 'BANCO BICENTENARIO'
    B0102 = 'BANCO DE VENEZUELA'
    B0104 = 'BANCO VENEZOLANO DE CREDITO'
    B0105 = 'BANCO MERCANTIL'
    B0108 = 'BANCO PROVINCIAL BBVA'
    B0114 = 'BANCARIBE'
    B0115 = 'BANCO EXTERIOR'
    B0116 = 'BANCO OCCIDENTAL DE DESCUENTO'
    B0128 = 'BANCO CARONI'
    B0134 = 'BANESCO'
    B0137 = 'BANCO SOFITASA'
    B0138 = 'BANCO PLAZA'
    B0145 = 'BANCO DE COMERCIO EXTERIOR'
    B0146 = 'BANGENTE'
    B0151 = 'BFC BANCO FONDO COMUN'
    B0156 = '100% BANCO'
    B0157 = 'DEL SUR'
    B0163 = 'BANCO DEL TESORO'
    B0166 = 'BANCO AGRICOLA'
    B0168 = 'BANCRECER BANCO MICROFINANCIERO'
    B0169 = 'MIBANCO BANCO DE DESARROLLO'
    B0171 = 'BANCO ACTIVO'
    B0172 = 'BANCAMIGA BANCO MICROFINANCIERO'
    B0173 = 'BANCO INTERNACIONAL DE DESARROLLO'
    B0174 = 'BANPLUS BANCO COMERCIAL'
    B0175 = 'BANCO BICENTENARIO'
    B0176 = 'BANCO ESPIRITO SANTO'
    B0177 = 'BANCO DE LA FANB'
    B0190 = 'CITIBANK'
    B0191 = 'BANCO NACIONAL DE CREDITO'
    B0196 = 'ABN AMRO BANK'
    B0601 = 'INSTITUTO MUNICIPAL DE CRÉDITO POPULAR'

    def to_JSON(self):
        """to_JSON."""
        return self.name

    @classmethod
    def from_JSON(cls, s: str):
        """from_JSON."""
        return DestBank[s]


class IdType(Enum):
    """Different types of identifications for recipients."""

    V = auto()
    E = auto()
    J = auto()
    P = auto()
    G = auto()
    R = auto()

    def to_JSON(self):
        """to_JSON."""
        return self.name

    @classmethod
    def from_JSON(cls, s: str):
        """from_JSON."""
        return IdType[s]


@dataclass
class Recipient:
    """A person/entity wich receives money."""

    description: str
    name: str
    id_type: IdType
    identification: str
    bank: DestBank
    number: str
    number_pref: Optional[str] = None
    email: Optional[str] = None

    def __post_init__(self) -> None:
        """Check fields."""
        pass
        # TODO check integrity of fields

    def to_JSON(self) -> str:
        """Dump instance into json."""
        d = self.__dict__.copy()
        d['id_type'] = d['id_type'].to_JSON()
        d['bank'] = d['bank'].to_JSON()
        return json.dumps(d)

    def __repr__(self) -> str:
        """Representation of a Recipient."""
        return "Recipient(descr=%r, id=%r %r, num=%r, bank=%r)" % (
            self.description, self.id_type, hide(
                self.identification), hide(self.number), self.bank)

    @classmethod
    def from_JSON(cls, json_string: str) -> "Recipient":
        """Create instance from json."""
        r = Recipient(**json.loads(json_string))
        r.id_type = IdType[r.id_type]  # type: ignore
        r.bank = DestBank[r.bank]  # type: ignore
        if r.number_pref == '':
            r.number_pref = None
        return r


@dataclass
class Transfer:
    """A transfer to be done."""

    recipient: Recipient
    amount: str
    concept: str
    id: str

    def to_JSON(self) -> str:
        """Dump instance into json."""
        d = {}
        d['recipient'] = self.recipient.to_JSON()
        d['amount'] = self.amount
        d['concept'] = self.concept
        d['id'] = self.id
        return json.dumps(d)

    def __repr__(self) -> str:
        """Representation of a Transfer."""
        return "Transfer(recipient=%r, amount=%r, concept=%r)" % (
            self.recipient, self.amount, self.concept)

    @classmethod
    def from_JSON(cls, json_string: str) -> "Transfer":
        """Create instance from json."""
        d = json.loads(json_string)
        d['recipient'] = Recipient.from_JSON(d['recipient'])
        return Transfer(**d)


@dataclass
class TransferResult(ABC):
    """Abstract Transfer Result."""

    screenshot: Optional[str]
    id: str

    def to_JSON(self) -> str:
        """Dump instance into json."""
        return json.dumps(self.__dict__)

    @classmethod
    def from_JSON(
            cls, json_string: str) -> Union["TransferSuccess", "TransferError"]:
        """Create instance from json."""
        try:
            return TransferError(**json.loads(json_string))
        except Exception:
            return TransferSuccess(**json.loads(json_string))


class TransferNotStarted(TransferResult):
    """A Transfer that has not yet started."""

    def __init__(self, id: str):
        """Build a Transfer that hast not yet started."""
        self.id = id
        self.screenshot = None
        self.msg = 'Hubo un error antes de iniciar la transferencia.'


@dataclass
class TransferNotCompleted(TransferResult):
    """A Transfer that is not yet completed."""

    def __init__(self, id: str):
        """Build a Transfer that is not yet completed."""
        self.id = id
        self.screenshot = None
        self.msg = 'Transferencia iniciada pero no completada..'

    def __repr__(self) -> str:
        """Representation of a TransferNotCompleted."""
        sshot = None
        if self.screenshot is not None:
            sshot = self.screenshot[:10]
        return f'TransferNotCompleted({sshot}, id={self.id})'


@dataclass
class TransferSuccess(TransferResult):
    """A Transfer that was successful."""

    reference: str

    def __repr__(self) -> str:
        """Representation of a TransferSuccess."""
        sshot = None
        if self.screenshot is not None:
            sshot = self.screenshot[:10]
        return f'TransferSuccess({sshot}, id={self.id}, ref={self.reference})'


@dataclass
class TransferError(TransferResult):
    """A Transfer that was NOT successful."""

    error_msg: str

    def __repr__(self) -> str:
        """Representation of a TransferError."""
        sshot = None
        if self.screenshot is not None:
            sshot = self.screenshot[:10]
        return (f'TransferError({sshot}, id={self.id}, '
                f'error_msg={self.error_msg})')
