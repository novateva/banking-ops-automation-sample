from dataclasses import dataclass
from enum import Enum, auto
from typing import List, Tuple, Optional

from datetime import date, datetime
import json


@dataclass
class Transaction:
    """A transaction in one of the managed accounts."""

    date: date
    description: str
    reference: str
    amount: str
    direction: str  # '+' or '-'

    def __post_init__(self) -> None:
        """Check fields."""
        pass
        # TODO check integrity of fields
        # this is already checked in the service, though.

    def to_JSON(self) -> str:
        """Dump instance into json."""
        d = self.__dict__.copy()
        d['date'] = d['date'].strftime('%d/%m/%Y')
        return json.dumps(d)

    @classmethod
    def from_JSON(cls, json_string: str) -> "Transaction":
        """Create instance from json."""
        d = json.loads(json_string)
        d['date'] = datetime.strptime(d['date'], '%d/%m/%Y').date()
        return Transaction(**d)


# TODO Should be a class I guess
QueryResult = Tuple[Optional[Tuple[str, str, str]], Optional[List[Transaction]]]


class QueryInterval(Enum):
    LAST_MONTH = auto()
    LAST_DAY = auto()
    LAST_TWO_DAYS = auto()
    LAST_TWO_MONTHS = auto()

    def to_JSON(self):
        return self.name

    @classmethod
    def from_JSON(cls, s: str):
        return QueryInterval[s]
