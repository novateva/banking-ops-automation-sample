"""BDVSession."""

from logging import error
from selenium.webdriver.chrome.webdriver import WebDriver as Chrome
from selenium.webdriver.remote.webdriver import WebElement
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import (  # noqa: F401
    ElementNotInteractableException, ElementClickInterceptedException,
    TimeoutException, InvalidSessionIdException)

from bs4 import BeautifulSoup

import time
import os
import threading
from datetime import datetime, timezone, timedelta

from typing import List, Tuple, Optional, Callable
import logging

from ..exceptions import (DriverError, LoginError, OTPTimeoutError,
                          OTPOtherError, OTPIncorrectError, BankSpecificError)
from .logger import get_logger
from .selenium_utils import (find_by_xpath, find_by_id, wait_for_xpath,
                             wait_for_id, test_for_presence,
                             wait_invisibility_xpath, wait_visible_xpath)
from ..status import (Status, LoginStatus, LogoutStatus, ConsultStatus,
                      OTPStatus, TransferStartStatus, TransferDoneStatus,
                      WaitingTransfersStatus, RegisterClientStatus)
from . import selenium_utils
from .frame_stack import FrameStack
from .bank_session import BankSession

from ..transfer import (IdType, Recipient, Transfer, TransferNotStarted,
                        TransferResult, TransferSuccess, TransferError)
from ..query import Transaction, QueryInterval, QueryResult
from ..credentials import Credentials
from ..utils import get_VE_time

# FIXME make pretty exceptions some day.


class BDVSession(BankSession):
    """BDV (Banco De Venezuela) Session."""

    credentials: Credentials
    logger: logging.Logger
    frame_stk: FrameStack
    driver: Chrome
    report: Callable[[List[str], Status], None]
    report_all: Callable[[Status], None]

    def __init__(self, credentials: Credentials, get_otp: Callable[[], str],
                 report: Callable[[List[str], Status], None],
                 report_all: Callable[[Status], None]) -> None:
        """BDVSession, use as context manager."""
        self.credentials = credentials
        uid = credentials.uid
        if uid.isnumeric():
            uid = uid[-6:]
        self.logger = get_logger(f'VENEZUELA-{uid}', logging.DEBUG)
        self.frame_stk = FrameStack()
        self.get_otp = get_otp
        self.report = report
        self.report_all = report_all
        self.mt_otp = ''  # Mobile Transfer OTP is persistent.

    def __enter__(self) -> "BDVSession":
        """Open session, see open()."""
        for tries in range(2):
            try:
                self.open()
                return self
            except Exception:
                if tries == 1:
                    raise
                time.sleep(5)
                try:
                    self.driver.close()
                except (InvalidSessionIdException, AttributeError):
                    pass
        raise Exception('This is impossible.')

    # Literal[True] would be better return type but it's not in 3.7
    def __exit__(self, type, value, traceback) -> Optional[bool]:
        """Close session, see close()."""
        if type is None:
            self.close()
            return True
        else:
            screenshot_path, sshot = _take_error_screenshot(self)

            scr_msg = ''
            if screenshot_path is not None:
                scr_msg = f' Error screenshot save at {screenshot_path}.'

            self.logger.error('trying to quit session after error')
            try:
                _quit_session_and_close(self)
            except Exception:  # FIXME except Exception
                pass
            self.logger.exception(
                f'Exception getting balance and transactions.{scr_msg}')
            return None

    def open(self) -> None:
        """Get a webdriver and login."""
        try:
            self.driver = _get_driver(self.logger)
            _login(self)
            self.report_all(WaitingTransfersStatus())
        except (DriverError, LoginError) as e:
            self.logger.error(e)
            raise
        except Exception:  # FIXME except exception
            self.logger.exception('catch specific exception')
            raise

    def close(self):
        """Logout and quit session."""
        self.logger.info('quitting session')
        _quit_session_and_close(self)

    def _get(self) -> Tuple[Chrome, FrameStack, logging.Logger]:
        return (self.driver, self.frame_stk, self.logger)

    def get_balance_and_transactions(
            self, query_interval: QueryInterval) -> QueryResult:
        """Get balance and transactions."""
        return _get_balance_and_transactions(self, query_interval)

    def make_same_bank_transfer(self, transfers: List[Transfer],
                                transfer_results: List[TransferResult]) -> None:
        """Make a list of transfers."""
        _SBT_make_transfers(self, transfers, transfer_results)

    def make_other_bank_transfer(
            self, transfers: List[Transfer],
            transfer_results: List[TransferResult]) -> None:
        """Make a list of transfers."""
        _OBT_make_transfers(self, transfers, transfer_results)

    def make_mobile_transfer(self, transfers: List[Transfer],
                             transfer_results: List[TransferResult]) -> None:
        """Make a list of transfers."""
        _MT_make_transfers(self, transfers, transfer_results)


###############################################################################
###############################################################################
# Internal stuff starts here
###############################################################################
###############################################################################


def _get_VE_prev_month() -> Tuple[int, str]:
    """Calculate Venezuela's current month and return the previous one."""
    months = [
        'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',
        'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
    ]
    tz = timezone(timedelta(hours=-4))
    cur_month = int(datetime.now(tz).strftime("%m")) - 1
    prev_month = (cur_month - 1 + 12) % 12
    return (prev_month, months[prev_month])


def _wait_for_spinner(session: BDVSession) -> None:
    """Halt the execution until the 'loading' spinner goes away."""
    driver, _frame_stk, logger = session._get()
    wait_invisibility_xpath(driver, '//*[@id="spinner"]')


###############################################################################
# Login, Quit session, etc
###############################################################################


def _keep_alive(session: BDVSession) -> None:
    """Check if we need to maintain the session alive."""
    driver, _frame_stk, logger = session._get()
    logger.debug('Checking if need to keep session alive.')

    # The last character of the id for the overlay
    # where the extend session button appears is a dinamically
    # generated number.
    try:
        find_by_xpath(driver,
                      ('//*[starts-with(@id, "cdk-overlay-")]/'
                       'snack-bar-container/simple-snack-bar/button')).click()
        logger.debug('Kept session alive.')
    except Exception:  # FIXME
        logger.debug('No need to keep session alive right now.')

    # No need to worry about frames in BDV.


def _get_otp_while_keep_alive(session) -> str:
    """Keep the session alive while waiting for otp.

    Call the function keep_alive every 10 seconds, while waiting for
    get_otp to return the wanted otp.
    """
    session.logger.debug('Waiting for OTP...')
    otp_received = threading.Event()

    def keep_alive_while_waiting():
        cnt = 0
        while not otp_received.is_set():
            cnt += 1
            if cnt == 10:
                _keep_alive(session)
                cnt = 0
            time.sleep(1)

    keep_alive_thread = threading.Thread(target=keep_alive_while_waiting)
    keep_alive_thread.start()

    try:
        otp = session.get_otp()
    except (OTPTimeoutError, OTPOtherError):
        otp_received.set()
        keep_alive_thread.join()
        raise
    except Exception as e:
        otp_received.set()
        keep_alive_thread.join()
        session.logger.debug(f'Exception ocurred while fetching OTP: {str(e)}')
        raise

    session.logger.info(f'Succesfully recieved OTP: {otp}')

    otp_received.set()
    keep_alive_thread.join()
    return otp


def _take_error_screenshot(
        session: BDVSession,
        msg: str = '') -> Tuple[Optional[str], Optional[str]]:
    """Take screenshot of current screen.

    Args:
        session (BDVSession)
        msg (str, optional): small description on filename. Defaults to ''.

    Returns:
        Tuple[Optional[str], Optional[str]]: if screenshot was successful,
        return a pair of strings (file_relative_path, base64_screenshot)
    """
    driver, _frame_stk, _logger = session._get()
    directory = 'logs/screenshots'
    file_name = f'{get_VE_time()}-{os.getpid()}{msg}-bdv.png'  # TODO
    os.makedirs(directory, exist_ok=True)
    screenshot_path = f'{directory}/{file_name}'
    try:
        screenshot = driver.get_screenshot_as_file(screenshot_path)
        sshot = driver.get_screenshot_as_base64()
    except Exception:  # FIXME except Exception
        screenshot = False
        sshot = None
    if screenshot:
        return screenshot_path, sshot
    else:
        return None, sshot


def _get_driver(logger: logging.Logger) -> Chrome:
    """Create a new chrome driver and open BDV."""
    try:
        logger.info('opening driver')
        driver = selenium_utils.get_driver()
        driver.get("https://bdvenlinea.banvenez.com/")
        return driver
    except Exception:  # FIXME except Exception
        logger.exception('Couldn\'t open driver')
        raise DriverError()


def _login(session: BDVSession) -> None:
    """Log into a BDV account."""
    driver, frame_stk, logger = session._get()
    credentials = session.credentials
    logger.info('login')
    session.report_all(LoginStatus())
    # FIXME Sometimes the login click won't take effect and must be clicked
    # several times.
    try:
        uid = credentials.uid
        pw = credentials.pw
        # BDV does not ask for secret questions on normal login.

        # Login page.
        find_by_id(driver, 'tarjeta').send_keys(uid)
        find_by_id(driver, 'tarjeta-password').send_keys(pw)

        find_by_xpath(driver, '//*[@id="login"]/form/div[4]/button').click()

        # Check for incorrect credentials.
        if test_for_presence(driver, By.XPATH,
                             ('//*[starts-with(@id, "cdk-overlay-")]/'
                              'snack-bar-container/simple-snack-bar'), 5):
            error_text = find_by_xpath(
                driver, ('//*[starts-with(@id, "cdk-overlay-")]/'
                         'snack-bar-container/simple-snack-bar')).text
            raise Exception(error_text)  # FIXME

        # Find an element in the next page, past the login to see if we
        # logged in succesfully (?).
        _wait_for_spinner(session)
        wait_for_id(driver, "snav")
    except Exception:  # FIXME except Exception
        # TODO
        screenshot_path, sshot = _take_error_screenshot(session)
        scr_msg = ''
        if screenshot_path is not None:
            scr_msg = f' Error screenshot save at {screenshot_path}.'

        logger.exception(f'Couldn\'t log in.{scr_msg}')
        raise LoginError(sshot, 'No se pudo iniciar sesion')


def _quit_session_and_close(session: BDVSession) -> None:
    """Quit an open BDV session and close the driver."""
    # For BDV, we have to wait for the exit button to be interactable,
    # this code solves this for now.
    # FIXME This should be able to be fixed using wait_visible_xpath
    # and removing the tries.
    driver, _frame_stk, logger = session._get()
    session.report_all(LogoutStatus())
    _wait_for_spinner(session)
    tries = 0
    while tries < 5:
        try:
            # Click exit button.
            exit_button = wait_visible_xpath(
                driver, '//*[@id="snav"]/div/ul/li[6]/button')
            time.sleep(tries)
            exit_button.click()
            logger.info('log out')
            driver.close()
            return
        except (ElementNotInteractableException,
                ElementClickInterceptedException) as e:
            logger.warning(f'Cannot click the exit button {e}')
            time.sleep(0.5)
            tries += 1
    if tries == 5:
        driver.close()
        raise Exception('Intentos agotados al intentar salir de la sesion.')


###############################################################################
# Transfers work-flow helper functions.
###############################################################################


def _go_to_homepage(session: BDVSession) -> None:
    driver, _frame_stk, logger = session._get()
    logger.debug('Returning to the home page...')

    _wait_for_spinner(session)
    find_by_xpath(driver, ('/html/body/app-root/app-home-layout/app-menu/'
                           'div/mat-toolbar/ mat-toolbar-row/div/a')).click()

    logger.debug('Sucessfully returned to the home page.')


def _recipient_with_personal_id(recipient: Recipient) -> bool:
    """Return a boolean signaling if a recipient possesses a personal id."""
    personal_ids = [IdType.V, IdType.E]
    return recipient.id_type in personal_ids


def _recipient_with_commerce_id(recipient: Recipient) -> bool:
    """Return a boolean signaling if a recipient possesses a commerce id."""
    commerce_ids = [IdType.J, IdType.P, IdType.G, IdType.R]
    return recipient.id_type in commerce_ids


def _check_recipient_registered(session: BDVSession,
                                recipient: Recipient) -> Optional[WebElement]:
    """Check if a recipient is already registered in the user's database."""
    driver, _frame_stk, logger = session._get()
    _wait_for_spinner(session)
    # The are 2 elements with the same id so yea..., solution is to find
    # both and select the second one which is the one we want.
    driver.find_elements_by_id('cuentaDestino')[1].click()
    recipients = driver.find_elements_by_tag_name('mat-option')
    # If recipient list is empty then it's obviously not registered,
    # TODO This will break if the user has more than one personal account
    # registered, because we're assuming the first element of the
    # 'recipients' list is the user's personal account, which is
    # also found by the find_elements_by_tag_name method and
    # there's no way around it for now.
    if len(recipients) == 1:
        return None
    # BDV saves the registered recipients like this:
    # FirstName LastName AccountNumber, AccountNumber represents the
    # last 20 characters in the string.
    logger.debug('Searching if recipient is already registered...')
    for r in recipients:
        if r.text[-20:] == recipient.number:
            logger.debug('Registered recipient found.')
            return r

    # If the given recipient wasn't found in the register, then exit
    # the list by clicking any recipient in the list, this will select
    # it, but this action will be overwritten later when registering
    # a new user.
    recipients[-1].click()
    logger.debug('Recipient not registered.')
    return None


def _get_transfer_result(session: BDVSession, transfer_id: str,
                         name: str) -> TransferResult:
    """Take screenshot and retrieve reference."""
    driver, _frame_stk, logger = session._get()
    logger.debug('Retrieving transaction results...')

    _wait_for_spinner(session)
    info = wait_for_id(driver, 'print-section')
    # Obtain the operation's reference number.
    ref = find_by_xpath(driver,
                        '//*[@id="print-section"]/div/div[2]/div[1]/p').text
    ref = ref.split(' ')[-1]
    # Generate and save screenshot.
    os.makedirs('screenshots', exist_ok=True)
    name = name.replace(' ', '_')
    screenshot_path = f'screenshots/bdv_personal_{name}_{ref}.png'
    info.screenshot(screenshot_path)
    sshot = None
    try:
        sshot = info.screenshot_as_base64
    except Exception:  # FIXME
        pass
    logger.debug('Succesfully retrieved transanction results.')
    return TransferSuccess(sshot, transfer_id, ref)


def aux_get_err_msg(field: str, field_error: str) -> str:
    """Return a string to be displayed in an input field related Exception."""
    return f'Mensaje de error encontrado en el campo {field}:  {field_error}'


###############################################################################
# Same Bank Transfers : Transfers between the same bank (BDV).
###############################################################################


def _go_to_SBT_thirdparty_page(session: BDVSession) -> None:
    """Switch to the 'third party BDV accounts' transfers page."""
    driver, _frame_stk, logger = session._get()
    logger.debug(
        'Switching to the ""same bank transfers (third-party)" page...')

    _wait_for_spinner(session)
    # Click the 'transfers' button, wait a bit because it's finicky.
    transfer = wait_visible_xpath(driver, '//*[@id="snav"]/div/ul/li[2]/button')
    time.sleep(1)
    transfer.click()
    # Move to and the 'third-party accounts' option. The last character
    # for the id of the overlay containing this button is a dynamic number.
    tp_acc = find_by_xpath(
        driver, "//*[starts-with(@id, 'cdk-overlay-')]/div/div/button[2]")
    ActionChains(driver).move_to_element(tp_acc).click(tp_acc).perform()

    logger.debug('Succesfully switched to the'
                 ' "same bank (third-party)" transfers page')


def _SBT_register_recipient(session: BDVSession,
                            recipient: Recipient) -> List[WebElement]:
    """Register a recipient into the user's database."""
    driver, _frame_stk, logger = session._get()
    logger.debug('Registering new recipient...')

    _wait_for_spinner(session)
    register = find_by_xpath(
        driver, "//*[starts-with(@id, 'mat-checkbox-')]/label/div")
    # Move to the element because there's a wrapper just above
    # intersecting it.
    ActionChains(driver).move_to_element(register).click(register).perform()
    time.sleep(1)
    # Find all input locations since they all have dynamic ids.
    inputs = driver.find_elements_by_xpath(
        '//*[starts-with(@id, "mat-input-")]')
    inputs[0].send_keys(recipient.number)
    # Open ID Type selection list. Cannot find it by id or xpath because of the
    # dynamic ids so I'm searching for all selectors and choosing the one
    # I need. Hacky Wacky.
    selectors = driver.find_elements_by_tag_name('mat-select')
    selectors[1].click()
    # Select ID Type.
    rec_id = recipient.id_type.name
    path = f'//*[starts-with(@id, "mat-option-") and @value="{rec_id}"]'
    find_by_xpath(driver, path, 2).click()

    inputs[1].send_keys(recipient.identification)
    inputs[2].send_keys(recipient.name)

    # Click 'recieve SMS Code' for retrieving the OTP.
    try:
        wait_for_id(driver, 'code').click()
    except TimeoutException:
        raise Exception(('El elemento web para pedir OTP no se encuentra '
                         'en la pagina.'))

    # Check for input field related error messages.
    errors = driver.find_elements_by_xpath(
        '//*[starts-with(@id, "mat-error-")]')
    # FIXME Each one of these should be it's own neaty pretty exception.
    # TODO Improve the description for each of these exceptions.
    if errors:
        field, field_msg = '', ''
        for err in errors:
            if err.text == 'La cuenta es requerida':
                field = "cuenta destino"
                field_msg = "El numero de cuenta destino es obligatorio."
                raise Exception(aux_get_err_msg(field, field_msg))  # FIXME
            elif err.text == 'El nombre es requerido':
                field = "nombre del destinatario"
                field_msg = "El nombre del destinatario es obligatorio."
                raise Exception(aux_get_err_msg(field, field_msg))  # FIXME
            elif (err.text == ('Complete los datos, sin caracteres '
                               'especiales')):
                # Ignore this one here because we'll still have
                # fields to fill.
                pass

    logger.debug('Succesfully registered recipient.')
    return inputs


def _SBT_thirdparty_write_transfer_details(session: BDVSession,
                                           transfer: Transfer) -> bool:
    """Fill in the transfer details for third-party BDV accounts.

    Returns a boolean signaling if a new recipient had to be registered.
    """
    driver, _frame_stk, logger = session._get()
    logger.debug('Filling in transfer details...')

    _wait_for_spinner(session)
    # Choose user's account from list.
    wait_for_xpath(driver, ('/html/body/app-root/app-home-layout/app-menu/div/'
                            'mat-sidenav-container/mat-sidenav-content/'
                            'app-transferencias-terceros/mat-card/div/'
                            'form/mat-form-field[1]')).click()
    time.sleep(1)
    # TODO We're selecting the first account we see from the list
    # because we're assuming for now user only has 1 bank account
    # registered.
    # Again the last character for the id for this button is a dynamic
    # number.
    wait_for_xpath(driver, '//*[starts-with(@id, "mat-option-")]').click()

    # Transfer details.
    amount = transfer.amount
    concept = transfer.concept
    recipient = transfer.recipient
    recipient_location = _check_recipient_registered(session, recipient)

    if recipient_location is None:
        session.report([transfer.id], RegisterClientStatus())
        inputs = _SBT_register_recipient(session, recipient)
        inputs[3].send_keys(amount)
        inputs[4].send_keys(concept)
    else:
        # Since recipient is registered, just select it from the list.
        recipient_location.click()
        # Find all input locations. They all have dynamic ids.
        inputs = driver.find_elements_by_xpath(
            '//*[starts-with(@id, "mat-input-")]')
        inputs[0].send_keys(amount)
        inputs[1].send_keys(concept)

    time.sleep(1)
    # Check for input field related error messages.
    errors = driver.find_elements_by_xpath(
        '//*[starts-with(@id, "mat-error-")]')
    # FIXME Each one of these should be it's own neaty pretty exception.
    # TODO Try to improve the description for each of these exceptions.
    if errors:
        field, field_msg = '', ''
        for err in errors:
            if err.text == 'Campo requerido *':
                field = "concepto"
                field_msg = 'El concepto de la transferencia es obligatorio.'
            elif (err.text == ('El campo no cumple con '
                               'el minimo de caracteres requeridos')):
                field = "concepto"
                field_msg = ('El mensaje para el concepto no cumple con '
                             'el minimo de caracteres requeridos.')
            elif (err.text == ('El campo no puede contener caracteres '
                               'especiales')):
                field = "concepto"
                field_msg = ('El mensaje para el concepto no puede contener '
                             'caracteres especiales.')
            elif (err.text == ('Complete los datos, sin caracteres '
                               'especiales')):
                raise Exception(('Error durante el llenado de datos, algun '
                                 'campo no ha sido llenado, o ha sido llenado '
                                 'incorrectamente.'))
            raise Exception(aux_get_err_msg(field, field_msg))  # FIXME

    # Click the 'Transfer' button.
    find_by_xpath(
        driver,
        ('/html/body/app-root/app-home-layout/app-menu/'
         'div/mat-sidenav-container/mat-sidenav-content/'
         'app-transferencias-terceros/mat-card/div/div/button[2]')).click()

    # Error management.
    try:
        error_msg = find_by_xpath(
            driver, ('/html/body/app-root/app-home-layout/'
                     'app-menu/div/mat-sidenav-container/'
                     'mat-sidenav-content/app-transferencias-terceros/'
                     'mat-card/div[2]/h4')).text
        if error_msg == 'El número de cuenta es invalido':
            raise Exception(error_msg)  # FIXME
        elif error_msg == 'La cuenta no es del banco':
            raise Exception('La cuenta no pertenece al Banco de Venezuela.')
        else:
            # Maybe it's an error we've never seen.
            raise Exception(error_msg)  # FIXME
    except TimeoutException:
        pass

    logger.debug('Succesfully filled in transfer details.')
    return recipient_location is None


def _SBT_confirm_unregistered_transfer(session: BDVSession,
                                       transfer_id: str) -> None:
    """Confirm a transfer directed to an unregistered recipient."""
    driver, _frame_stk, logger = session._get()
    logger.debug('Confirming unregistered recipient transfer...')

    # Find OTP input field. It's id is dynamic and Selenium keeps seeing
    # the previous input fields, so I'm just searching for all of them again and
    # selecting the proper one, which is the last one in the page.
    _wait_for_spinner(session)
    try:
        b = driver.find_elements_by_xpath(
            '//*[starts-with(@id, "mat-input-")]')[-1]
        session.report([transfer_id], OTPStatus())
        otp = _get_otp_while_keep_alive(session)
        b.send_keys(otp)

        # Click the 'Confirm' button.
        find_by_xpath(driver, ('//*[starts-with(@id, "mat-dialog-")]/'
                               'transferencias-terceros-confirmar/'
                               'div/div[2]/div[3]/button[2]')).click()
        _wait_for_spinner(session)
        time.sleep(1)
    except Exception:
        # Click the 'Cancel' button.
        find_by_xpath(driver, ('//*[starts-with(@id, "mat-dialog-")]/'
                               'transferencias-terceros-confirmar/'
                               'div/div[2]/div[3]/button[1]')).click()
        _wait_for_spinner(session)
        time.sleep(1)
        raise

    # Error management in case the OTP expires or is incorrect.
    try:
        error_msg = find_by_xpath(
            driver, ('/html/body/app-root/app-home-layout/'
                     'app-menu/div/mat-sidenav-container/'
                     'mat-sidenav-content/app-transferencias-terceros/'
                     'mat-card/div[2]/h4')).text
        if (error_msg == ('El Codigo de Operaciones Especiales'
                          ' introducido ha expirado')):
            raise Exception(error_msg)
        elif (error_msg == ('El Codigo de Operaciones Especiales '
                            'introducido no coincide')):
            raise Exception(error_msg)
    except TimeoutException:
        pass

    logger.debug('Succesfully confirmed unregistered recipient transfer.')


def _SBT_thirdparty_transfer(session: BDVSession,
                             transfer: Transfer) -> TransferResult:
    """Execute a single same bank transfer to a BDV third-party account."""
    driver, _frame_stk, logger = session._get()
    logger.info('Starting execution of transfer (BDV -> third-party BDV)...')
    session.report([transfer.id], TransferStartStatus())

    _wait_for_spinner(session)
    try:
        _go_to_SBT_thirdparty_page(session)
        unregistered_rec = _SBT_thirdparty_write_transfer_details(
            session, transfer)
        if unregistered_rec:
            _SBT_confirm_unregistered_transfer(session, transfer.id)
        else:
            # Click the 'confirm' button.
            find_by_xpath(driver, ('//*[starts-with(@id, "mat-dialog-")]/'
                                   'transferencias-terceros-confirmar/'
                                   'div/div[2]/div[2]/button[2]')).click()

        transfer_results = _get_transfer_result(session, transfer.id,
                                                transfer.recipient.name)
    except Exception as e:  # FIXME
        logger.exception("Coudn't execute transfer (BDV -> Other bank).")
        screenshot_path, sshot = _take_error_screenshot(session)
        if screenshot_path is not None:
            scr_msg = f'Error Screenshot saved at: {screenshot_path}'
            logger.debug(f'{scr_msg}')
        transfer_results = TransferError(sshot, transfer.id, str(e))
        _go_to_homepage(session)

    if isinstance(transfer_results, TransferSuccess):
        logger.info('Succesfully executed transfer (BDV -> third-party BDV).')
    return transfer_results


def _SBT_make_transfers(session: BDVSession, transfers: List[Transfer],
                        transfers_results: List[TransferResult]) -> None:
    """Attemp to execute each of the requested transfers."""
    i = 0
    for transfer in transfers:
        transfers_results[i] = _SBT_thirdparty_transfer(session, transfer)
        session.report([transfer.id], TransferDoneStatus())
        i += 1


###############################################################################
# Other Banks Transfers : Transfers between BDV and other banks.
###############################################################################


def _go_to_OBT_page(session: BDVSession) -> None:
    """Switch to the 'other banks' transfers page."""
    driver, _frame_stk, logger = session._get()
    logger.debug('Switching to the "other banks" transfers page...')

    _wait_for_spinner(session)
    # Click the 'transfers' button, wait a bit because it's finicky.
    transfers = wait_visible_xpath(driver,
                                   '//*[@id="snav"]/div/ul/li[2]/button')
    time.sleep(1)
    transfers.click()
    # Click the 'other banks' option.
    ob_button = find_by_xpath(
        driver, '//*[starts-with(@id, "cdk-overlay-")]/div/div/button[3]')
    ActionChains(driver).move_to_element(ob_button).click(ob_button).perform()

    logger.debug('Succesfully switched to the "other banks" transfers page.')


def _OBT_register_recipient(session: BDVSession,
                            recipient: Recipient) -> List[WebElement]:
    """Register a recipient into the user's database."""
    driver, _frame_stk, logger = session._get()
    logger.debug('Registering new recipient...')

    _wait_for_spinner(session)
    # Mark the 'account not registered' checkbox.
    register = find_by_xpath(
        driver, "//*[starts-with(@id, 'mat-checkbox-')]/label/div")
    # Move to the element because there's a wrapper just above
    # intersecting it.
    ActionChains(driver).move_to_element(register).click(register).perform()
    time.sleep(1)
    # Find all input locations since they all have dynamic ids.
    inputs = driver.find_elements_by_xpath(
        '//*[starts-with(@id, "mat-input-")]')
    inputs[0].send_keys(recipient.number)
    # Open ID Type selection list. Cannot find it by id or xpath because of the
    # dynamic ids so I'm searching for all selectors and choosing the one
    # I need. Hacky Wacky.
    selectors = driver.find_elements_by_tag_name('mat-select')
    selectors[1].click()
    # Select ID Type.
    rec_id = recipient.id_type.name
    path = f'//*[starts-with(@id, "mat-option-") and @value="{rec_id}"]'
    find_by_xpath(driver, path).click()

    inputs[1].send_keys(recipient.identification)
    inputs[2].send_keys(recipient.name)

    # Click 'recieve SMS Code' for retrieving the OTP.
    # Radio button for selecting recieve otp via message
    # is different in OBT, it's an element with dynamic id "mat-radio-" and
    # value = "token".
    try:
        wait_for_xpath(
            driver,
            '//*[starts-with(@id, "mat-radio-") and @value="token"]').click()
    except TimeoutException:
        raise Exception(('El elemento web para pedir OTP no se encuentra '
                         'en la pagina.'))

    # Check for input field related error messages.
    errors = driver.find_elements_by_xpath(
        '//*[starts-with(@id, "mat-error-")]')
    errors.pop()
    # FIXME Each one of these should be it's own neaty pretty exception.
    # TODO Improve the description for each of these exceptions.
    if errors:
        field, field_msg = '', ''
        for err in errors:
            if err.text == 'La cuenta es requerida':
                field = "cuenta destino"
                field_msg = "El numero de cuenta destino es obligatorio."
                raise Exception(aux_get_err_msg(field, field_msg))  # FIXME
            elif err.text == 'El nombre es requerido':
                field = "nombre del destinatario"
                field_msg = "El nombre del destinatario es obligatorio."
                raise Exception(aux_get_err_msg(field, field_msg))  # FIXME
            elif (err.text == ('Complete los datos, sin caracteres '
                               'especiales')):
                # Ignore this one here because we'll still have
                # fields to fill.
                pass

    logger.debug('Succesfully registered recipient.')
    return inputs


def _OBT_write_transfer_details(session: BDVSession,
                                transfer: Transfer) -> bool:
    """Fill in the transfer details for other bank accounts.

    Returns a boolean signaling if a new recipient had to be registered.
    """
    driver, _frame_stk, logger = session._get()
    logger.debug('Filling in transfer details...')

    _wait_for_spinner(session)
    # Choose user's account from list.
    # Again 2 elements with the same id, find both of them and the
    # one we want is the second one.
    driver.find_elements_by_id('cuentaOrigen')[1].click()
    time.sleep(1)
    # TODO We're selecting the first account we see from the list
    # because we're assuming for now user only has 1 bank account
    # registered.
    # Again the last character for the id for this button is a dynamic
    # number.
    wait_for_xpath(driver, '//*[starts-with(@id, "mat-option-")]').click()

    # Transfer details.
    amount = transfer.amount
    concept = transfer.concept
    recipient = transfer.recipient
    recipient_location = _check_recipient_registered(session, recipient)

    if recipient_location is None:
        session.report([transfer.id], RegisterClientStatus())
        inputs = _OBT_register_recipient(session, recipient)
        inputs[3].send_keys(amount)
        inputs[4].send_keys(concept)
    else:
        # Since recipient is registered, just select it from the list.
        recipient_location.click()
        # Find all input locations. They all have dynamic ids.
        inputs = driver.find_elements_by_xpath(
            '//*[starts-with(@id, "mat-input-")]')
        inputs[0].send_keys(amount)
        inputs[1].send_keys(concept)

    time.sleep(1)
    # Check for input field related error messages.
    errors = driver.find_elements_by_xpath(
        '//*[starts-with(@id, "mat-error-")]')
    if errors:
        field, field_msg = '', ''
        for err in errors:
            if err.text == 'Campo requerido *':
                field = "concepto"
                field_msg = 'El concepto de la transferencia es obligatorio.'
            elif (err.text == ('El campo no cumple con '
                               'el minimo de caracteres requeridos')):
                field = "concepto"
                field_msg = ('El mensaje para el concepto no cumple con '
                             'el minimo de caracteres requeridos.')
            elif (err.text == ('El campo no puede contener caracteres '
                               'especiales')):
                field = "concepto"
                field_msg = ('El mensaje para el concepto no puede contener '
                             'caracteres especiales.')
            elif (err.text == ('Complete los datos, sin caracteres '
                               'especiales')):
                raise Exception(('Error durante el llenado de datos, algun '
                                 'campo no ha sido llenado, o ha sido llenado '
                                 'incorrectamente.'))
            raise Exception(aux_get_err_msg(field, field_msg))  # FIXME

    # Click the 'Transfer' button.
    find_by_xpath(
        driver,
        ('/html/body/app-root/app-home-layout/app-menu/'
         'div/mat-sidenav-container/mat-sidenav-content/'
         'app-transferencias-otrosbancos/mat-card/div/div/button[2]')).click()

    # Error management.
    try:
        error_msg = find_by_xpath(
            driver, ('/html/body/app-root/app-home-layout/'
                     'app-menu/div/mat-sidenav-container/'
                     'mat-sidenav-content/app-transferencias-otrosbancos/'
                     'mat-card/div[2]/h4')).text
        if error_msg == 'El número de cuenta es invalido':
            raise Exception(error_msg)  # FIXME
        elif error_msg == 'La cuenta no es de otro banco':
            msg = 'La cuenta destino no pertenece a otro banco.'
            raise Exception(msg)
        else:
            # Maybe it's an error we've never seen.
            raise Exception(error_msg)  # FIXME
    except TimeoutException:
        pass

    logger.debug('Succesfully filled in transfer details.')
    return recipient_location is None


def _OBT_confirm_unregistered_transfer(session: BDVSession,
                                       transfer_id: str) -> None:
    """Confirm a transfer directed to an unregistered recipient."""
    driver, _frame_stk, logger = session._get()
    logger.debug('Confirming unregistered recipient transfer...')

    # Find OTP input field. It's id is dynamic and Selenium keeps seeing
    # the previous input fields, so I'm just searching for all of them again and
    # selecting the proper one, which is the last one in the page.
    _wait_for_spinner(session)
    try:
        b = driver.find_elements_by_xpath(
            '//*[starts-with(@id, "mat-input-")]')[-1]
        session.report([transfer_id], OTPStatus())
        otp = _get_otp_while_keep_alive(session)
        b.send_keys(otp)

        # Click the 'Confirm' button.
        find_by_xpath(driver, ('//*[starts-with(@id, "mat-dialog-")]/'
                               'transferencias-otrosbancos-confirmar/'
                               'div/div[3]/div/button[2]')).click()
        _wait_for_spinner(session)
        time.sleep(1)
    except Exception:
        # Click the 'Cancel' button.
        find_by_xpath(driver, ('//*[starts-with(@id, "mat-dialog-")]/'
                               'transferencias-otrosbancos-confirmar/'
                               'div/div[3]/div/button[1]')).click()
        _wait_for_spinner(session)
        time.sleep(1)
        raise

    # Error management in case the OTP expires or is incorrect.
    try:
        error_msg = find_by_xpath(
            driver, ('/html/body/app-root/app-home-layout/'
                     'app-menu/div/mat-sidenav-container/'
                     'mat-sidenav-content/app-transferencias-otrosbancos/'
                     'mat-card/div[2]/h4')).text
        if (error_msg == ('El Codigo de Operaciones Especiales'
                          ' introducido ha expirado')):
            raise Exception(error_msg)
        elif (error_msg == ('El Codigo de Operaciones Especiales '
                            'introducido no coincide')):
            raise Exception(error_msg)
    except TimeoutException:
        pass

    logger.debug('Succesfully confirmed unregistered recipient transfer.')


def _OBT_transfer(session: BDVSession, transfer: Transfer) -> TransferResult:
    """Execute a single transfer from BDV to another bank."""
    driver, _frame_stk, logger = session._get()
    logger.info('Starting execution of transfer (BDV -> Other bank)...')
    session.report([transfer.id], TransferStartStatus())

    _wait_for_spinner(session)
    try:
        _go_to_OBT_page(session)
        unregistered_rec = _OBT_write_transfer_details(session, transfer)
        if unregistered_rec:
            _OBT_confirm_unregistered_transfer(session, transfer.id)
        else:
            # Click the 'confirm' button.
            find_by_xpath(driver, ('//*[starts-with(@id, "mat-dialog-")]/'
                                   'transferencias-otrosbancos-confirmar/'
                                   'div/div[3]/div/button[2]')).click()
        transfer_results = _get_transfer_result(session, transfer.id,
                                                transfer.recipient.name)
    except Exception as e:  # FIXME
        logger.exception("Coudn't execute transfer (BDV -> Other bank).")
        screenshot_path, sshot = _take_error_screenshot(session)
        if screenshot_path is not None:
            scr_msg = f'Error Screenshot saved at: {screenshot_path}'
            logger.debug(f'{scr_msg}')
        transfer_results = TransferError(sshot, transfer.id, str(e))
        _go_to_homepage(session)

    if isinstance(transfer_results, TransferSuccess):
        logger.info('Succesfully executed transfer (BDV -> third-party BDV).')
    return transfer_results


def _OBT_make_transfers(session: BDVSession, transfers: List[Transfer],
                        transfers_results: List[TransferResult]) -> None:
    i = 0
    for transfer in transfers:
        transfers_results[i] = _OBT_transfer(session, transfer)
        session.report([transfer.id], TransferDoneStatus())
        i += 1


###############################################################################
# Mobile Transfers
###############################################################################


def _go_to_MT_payment_page(session: BDVSession, recipient: Recipient) -> None:
    """Switch to the 'mobile payments' page."""
    driver, _frame_stk, logger = session._get()
    logger.debug('Switching to the "mobile payments" page')

    _wait_for_spinner(session)
    # Click the 'payments' button.
    payments = wait_visible_xpath(driver, '//*[@id="snav"]/div/ul/li[3]/button')
    time.sleep(1)
    payments.click()
    # Move to and click the 'PagoClave option.
    pc = find_by_xpath(
        driver, '//*[starts-with(@id, "cdk-overlay-")]/div/div/button[2]')
    ActionChains(driver).move_to_element(pc).click(pc).perform()
    # The next option to click depends on whenever the recipient
    # is a person or a commerce.
    if _recipient_with_personal_id(recipient):
        per = find_by_xpath(driver, '//*[text()="Pago a Personas"]')
        ActionChains(driver).move_to_element(per).click(per).perform()
    elif _recipient_with_commerce_id(recipient):
        com = find_by_xpath(driver, '//*[text()="Pago a Comercios"]')
        ActionChains(driver).move_to_element(com).click(com).perform()

    logger.debug('Succesfully switched to the "mobile payments" page.')


def _MT_write_transfer_details(session: BDVSession, transfer: Transfer) -> None:
    """Fill in the transfer details for mobile payment accounts.

    Returns a boolean signaling if a new recipient had to be registered.
    """
    driver, _frame_stk, logger = session._get()
    logger.debug('Filling in transfer details...')

    _wait_for_spinner(session)
    amount = transfer.amount
    concept = transfer.concept
    recipient_id_number = transfer.recipient.identification
    recipient_phone = (transfer.recipient.number_pref +
                       transfer.recipient.number)
    # Check if recipient is already in the 'frequent payments' list.
    # This is going to be more of a hassle than just inputting the
    # recipients credentials for every recipient because unlike
    # the search done in the transfers sections the records don't have
    # any standarized form. So I'm just gonna treat all recipients as
    # unregistered.
    # selector = find_by_xpath(driver, '//*[starts-with(@id, "mat-select-")]')
    # time.sleep(1)
    # selector.click()

    # Mark the 'payment not registered' checkbox.
    register = find_by_xpath(
        driver, '//*[starts-with(@id, "mat-checkbox-")]/label/div')
    ActionChains(driver).move_to_element(register).click(register).perform()

    # Choose the recipient's bank.
    recipient_bank = transfer.recipient.bank.value
    selector = find_by_xpath(driver, '//*[starts-with(@id, "mat-select-")]')
    time.sleep(1)
    selector.click()
    posible_banks = driver.find_elements_by_tag_name('mat-option')
    logger.debug("Searching for recipient's bank...")
    bank_found = False
    for bank in posible_banks:
        # Bank names are not standarized between bank platforms.
        # This is ugly...
        if recipient_bank == bank.text:
            logger.debug("Recipient's bank found.")
            bank.click()
            bank_found = True
        elif (recipient_bank == 'BANCO OCCIDENTAL DE DESCUENTO' and
              bank.text == 'BANCO OCCIDENTAL DE DESCUENTO CA'):
            logger.debug("Recipient's bank found.")
            bank.click()
            bank_found = True
        elif (recipient_bank == 'DEL SUR' and
              bank.text == 'DELSUR BANCO UNIVERSAL'):
            logger.debug("Recipient's bank found.")
            bank.click()
            bank_found = True
        elif (recipient_bank == 'BFC BANCO FONDO COMUN' and
              bank.text == 'FONDO COMUN BANCO UNIVERSAL'):
            logger.debug("Recipient's bank found.")
            bank.click()
            bank_found = True
        elif (recipient_bank == 'BANCO PROVINCIAL BBVA' and
              bank.text == 'BANCO PROVINCIAL'):
            logger.debug("Recipient's bank found.")
            bank.click()
            bank_found = True
        elif (recipient_bank == 'BANCRECER BANCO MICROFINANCIERO' and
              bank.text == 'BANCRECER'):
            logger.debug("Recipient's bank found.")
            bank.click()
            bank_found = True
        elif (recipient_bank == 'BANCAMIGA BANCO MICROFINANCIERO' and
              bank.text == 'BANCAMIGA BANCO MICROFINANCIERO C A'):
            logger.debug("Recipient's bank found.")
            bank.click()
            bank_found = True
        elif (recipient_bank == 'BANCO DE LA FANB' and
              bank.text == 'BANCO DE LAS FUERZAS ARMADAS BANFANB'):
            logger.debug("Recipient's bank found.")
            bank.click()
            bank_found = True

    if not bank_found:
        posible_banks[0].click()
        raise Exception('No se pudo encontrar el banco del destinatario.')

    # Fill in input fields.
    inputs = driver.find_elements_by_xpath(
        '//*[starts-with(@id, "mat-input-")]')
    inputs[0].send_keys(recipient_id_number)
    inputs[1].send_keys(recipient_phone)
    inputs[2].send_keys(amount)
    inputs[3].send_keys(concept)

    # Click 'recieve SMS Code' for retrieving the OTP.
    try:
        wait_for_xpath(
            driver,
            '//*[starts-with(@id, "mat-radio-") and @value="token"]').click()
    except TimeoutException:
        raise Exception(('El elemento web para pedir OTP no se encuentra '
                         'en la pagina.'))

    # Check for input field related errors.
    try:
        find_by_xpath(driver, '//*[starts-with(@id, "mat-error-")]')
        # In this page BDV does give any way to know which input field
        # has been left unfilled.
        error_msgm = ('Error durante el llenado de datos, algun '
                      'campo no ha sido llenado, o ha sido llenado '
                      'incorrectamente.')
        raise Exception(error_msgm)  # FIXME
    except TimeoutException:
        pass
    # Complete los datos, sin caracteres especiales

    # Click the 'pay' button.
    find_by_xpath(driver, ('//*[@class="boton mat-raised-button mat-primary" '
                           'and contains(., "Pagar")]')).click()

    logger.debug('Succesfully filled in transfer details.')


def _MT_confirm_payment(session: BDVSession, recipient: Recipient,
                        transfer_id: str) -> None:
    """Confirm a mobile payment."""
    driver, _frame_stk, logger = session._get()
    logger.debug('Confirming mobile payment...')

    _wait_for_spinner(session)
    # Find OTP input field.
    try:
        b = driver.find_elements_by_xpath(
            '//*[starts-with(@id, "mat-input-")]')[-1]
        # OTP for mobile transfers is persistent throught the session.
        # FIXME Make this a class method.
        if session.mt_otp is None:
            session.report([transfer_id], OTPStatus())
            otp = _get_otp_while_keep_alive(session)
            session.mt_otp = otp
        else:
            otp = session.mt_otp
        b.send_keys(otp)
        # Click the 'confirm' button.
        find_by_xpath(
            driver,
            ('//*[@class="boton mat-button" and contains(., "Confirmar")]'
            )).click()
        _wait_for_spinner(session)
        time.sleep(1)
    except Exception:
        # Click the 'cancel' button.
        find_by_xpath(
            driver,
            ('//*[@class="boton mat-button" and contains(., "Cancelar")]'
            )).click()
        _wait_for_spinner(session)
        time.sleep(1)
        raise

    # Error management.
    # Some one the errors returned by BDV are just... cryptic.
    errors = [
        'GENERAL',
        'SHORTCIRCUIT',  # What, ok then...
        'Monto menor al mínimo permitido para el servicio',
        'Transacción rechazada en conexión a otros Bancos',
        'El Codigo de Operaciones Especiales introducido ha expirado',
        'El Codigo de Operaciones Especiales introducido no coincide',
        'Cliente pagador no afiliado a PagoClave',
        ('No se pueden hacer más de un pago clave a la misma cuenta'
         ' destino por el mismo monto')
    ]
    # XPath for error header changes depending in which payment page we are,
    # there's also no way to do this remotely smart because they're too
    # different and don't even share common classes, names or id's between
    # elements. By default it's assumed we are in the personal payments page.
    err_xpath = ('/html/body/app-root/app-home-layout/'
                 'app-menu/div/mat-sidenav-container/'
                 'mat-sidenav-content/app-pagoclavep2p-home/'
                 'div[2]/div/mat-card/div[2]/div/h4')
    if _recipient_with_commerce_id(recipient):
        err_xpath = ('/html/body/app-root/app-home-layout/'
                     'app-menu/div/mat-sidenav-container/'
                     'mat-sidenav-content/app-pagarcomercio-detalle/'
                     'div[2]/mat-card/div[2]/h4')

    try:
        error_msg = find_by_xpath(driver, err_xpath, 5).text
        if (error_msg == ('El Codigo de Operaciones Especiales '
                          'introducido ha expirado')):
            raise Exception(error_msg)
        elif (error_msg == ('El Codigo de Operaciones Especiales '
                            'introducido no coincide')):
            raise Exception(error_msg)
        else:
            raise Exception(error_msg)
    except TimeoutException:
        pass

    logger.debug('Succesfully confirmed mobile payment.')


def _MT_transfer(session: BDVSession, transfer: Transfer) -> TransferResult:
    """Execute a mobile transfer from BDV to any bank."""
    _driver, _frame_stk, logger = session._get()
    logger.info('Starting execution of mobile transfer...')
    session.report([transfer.id], TransferStartStatus())

    _wait_for_spinner(session)
    try:
        _go_to_MT_payment_page(session, transfer.recipient)
        _MT_write_transfer_details(session, transfer)
        _MT_confirm_payment(session, transfer.recipient, transfer.id)
        transfer_results = _get_transfer_result(session, transfer.id,
                                                transfer.recipient.name)
    except Exception as e:  # FIXME
        logger.exception("Coudn't execute mobile transfer.")
        screenshot_path, sshot = _take_error_screenshot(session)
        if screenshot_path is not None:
            scr_msg = f'Error Screenshot saved at: {screenshot_path}'
            logger.debug(f'{scr_msg}')
        transfer_results = TransferError(sshot, transfer.id, str(e))
        _go_to_homepage(session)

    if isinstance(transfer_results, TransferSuccess):
        logger.info('Succesfully executed mobile transfer.')
    return transfer_results


def _MT_make_transfers(session: BDVSession, transfers: List[Transfer],
                       transfers_results: List[TransferResult]) -> None:
    i = 0
    for transfer in transfers:
        transfers_results[i] = _MT_transfer(session, transfer)
        session.report([transfer.id], TransferDoneStatus())
        i += 1


###############################################################################
# Get balance and transactions
###############################################################################


def _go_to_balance_page(session: BDVSession) -> None:
    """Switch to the 'consolidated position' page."""
    driver, _frame_stk, logger = session._get()
    logger.debug('Switching to the consolidated position page...')

    _wait_for_spinner(session)
    # Click the 'consults' button.
    consults = wait_visible_xpath(driver, '//*[@id="snav"]/div/ul/li[1]/a')
    time.sleep(1)
    consults.click()
    # Move to and click the 'consolidated positions' button.
    pos = find_by_xpath(
        driver, '//*[starts-with(@id, "cdk-overlay-")]/div/div/button[1]')
    ActionChains(driver).move_to_element(pos).click(pos).perform()

    logger.debug('Succesfully switched to consolidated position page.')


def _get_balance(session: BDVSession) -> Tuple[str, str, str]:
    """Retrieve main account balance."""
    driver, _frame_stk, logger = session._get()
    logger.info('Retrieving account balance...')

    _wait_for_spinner(session)
    # Access an account (for now the first one we see in the
    # 'consults' table).
    wait_for_xpath(driver, ('//*[starts-with(@id, "cdk-accordion-child-")]/div/'
                            'app-saldoscuenta/section/div/div/div/'
                            'table/tbody/tr/td[1]/span')).click()

    def aux_get_field(x):
        return find_by_xpath(
            driver,
            ('/html/body/app-root/app-home-layout/app-menu/'
             'div/mat-sidenav-container/mat-sidenav-content/'
             f'app-movimientoscta/div[3]/div[1]/table/tbody/tr/td[{x}]')).text

    _wait_for_spinner(session)
    balance = aux_get_field(3)
    deferred = aux_get_field(2)
    # It seems like BDV only displays the available and deferred balance,
    # cannot find anything for the blocked, so I'm assigning zero for now.
    blocked = '0.0'
    logger.info('Succesfully retrieved account balance.')
    return (balance, deferred, blocked)


def _go_to_transactions_page(session: BDVSession) -> None:
    """Switch to the 'online movements' page."""
    driver, _frame_stk, logger = session._get()
    logger.debug('Switching to the "online movements" page...')

    _wait_for_spinner(session)
    # Click the 'consults' button.
    consults = wait_visible_xpath(driver, '//*[@id="snav"]/div/ul/li[1]/a')
    # Even after waiting for visibility, we have to wait a bit more
    # otherwise it seems selenium can't click it.
    time.sleep(1)
    # TODO Relocate this comment block.
    # Sometimes the page takes even longer to load and an overlay
    # appears over it not allowing selenium to click the button until
    # it goes away.
    # Edit: It seems this overlay can appear anytime during a page switch
    # load operation.
    # Edit2: Function _wait_for_spinner fixes these, now only needs to be called
    # where the overlay is giving problems.
    consults.click()
    # Click the 'online movements' button.
    mov = find_by_xpath(
        driver, '//*[starts-with(@id, "cdk-overlay-")]/div/div/button[3]')
    # Move to the button before clicking it, because it seems there's an
    # invisible overlay above it which doesn't let selenium click
    # it the other way. Using ActionChains because this website doesnt
    # seem to have frames.
    ActionChains(driver).move_to_element(mov).click(mov).perform()

    logger.debug('Succesfully switched to the "online movements" page.')


def _select_previous_month(session: BDVSession) -> None:
    driver, _frame_stk, logger = session._get()
    i, prev_month = _get_VE_prev_month()
    # Find the button whose text attribute matches the previous month string.
    # Had to do this because the id's for the buttons are dynamically
    # generated.
    wait_for_xpath(driver, f'//*[text()="{prev_month}"]').click()


def _get_transactions(session: BDVSession) -> List[Transaction]:
    """Retrieve main account transactions."""
    driver, _frame_stk, logger = session._get()
    logger.info('Retrieving transactions...')

    # Select the account to consult. For now we're dealing with users with
    # only 1 bank account in their database.
    _wait_for_spinner(session)
    wait_for_xpath(driver, (
        '/html/body/app-root/app-home-layout/app-menu/div/'
        'mat-sidenav-container/mat-sidenav-content/'
        'app-movimientos-cuenta-enlinea/mat-card/form/mat-form-field')).click()
    # This button seems to be non interactable sometimes, not sure why.
    # Gonna add a sleep since it's solving the issue.
    time.sleep(1)
    # This button changes id dynamically and it's also one out of 4 buttons
    # (so far) that starts with the string 'mat-option', since it's
    # the first one being found so I'm just going to look for it
    # and ignore the rest.
    wait_for_xpath(driver, "//*[starts-with(@id, 'mat-option-')]").click()

    _wait_for_spinner(session)
    # Click the 'process' button. Sometimes this operation can fail due to
    # BDV's shenanigans and a error prompt will appear, gotta keep
    # clicking the button until it finally processes the order or
    # we run out of tries.
    process = find_by_xpath(
        driver, ('/html/body/app-root/app-home-layout/app-menu/div/'
                 'mat-sidenav-container/mat-sidenav-content/'
                 'app-movimientos-cuenta-enlinea/mat-card/div[3]/button'))
    # Arbitrarily using 10 tries.
    for tries in range(10):
        try:
            process.click()
            _wait_for_spinner(session)
            find_by_xpath(
                driver, '//*[text()="Transacción fallida, intente más tarde."]')
            if tries == 9:
                raise Exception(('Intentos agotados durante la obtencion de '
                                 'movimientos.'))
            else:
                logger.debug('Transaction fetch failed, retrying...')
                time.sleep(1)
        except TimeoutException:
            break
        except Exception:  # FIXME
            return []

    # Now check in case there are no transactions for the queried time period.
    _wait_for_spinner(session)
    try:
        time.sleep(1)
        find_by_xpath(driver, '//*[text()="NO HAY MOVIMIENTOS"]')
        logger.info('No transactions found for the specified time period.')
        return []
    except Exception:  # FIXME
        pass

    # TODO Sometimes if the connection is very slow, it will process
    # the query but not show the table anyway.
    _wait_for_spinner(session)
    time.sleep(1)
    # Obtain the 'next page' button.
    next_page = find_by_xpath(
        driver, ('/html/body/app-root/app-home-layout/'
                 'app-menu/div/mat-sidenav-container/'
                 'mat-sidenav-content/app-movimientos-cuenta-enlinea/'
                 'mat-card/div[4]/div[2]/mat-paginator/div/div[2]/button[3]'))

    # Scroll throught the pages gathering transactions
    # until the 'next page' button becomes disabled.
    transactions = []
    logger.debug('Scrolling throught the transaction pages...')
    while True:
        logger.debug('Found new page, gathering transactions...')
        transactions_table = wait_for_xpath(
            driver, ('/html/body/app-root/app-home-layout/app-menu/'
                     'div/mat-sidenav-container/'
                     'mat-sidenav-content/app-movimientos-cuenta-enlinea/'
                     'mat-card/div[4]/div[2]/div/mat-table'))
        soup = BeautifulSoup(transactions_table.get_attribute('innerHTML'),
                             features="html.parser")
        t_rows = soup.find_all('mat-row')
        for row in t_rows:
            t_cells = row.find_all('mat-cell')
            # Date displayed in BDV is in a DD/MM/YY - Hour:Minute fashion.
            # We don't really care about the later part so I'm slicing the
            # string.
            date = datetime.strptime(t_cells[0].contents[0].strip()[:10],
                                     '%d/%m/%Y').date()
            amount = t_cells[4].contents[0].strip()
            # Check sign of amount.
            direc = '-' if amount[0] == '-' else '+'
            transactions.append(
                Transaction(date=date,
                            description=t_cells[2].contents[0].strip(),
                            reference=t_cells[1].contents[0].strip(),
                            amount=amount,
                            direction=direc))
        if next_page.is_enabled():
            next_page.click()
        else:
            logger.debug('No more pages to explore.')
            break

    logger.info('Succesfully retrieved transactions.')
    # Check that retrieved transactions match the number of displayed
    # transactions (optional?).
    # assert len(transactions) == int(n_trans)
    return transactions


def _get_balance_and_transactions(session: BDVSession,
                                  query_interval: QueryInterval) -> QueryResult:
    """Login to BDV and retrieve account balance and transactions."""
    driver, _frame_stk, logger = session._get()
    logger.info('Retrieving account balance and transactions...')
    session.report_all(ConsultStatus())

    try:
        _go_to_balance_page(session)
        balances = _get_balance(session)
    except Exception:  # FIXME
        logger.error("Coudn't retrieve account balance.")
        balances = None

    try:
        _go_to_transactions_page(session)
        # By default BDV returns the current month's transactions.
        transactions = _get_transactions(session)

        if query_interval == QueryInterval.LAST_TWO_MONTHS:
            logger.debug('Retrieving older transactions...')
            _select_previous_month(session)
            older_transactions = _get_transactions(session)
            transactions = transactions + older_transactions

        logger.info('Succesfully retrieved account balance and transactions.')
    except Exception:  # FIXME
        logger.error("Coudn't retrieve account transactions.")
        transactions = None

    return (balances, transactions)


###############################################################################


# Report Placeholders for testing Status Update
def get_reports(
    transfers: List[Transfer]
) -> Tuple[Callable[[List[str], Status], None], Callable[[Status], None]]:

    def report(t_id: List[str], status: Status) -> None:
        print(f'reporting transfer {t_id}, status msg: {status}')

    def report_all(status: Status) -> None:
        report(list(map(lambda t: t.id, transfers)), status)

    return report, report_all


if __name__ == '__main__':
    pw = os.environ['CRED_ENCR_PW']
    creds = Credentials.from_encrypted_YAML_file(
        'credentials/credentials_BDV.yaml.encr', pw)

    from ..testing_recipients import (papa_daniel_bdv, papa_daniel_mercantil,
                                      papa_daniel_pago_movil_mer, wrong_bdv)

    test_transfer1 = Transfer(amount='1',
                              recipient=papa_daniel_bdv,
                              concept='Prueba novateva Daniel',
                              id='testytest001')

    test_transfer2 = Transfer(amount='1',
                              recipient=papa_daniel_mercantil,
                              concept='Prueba novateva Daniel',
                              id='testalatesta564')

    test_wrong_transfer = Transfer(amount='1',
                                   recipient=wrong_bdv,
                                   concept='Prueba novateva campos erroneos',
                                   id='testwronginputfields')

    test_mobile_transfer = Transfer(amount='100,00',
                                    recipient=papa_daniel_pago_movil_mer,
                                    concept='Prueba novateva movil',
                                    id='testmobiletrans1')

    transfers_bdv = [test_transfer1, test_transfer2]
    transfers_r_bdv: List[TransferResult] = [
        TransferNotStarted('bdv1'),
        TransferNotStarted('bdv2')
    ]

    transfers_mer = [test_transfer2, test_transfer2]
    transfers_r_mer: List[TransferResult] = [
        TransferNotStarted('bdvobt1'),
        TransferNotStarted('bdvobt2')
    ]

    (report, report_all) = get_reports(transfers_bdv + transfers_mer)

    def get_otp() -> str:
        return input('Insert OTP: ')

    with BDVSession(creds, get_otp, report, report_all) as session:
        #_SBT_make_transfers(session, transfers_bdv, transfers_r_bdv)
        pass
        #_MT_transfer(session, test_mobile_transfer)
