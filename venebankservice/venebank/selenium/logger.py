import logging
import os
from ..utils import get_VE_time


def get_logger(logger_name: str, logging_level: int) -> logging.Logger:
    name = f'{get_VE_time()}-{logger_name}'
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)

    console_formatter = logging.Formatter(
        f'%(levelname)s - {logger_name} - %(asctime)s - %(message)s',
        datefmt='%H:%M:%S')
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(console_formatter)
    console_handler.setLevel(logging_level)

    file_formatter = logging.Formatter('%(levelname)s;%(asctime)s;%(message)s',
                                       datefmt='%H:%M:%S')
    file_name = f'{name}.log'.replace(':', '-')
    os.makedirs('logs', exist_ok=True)
    file_handler = logging.FileHandler(f'logs/{file_name}')
    file_handler.setFormatter(file_formatter)
    file_handler.setLevel(logging.DEBUG)

    if not logger.hasHandlers():
        logger.addHandler(console_handler)
        logger.addHandler(file_handler)

    logger.info(f'log file: {file_name}')

    return logger
