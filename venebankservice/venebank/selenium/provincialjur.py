"""ProvincialJSession.

    It contains all functions for getting balance, transactions for the
    required QueryIntervals, and transfers between the same bank and
    other banks.

    Main is used for internal testing
"""
from selenium.webdriver.chrome.webdriver import WebDriver as Chrome
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.common.exceptions import (  # noqa: F401
    TimeoutException, InvalidSessionIdException, NoAlertPresentException)
from bs4 import BeautifulSoup

import time
import json
import os
import threading
from datetime import datetime, timezone, timedelta

from typing import List, Tuple, Optional, Callable
import logging

from ..exceptions import (DriverError, LoginError, OTPTimeoutError,
                          OTPOtherError)
from .logger import get_logger
from .selenium_utils import (find_by_xpath, wait_for_xpath, test_for_xpath,
                             test_for_presence)
from . import selenium_utils
from .frame_stack import FrameStack
from .bank_session import BankSession

from ...otp_interaction import check_ack, get_get_otp

from ..transfer import (Transfer, TransferNotCompleted, TransferNotStarted,
                        TransferResult, TransferSuccess, TransferError)
from ..status import (Status, LoginStatus, LogoutStatus, ConsultStatus,
                      OTPStatus, TransferStartStatus, TransferDoneStatus,
                      WaitingTransfersStatus)
from ..query import Transaction, QueryInterval, QueryResult
from ..credentials import Credentials
from ..utils import get_VE_time

# TODO Cambiar a ProvincialJSession


class ProvincialJSession(BankSession):
    """Banco Provincial Juridico Session."""

    credentials: Credentials
    logger: logging.Logger
    frame_stk: FrameStack
    driver: Chrome
    get_otp: Callable[[], str]
    report: Callable[[List[str], Status], None]
    report_all: Callable[[Status], None]

    def __init__(self, credentials: Credentials, get_otp: Callable[[], str],
                 report: Callable[[List[str], Status], None],
                 report_all: Callable[[Status], None]) -> None:
        """ProvincialJSession, use as context manager."""
        self.credentials = credentials
        uid = credentials.uid
        if uid.isnumeric():
            uid = uid[-6:]
        self.logger = get_logger(f'PROVJ-{uid}', logging.DEBUG)
        self.frame_stk = FrameStack()
        self.get_otp = get_otp
        self.report = report
        self.report_all = report_all

    def __enter__(self) -> "ProvincialJSession":
        """Open session, see open()."""
        for tries in range(2):
            try:
                self.open()
                return self
            except Exception:
                if tries == 1:
                    raise
                time.sleep(5)
                try:
                    self.driver.close()
                except (InvalidSessionIdException, AttributeError):
                    pass
        raise Exception('This is impossible.')

    # Literal[True] would be better return type but it's not in 3.7
    def __exit__(self, type, value, traceback) -> Optional[bool]:
        """Close session, see close()."""
        if type is None:
            self.close()
            return True
        else:
            screenshot_path, sshot = _take_error_screenshot(self)

            scr_msg = ''
            if screenshot_path is not None:
                scr_msg = f' Error screenshot save at {screenshot_path}.'

            self.logger.error('trying to quit session after error')
            try:
                _quit_session_and_close(self)
            except Exception:  # FIXME except Exception
                pass
            self.logger.exception(
                f'Exception getting balance and transactions.{scr_msg}')
            return None

    def open(self) -> None:
        """Get a webdriver and login."""
        try:
            self.driver = _get_driver(self.logger)
            _login(self)
            self.report_all(WaitingTransfersStatus())
        except (DriverError, LoginError) as e:
            self.logger.error(e)
            raise
        except Exception:  # FIXME except exception
            self.logger.exception('catch specific exception')
            raise

    def close(self):
        """Logout and quit session."""
        self.logger.debug('quitting session')
        _quit_session_and_close(self)

    def _get(self) -> Tuple[Chrome, FrameStack, logging.Logger]:
        return (self.driver, self.frame_stk, self.logger)

    def get_balance_and_transactions(
            self, query_interval: QueryInterval) -> QueryResult:
        """Get balance and transactions."""
        return _get_balance_and_transactions(self, query_interval)

    def make_same_bank_transfer(self, transfers: List[Transfer],
                                transfer_results: List[TransferResult]) -> None:
        """Make a list of transfers."""
        _SBT_get_transfers(self, transfers, transfer_results)

    def make_other_bank_transfer(
            self, transfers: List[Transfer],
            transfer_results: List[TransferResult]) -> None:
        _OBT_get_transfers(self, transfers, transfer_results)

    def make_mobile_transfer(self, transfers: List[Transfer],
                             transfer_results: List[TransferResult]) -> None:
        """Make a list of transfers.

        There are no mobile transfers for provincial :(
        """
        print("There are no mobile transfers for Provincial")


###############################################################################
###############################################################################
# Internal stuff starts here
###############################################################################
###############################################################################


def _get_VE_prev_month() -> Tuple[int, str]:
    months = [
        'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',
        'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
    ]
    tz = timezone(timedelta(hours=-4))
    cur_month = int(datetime.now(tz).strftime("%m")) - 1
    prev_month = (cur_month - 1 + 12) % 12
    return (prev_month, months[prev_month])


def _get_VE_cur_month() -> Tuple[int, str]:
    months = [
        'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',
        'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
    ]
    tz = timezone(timedelta(hours=-4))
    cur_month = int(datetime.now(tz).strftime("%m")) - 1
    return (cur_month, months[cur_month])


def _today() -> str:
    tz = timezone(timedelta(hours=-4))
    return datetime.now(tz).strftime("%d-%m-%Y")


def _get_delta_day(num: int) -> str:
    tz = timezone(timedelta(hours=-4))
    tmp = datetime.now(tz) - timedelta(days=num)
    return tmp.strftime("%d-%m-%Y")


###############################################################################
# Login, Quit session, etc
###############################################################################


def _get_otp_while_keep_alive(session, get_otp: Callable[[], str]) -> str:
    otp_received = threading.Event()

    def keep_alive_while_waiting():
        max_cnt = 0
        cnt = 0
        while ((not otp_received.is_set()) or (max_cnt == 24)):
            cnt += 1
            max_cnt += 1
            if cnt == 5:
                _keep_alive(session)
                cnt = 0
            time.sleep(1)

    keep_alive_thread = threading.Thread(target=keep_alive_while_waiting)
    keep_alive_thread.start()

    try:
        otp = get_otp()
    except (OTPTimeoutError, OTPOtherError):
        otp_received.set()
        keep_alive_thread.join()
        raise

    session.logger.info(f'received otp {otp}')

    otp_received.set()
    keep_alive_thread.join()
    return otp


def _keep_alive_test(session: ProvincialJSession) -> None:
    driver, frame_stk, logger = session._get()
    try:
        wait = WebDriverWait(driver, 200)
        wait.until(EC.alert_is_present())
        logger.debug("Alerta detectada")
        alert = driver.switch_to.alert
        alert.accept()
        logger.debug('maintained alive')
    except Exception:
        logger.debug('alert undetected')
        pass


def _keep_alive(session: ProvincialJSession) -> None:
    driver, frame_stk, logger = session._get()
    try:
        wait = WebDriverWait(driver, 1)
        wait.until(EC.alert_is_present())
        alert = driver.switch_to.alert
        alert.accept()
        logger.debug('maintained alive')
    except:
        #logger.debug('alert undetected')
        pass


def _quit_session(session: ProvincialJSession) -> None:
    # TODO
    driver, frame_stk, logger = session._get()
    logger.info('log out')
    session.report_all(LogoutStatus())
    frame_stk.switch_to_default(driver)
    frame_stk.switch_to(driver, By.NAME, 'FCabecera')
    # Para desconectar y sufrir epilepsia
    find_by_xpath(
        driver,
        f'//*[@id="rbb"]/table/tbody/tr[1]/td[2]/table/tbody/tr/td[4]/table/tbody/tr/td/a'
    ).click()


def _quit_session_and_close(session: ProvincialJSession) -> None:
    # TODO
    driver, _frame_stk, _logger = session._get()
    # Descomentar al final, por ahora mantener abierto para explorar flujo una vez logineado
    _quit_session(session)
    time.sleep(1)
    driver.close()


def _take_error_screenshot(
        session: ProvincialJSession,
        msg: str = '') -> Tuple[Optional[str], Optional[str]]:
    """Take screenshot of current screen.

    Args:
        session (ProvincialJSession)
        msg (str, optional): small description on filename. Defaults to ''.

    Returns:
        Tuple[Optional[str], Optional[str]]: if screenshot was successful,
        return a pair of strings (file_relative_path, base64_screenshot)
    """
    driver, _frame_stk, _logger = session._get()
    directory = 'logs/screenshots'
    file_name = f'{get_VE_time()} {os.getpid()}{msg}-provj.png'  # TODO
    os.makedirs(directory, exist_ok=True)
    screenshot_path = f'{directory}/{file_name}'

    try:
        screenshot = driver.get_screenshot_as_file(screenshot_path)
        sshot = driver.get_screenshot_as_base64()

    except Exception:  # FIXME except Exception
        _logger.exception("Fell in screenshot exception")
        screenshot = False
        sshot = None
    if screenshot:
        _logger.debug("Screenshot worked fine and returned")
        return screenshot_path, sshot
    else:
        return None, sshot


def _get_driver(logger: logging.Logger) -> Chrome:
    try:
        logger.info('opening driver')
        driver = selenium_utils.get_driver()
        driver.get(
            "https://ve1.provinet.net/nhvp_ve_web/atpn_es_web_jsp/login.jsp?it=2"
        )
        return driver
    except Exception:  # FIXME except Exception
        logger.exception('Couldn\'t open driver')
        raise DriverError()


def _login(session: ProvincialJSession) -> None:
    driver, frame_stk, logger = session._get()
    credentials = session.credentials

    try:
        logger.info('login')
        session.report_all(LoginStatus())
        # With the new credentials, uid is splitted by a space between the Username and the Name
        #uid = "J420420230"
        uid = credentials.uid.split()[0]
        #uid_name = "a"
        uid_name = credentials.uid.split()[1]
        #pw = "00000000"
        pw = credentials.pw
        #coords = json.loads('{' + credentials.answers[0][1][1:-1].replace("'", '"').replace('{', '').replace('}', '') + '}')
        coords = json.loads('{' + credentials.answers[0][1][1:-1].replace(
            '\'', '\"').replace('{', '').replace('}', '') + '}')

        if ("J" in uid or "G" in uid):
            # Case for R.I.F.
            logger.info('R.I.F. option')
            e = find_by_xpath(driver,
                              f'//*[@id="DOCTARJ"]/option[text()="R.I.F."]')
            e.click()
            # Case for R.I.F. with J
            if ("J" in uid):
                e = find_by_xpath(driver,
                                  f'//*[@id="NACIDOC"]/option[text()="J"]')
                e.click()
            # Case for R.I.F. with G
            else:
                e = find_by_xpath(driver,
                                  f'//*[@id="NACIDOC"]/option[text()="G"]')
                e.click()

            lastrif: str = uid[-1]
            firstrif: str = uid[0:-1]
            find_by_xpath(driver, '//*[@id="NUMDOCR"]').send_keys(firstrif)
            find_by_xpath(driver, '//*[@id="NUMIDER"]').send_keys(lastrif)

        # Case for Numero de Identificacion
        else:
            logger.info('ID number option')
            e = find_by_xpath(
                driver,
                f'//*[@id="DOCTARJ"]/option[text()="Número de Identificación"]')
            e.click()
            find_by_xpath(driver, '//*[@id="NUMDOCI"]').send_keys(uid)

        # Fill out the role input
        find_by_xpath(driver, '//*[@id="COD_USU"]').send_keys(uid_name)

        # Fill out the password input
        find_by_xpath(driver, '//*[@id="cod_cvepass"]').send_keys(pw)
        find_by_xpath(driver, '//*[@id="ingresarLogin"]').click()

        # Check for alerts in blanks
        try:
            alert = driver.switch_to.alert
            err_msg = alert.text
            logger.debug(err_msg)
            alert.accept()
            logger.error(f'Couldn\'t log in. {err_msg}')
            raise Exception  #FIXME
        except NoAlertPresentException:
            logger.debug('no alert detected')
            pass
        except TimeoutException:
            logger.debug('no alert detected by timeout')
            pass

        # Check for wrong input page
        if (test_for_presence(driver, By.XPATH, '//*[@id="tabla1"]/h1', 1)):
            if ("Incorrecto" in find_by_xpath(driver,
                                              '//*[@id="tabla1"]/h1').text):
                logger.error('Login with incorrect data')
                raise Exception  # FIXME

        # Coords
        e = find_by_xpath(driver, '//*[@id="coord"]')

        coord_act = e.get_attribute("alt")[-3:].replace(" ", "")
        logger.info('Looking for coordinate: ' + coord_act)

        # Input coordenada New Credentials Version
        coord = coords[coord_act]
        logger.info('Coordinate found, submitting: ' + coord)
        wait_for_xpath(driver, '//*[@id="eai_OTPTC"]').send_keys(coord)

        # Old Version
        # for (q, a) in coords:
        #    if coord_act in q:
        #        wait_for_xpath(driver, '//*[@id="eai_OTPTC"]').send_keys(a)
        #        break

        wait_for_xpath(driver, '//*[@id="bEntrar"]').click()
        logger.info('Logged in successfully')

    except Exception:  # FIXME except Exception
        # TODO
        screenshot_path, sshot = _take_error_screenshot(session)
        scr_msg = ''
        if screenshot_path is not None:
            scr_msg = f' Error screenshot save at {screenshot_path}.'

        logger.exception(f'Couldn\'t log in. {scr_msg}')
        raise LoginError(sshot, 'No se pudo iniciar sesion')


###############################################################################
# transfers
###############################################################################

# There are no common functions between same bank and other banks
# Everything is different. Provincial kind of sucks

###############################################################################
# transfer same bank
###############################################################################


def _get_transfer_result_sbt(session: ProvincialJSession, t_i: str,
                             name: str) -> TransferResult:
    """Take screenshot and get reference.

    Works for Provincial same bank
    """
    driver, _frame_stk, logger = session._get()
    try:
        completed = test_for_xpath(
            driver,
            '//*[@id="tablaPrincipal"]/tbody/tr[3]/td/table/tbody/tr/td/p')
        if (not completed):
            # There was an internet error here, which makes it unsure if
            # the transfer was a success
            # I havent been able to check this test, but it should work
            logger.debug(
                "Unsure if transfer is succesful due to an Internet error. Check Transactions"
            )
            return TransferNotCompleted(t_i)

        result = find_by_xpath(
            driver,
            '//*[@id="tablaPrincipal"]/tbody/tr[3]/td/table/tbody/tr/td/p').text
        if ("exitosamente" in result):
            logger.debug("The Transfer was a success. Taking a screenshot")
            ref = find_by_xpath(driver, '//*[@id="monto"]').text
            logger.debug(ref)

            os.makedirs('screenshots', exist_ok=True)
            screenshot_path = f'screenshots/pantallazo_provincial_j_{name}_{ref}.png'
            e = find_by_xpath(driver, '//*[@id="divPrincipal"]')
            e.screenshot(screenshot_path)
            sshot = None
            try:
                sshot = e.screenshot_as_base64
            except Exception:  # FIXME: except Exception
                pass
            logger.debug("Returning TransferSuccess")
            return TransferSuccess(sshot, t_i, ref)
        else:
            logger.debug(
                "Transfer was unsuccesful. I don't get this case. Check error")
            screenshot_path, sshot = _take_error_screenshot(session)
            scr_msg = ''
            if screenshot_path is not None:
                scr_msg = f' Error screenshot save at {screenshot_path}.'
            logger.debug(f'Exception getting transfer done. {scr_msg}')
            return TransferError(sshot, t_i,
                                 'La Transferencia no fue exitosa. Revisar los datos.')
    except Exception:
        logger.debug("Transfer was not succesful. Check error")
        screenshot_path, sshot = _take_error_screenshot(session)
        scr_msg = ''
        if screenshot_path is not None:
            scr_msg = f' Error screenshot save at {screenshot_path}.'
        logger.debug(f'Exception getting transfer done. {scr_msg}')
        return TransferError(sshot, t_i,
                             'La Transferencia no fue exitosa. Revisar los datos.')


def _SBT_get_transfers(session: ProvincialJSession, transfers: List[Transfer],
                       results: List[TransferResult]) -> None:
    get_otp = session.get_otp

    for i in range(0, len(transfers)):
        # results[i] = TransferNotCompleted(transfers[i].id)
        new_result = _PROV_single_transfer_same_bank(session, get_otp,
                                                     transfers[i])
        # print(new_result)
        session.report([transfers[i].id], TransferDoneStatus())
        results[i] = new_result


def _PROV_single_transfer_same_bank(session: ProvincialJSession,
                                    get_otp: Callable[[], str],
                                    t: Transfer) -> TransferResult:
    driver, frame_stk, logger = session._get()
    credentials = session.credentials
    amount = t.amount
    recipient = t.recipient
    account = recipient.number

    try:
        logger.debug('waiting for the transfer')
        session.report([t.id], TransferStartStatus())
        frame_stk.switch_to_default(driver)
        frame_stk.switch_to(driver, By.NAME, 'Fmenu')

        time.sleep(1)
        wait_for_xpath(driver,
                       '//*[@id="divMenu"]/table/tbody/tr[6]/td/a').click()
        time.sleep(1)

        frame_stk.switch_to_default(driver)
        frame_stk.switch_to(driver, By.NAME, 'Fprincipal')

        try: 
            wait_for_xpath(driver, '//*[@id="tab1"]/a[3]').click()
            time.sleep(1)

            # Choose account. What happens when there are TWO accounts?
            wait_for_xpath(
                driver,
                '//*[@id="tablaPrincipal"]/tbody/tr[9]/td/table/tbody/tr[3]/td[2]/customcombo/select/option[2]'
            ).click()

        except Exception:
            err_msg = "Problema con la conexión de Internet. No cargo la ventana principal."
            logger.debug(err_msg)
            screenshot_path, sshot = _take_error_screenshot(session)
            scr_msg = ''
            if screenshot_path is not None:
                scr_msg = f' Error screenshot save at {screenshot_path}.'
            logger.debug(f'Exception getting transfer done.{scr_msg}')
            return TransferError(sshot, t.id, err_msg)

        # Fill blanks
        #wrong_account = "01084545454545454545"
        #account = "01080001330100186166"
        find_by_xpath(
            driver,
            '//*[@id="tablaPrincipal"]/tbody/tr[9]/td/table/tbody/tr[4]/td[2]/inputfieldtext/input'
        ).send_keys(account)
        #amount = "150,00"
        find_by_xpath(
            driver,
            '//*[@id="tablaPrincipal"]/tbody/tr[9]/td/table/tbody/tr[7]/td[2]/inputfieldtext/input'
        ).send_keys(amount)

        # Click next:
        find_by_xpath(
            driver, '//*[@id="divocultar"]/table/tbody/tr/td[1]/input').click()

        # Check if there's an alert message if there's an error in the input
        try:
            alert = driver.switch_to.alert
            err_msg = alert.text
            logger.debug(err_msg)
            alert.accept()
            screenshot_path, sshot = _take_error_screenshot(session)
            scr_msg = ''
            if screenshot_path is not None:
                scr_msg = f' Error screenshot save at {screenshot_path}.'
            logger.debug(
                f'Exception getting transfer done. Error: {err_msg}. {scr_msg}')
            return TransferError(sshot, t.id, err_msg)
        except NoAlertPresentException:
            logger.debug('no alert detected')
            pass
        except TimeoutException:
            logger.debug('no alert detected by timeout')
            pass

        # Check if the Provincial account exists
        if (test_for_presence(driver, By.XPATH, '//*[@id="MENSAJE"]', 1)):
            err_msg = find_by_xpath(driver, '//*[@id="MENSAJE"]').text
            logger.error(err_msg)
            screenshot_path, sshot = _take_error_screenshot(session)
            scr_msg = ''
            if screenshot_path is not None:
                scr_msg = f' Error screenshot save at {screenshot_path}.'
            logger.error(f'Exception getting transfer done.{scr_msg}')
            return TransferError(sshot, t.id, err_msg)

        logger.debug('Going for special password')
        # Logging special password
        special_pw = credentials.mobile_pw
        wait_for_xpath(
            driver,
            '//*[@id="tablaPrincipal"]/tbody/tr[13]/td/table/tbody/tr[2]/td[2]/inputfieldtext/input'
        ).send_keys(special_pw)
        find_by_xpath(driver, '//*[@id="aceptar"]').click()

        # Need to check another alert which is different from other banks transfer.
        try:
            alert = driver.switch_to.alert
            err_msg = alert.text
            if ("Desea usted transferir" in err_msg):
                logger.debug('Confirmation alert detected and accepted')
                alert.accept()
                pass
            logger.debug(err_msg)
            alert.accept()
            # If it's at this point, the special password was wrong
            screenshot_path, sshot = _take_error_screenshot(session)
            scr_msg = ''
            if screenshot_path is not None:
                scr_msg = f' Error screenshot save at {screenshot_path}.'
            logger.debug(
                f'Exception getting transfer done. Error: {err_msg}. {scr_msg}')
            return TransferError(sshot, t.id, err_msg)
        except NoAlertPresentException:
            logger.debug('no alert detected')
            pass
        except TimeoutException:
            logger.debug('no alert detected by timeout')
            pass

        # Su clave especial es invalida
        if (test_for_presence(driver, By.XPATH, '//*[@id="MENSAJE"]', 1)):
            err_msg = find_by_xpath(driver, '//*[@id="MENSAJE"]').text
            logger.error(err_msg)
            screenshot_path, sshot = _take_error_screenshot(session)
            scr_msg = ''
            if screenshot_path is not None:
                scr_msg = f' Error screenshot save at {screenshot_path}.'
            logger.error(f'Exception getting transfer done.{scr_msg}')
            return TransferError(sshot, t.id, err_msg)

        # Each operation needs a new OTP
        if (test_for_xpath(driver, f'//*[@id="inputToken"]')):
            # OTP Moment
            logger.debug("Looking for otp at Session level")
            session.report([t.id], OTPStatus())
            try:
                otp = _get_otp_while_keep_alive(session, get_otp)
                logger.debug("Received otp succesfully")
            except Exception:
                screenshot_path, sshot = _take_error_screenshot(session)
                scr_msg = ''
                if screenshot_path is not None:
                    scr_msg = f' Error screenshot save at {screenshot_path}.'
                logger.exception(f'Exception making transfers.{scr_msg}')
                return TransferError(sshot, t.id, 'OTP no recibido.')

            find_by_xpath(driver, '//*[@id="inputToken"]').send_keys(otp)
            # Waiting for "Accept Button" to register good otp
            time.sleep(0.2)
            find_by_xpath(driver, '//*[@id="btnAceptar"]').click()

            # Estimado cliente: la Clave Digital introducida es inválida
            if (test_for_presence(driver, By.XPATH, '//*[@id="MENSAJE"]', 1)):
                err_msg = find_by_xpath(driver, '//*[@id="MENSAJE"]').text
                logger.error(err_msg)
                screenshot_path, sshot = _take_error_screenshot(session)
                scr_msg = ''
                if screenshot_path is not None:
                    scr_msg = f' Error screenshot save at {screenshot_path}.'
                logger.error(f'Exception getting transfer done.{scr_msg}')
                return TransferError(sshot, t.id, err_msg)

            name = recipient.name
            t_id = t.id
            t_result = _get_transfer_result_sbt(session, t_id, name)
            return t_result

        else:
            # No way it fell in here
            # raise Exception
            # LMAO THIS HAPPENS "Disculpa no se puede procesar la operacion"
            screenshot_path, sshot = _take_error_screenshot(session)
            scr_msg = ''
            if screenshot_path is not None:
                scr_msg = f' Error screenshot save at {screenshot_path}.'
            logger.debug(f'Exception making transfers. Operation wasnt processed. {scr_msg}')
            return TransferError(sshot, t.id, 'Error con el Banco Provincial. No se pudo procesar la operación.')

    except Exception:
        screenshot_path, sshot = _take_error_screenshot(session)
        scr_msg = ''
        if screenshot_path is not None:
            scr_msg = f' Error screenshot save at {screenshot_path}.'
        logger.exception(f'Exception making transfers.{scr_msg}')
        return TransferError(sshot, t.id, 'Error con la conexión de Internet.')


###############################################################################
# transfer other banks
###############################################################################


def _get_transfer_result_obt(session: ProvincialJSession, t_i: str,
                             name: str) -> TransferResult:
    """Take screenshot and get reference.

    Works for Provincial other banks only
    """
    driver, _frame_stk, logger = session._get()
    try:
        completed = test_for_xpath(driver,
                                   '//*[@id="tbTexto"]/tbody/tr[1]/td/p')
        if (not completed):
            # There was an internet error here, which makes it unsure if
            # the transfer was a success
            # I havent been able to check this test, but it should work
            logger.debug(
                "Unsure if transfer is succesful due to an Internet error. Check Transactions"
            )
            return TransferNotCompleted(t_i)

        result = find_by_xpath(driver,
                               '//*[@id="tbTexto"]/tbody/tr[1]/td/p').text
        if ("exitosamente" in result):
            logger.debug("The Transfer was a success. Taking a screenshot")
            ref = find_by_xpath(driver, '//*[@id="lblOperacion"]').text
            logger.debug(ref)

            os.makedirs('screenshots', exist_ok=True)
            screenshot_path = f'screenshots/pantallazo_provincial_j_{name}_{ref}.png'
            e = find_by_xpath(driver, '//*[@id="divPrincipal"]')
            e.screenshot(screenshot_path)
            sshot = None
            try:
                sshot = e.screenshot_as_base64
            except Exception:  # FIXME: except Exception
                pass
            logger.debug("Returning TransferSuccess")
            return TransferSuccess(sshot, t_i, ref)
        else:
            logger.debug(
                "Transfer was unsuccesful. I don't get this case. Check error")
            screenshot_path, sshot = _take_error_screenshot(session)
            scr_msg = ''
            if screenshot_path is not None:
                scr_msg = f' Error screenshot save at {screenshot_path}.'
            logger.debug(f'Exception getting transfer done. {scr_msg}')
            return TransferError(sshot, t_i,
                                 'La Transferencia no fue exitosa. Revisar los datos.')
    except Exception:
        logger.debug("Transfer was not succesful. Check error")
        screenshot_path, sshot = _take_error_screenshot(session)
        scr_msg = ''
        if screenshot_path is not None:
            scr_msg = f' Error screenshot save at {screenshot_path}.'
        logger.debug(f'Exception getting transfer done. {scr_msg}')
        return TransferError(sshot, t_i,
                             'La Transferencia no fue exitosa. Revisar los datos.')


def _OBT_get_transfers(session: ProvincialJSession, transfers: List[Transfer],
                       results: List[TransferResult]) -> None:
    get_otp = session.get_otp

    for i in range(0, len(transfers)):
        # results[i] = TransferNotCompleted(transfers[i].id)
        new_result = _PROV_single_transfer_other_bank(session, get_otp,
                                                      transfers[i])
        #print(new_result)
        session.report([transfers[i].id], TransferDoneStatus())
        results[i] = new_result


def _PROV_single_transfer_other_bank(session: ProvincialJSession,
                                     get_otp: Callable[[], str],
                                     t: Transfer) -> TransferResult:
    driver, frame_stk, logger = session._get()
    credentials = session.credentials
    amount = t.amount
    recipient = t.recipient
    concept = t.concept
    # Provincial doesnt accept numbers in its concept
    if (not concept.replace(' ', '').isalpha()):
        concept = "pago"
    account = recipient.number
    account_name = recipient.name
    account_id = recipient.identification
    account_id_type = recipient.id_type.name

    try:
        logger.debug('waiting for the transfer')
        session.report([t.id], TransferStartStatus())
        frame_stk.switch_to_default(driver)
        frame_stk.switch_to(driver, By.NAME, 'Fmenu')

        time.sleep(1)
        wait_for_xpath(driver,
                       '//*[@id="divMenu"]/table/tbody/tr[6]/td/a').click()
        time.sleep(1)

        frame_stk.switch_to_default(driver)
        frame_stk.switch_to(driver, By.NAME, 'Fprincipal')

        try:
            wait_for_xpath(driver, '//*[@id="tab1"]/a[4]').click()
            time.sleep(1)

            # Account: //*[@id="cmbDebitar"]/option[2]
            wait_for_xpath(driver, '//*[@id="cmbDebitar"]/option[2]').click()
        except Exception:
            err_msg = "Problema con la conexión de Internet. No cargo la ventana principal."
            logger.debug(err_msg)
            screenshot_path, sshot = _take_error_screenshot(session)
            scr_msg = ''
            if screenshot_path is not None:
                scr_msg = f' Error screenshot save at {screenshot_path}.'
            logger.error(f'Exception getting transfer done.{scr_msg}')
            return TransferError(sshot, t.id, err_msg)

        find_by_xpath(driver, '//*[@id="abonar"]').send_keys(account)

        #account_name = "Alexander Romero"
        find_by_xpath(
            driver,
            '//*[@id="tablaPrincipal"]/tbody/tr[9]/td/table/tbody/tr[4]/td[2]/div/inputfieldtext/input'
        ).send_keys(account_name)

        time.sleep(1)

        #id = "J244405080"
        #letter = id[0]
        #number = id[1:]

        if ((account_id_type in "JGVEP") and account_id.isnumeric()):
            if ("J" in account_id_type or "G" in account_id_type):
                # Case for R.I.F.
                logger.debug('R.I.F. option')
                e = wait_for_xpath(
                    driver,
                    f'//*[@id="cmbletracb"]/option[text()="{account_id_type}"]')
                e.click()
                lastrif: str = account_id[-1]
                firstrif: str = account_id[:-1]
                wait_for_xpath(driver,
                               f'//*[@id="cod_valida"]').send_keys(lastrif)
                wait_for_xpath(driver,
                               f'//*[@id="txtcedulab"]').send_keys(firstrif)
            else:
                logger.debug('CI option')
                e = wait_for_xpath(
                    driver,
                    f'//*[@id="cmbletracb"]/option[text()="{account_id_type}"]')
                e.click()
                wait_for_xpath(driver,
                               f'//*[@id="txtcedulab"]').send_keys(account_id)
        else:
            logger.debug('Wrong Identification for transfer')
            screenshot_path, sshot = _take_error_screenshot(session)
            scr_msg = ''
            if screenshot_path is not None:
                scr_msg = f' Error screenshot save at {screenshot_path}.'
            logger.exception(f'Exception making transfers.{scr_msg}')
            return TransferError(sshot, t.id,
                                 'Identificación incorrecta colocada.')

        #amount = "100,00"
        find_by_xpath(driver, f'//*[@id="cantidad"]').send_keys(amount)

        #reference = "pago"
        find_by_xpath(driver, f'//*[@id="txtReferencia"]').send_keys(concept)

        # Accept button //*[@id="divocultar"]/table/tbody/tr/td[1]/inputbutton/input
        find_by_xpath(
            driver,
            '//*[@id="divocultar"]/table/tbody/tr/td[1]/inputbutton/input'
        ).click()

        try:
            alert = driver.switch_to.alert
            err_msg = alert.text
            if ("Desea usted transferir" in err_msg):
                logger.debug('Confirmation alert detected and accepted')
                alert.accept()
                pass
            logger.debug(err_msg)
            alert.accept()
            screenshot_path, sshot = _take_error_screenshot(session)
            scr_msg = ''
            if screenshot_path is not None:
                scr_msg = f' Error screenshot save at {screenshot_path}.'
            logger.exception(f'Exception making transfers.{scr_msg}')
            return TransferError(sshot, t.id, err_msg)
        except NoAlertPresentException:
            logger.debug('no alert detected')
            pass
        except TimeoutException:
            logger.debug('no alert detected by timeout')
            pass

        logger.debug('Going for special password')

        special_pw = credentials.mobile_pw
        wait_for_xpath(
            driver,
            '//*[@id="tablaPrincipal"]/tbody/tr[12]/td/table/tbody/tr[2]/td[2]/inputfieldtext/input'
        ).send_keys(special_pw)

        # Accept button for sending special password
        find_by_xpath(
            driver,
            '//*[@id="divocultar"]/table/tbody/tr/td[1]/inputbutton/input'
        ).click()

        # Need to check another alert which is different from other banks transfer.
        try:
            alert = driver.switch_to.alert
            err_msg = alert.text
            logger.debug(err_msg)
            alert.accept()
            # If it's at this point, the special password was wrong
            screenshot_path, sshot = _take_error_screenshot(session)
            scr_msg = ''
            if screenshot_path is not None:
                scr_msg = f' Error screenshot save at {screenshot_path}.'
            logger.debug(
                f'Exception getting transfer done. Error: {err_msg}. {scr_msg}')
            return TransferError(sshot, t.id, err_msg)
        except NoAlertPresentException:
            logger.debug('no alert detected')
            pass
        except TimeoutException:
            logger.debug('no alert detected by timeout')
            pass

        # Su clave especial es invalida
        if (test_for_presence(driver, By.XPATH, '//*[@id="MENSAJE"]', 1)):
            err_msg = find_by_xpath(driver, '//*[@id="MENSAJE"]').text
            logger.error(err_msg)
            screenshot_path, sshot = _take_error_screenshot(session)
            scr_msg = ''
            if screenshot_path is not None:
                scr_msg = f' Error screenshot save at {screenshot_path}.'
            logger.error(f'Exception getting transfer done.{scr_msg}')
            return TransferError(sshot, t.id, err_msg)

        # Each operation needs a new OTP
        if (test_for_xpath(driver, f'//*[@id="inputToken"]')):
            # OTP Moment
            logger.debug("Looking for otp at Session level")
            session.report([t.id], OTPStatus())
            try:
                otp = _get_otp_while_keep_alive(session, get_otp)
                logger.debug("Received otp succesfully")
            except Exception:
                screenshot_path, sshot = _take_error_screenshot(session)
                scr_msg = ''
                if screenshot_path is not None:
                    scr_msg = f' Error screenshot save at {screenshot_path}.'
                logger.exception(f'Exception making transfers.{scr_msg}')
                return TransferError(sshot, t.id, 'OTP no recibido.')

            # OTP was received but its invalid in length
            if (len(otp) != 8):
                screenshot_path, sshot = _take_error_screenshot(session)
                scr_msg = ''
                if screenshot_path is not None:
                    scr_msg = f' Error screenshot save at {screenshot_path}.'
                logger.error(f'Exception getting transfer done.{scr_msg}')
                return TransferError(sshot, t.id, 'La OTP recibida es inválida.')

            find_by_xpath(driver, '//*[@id="inputToken"]').send_keys(otp)
            # Waiting for "Accept Button" to register good otp
            time.sleep(0.2)
            find_by_xpath(driver, '//*[@id="btnAceptar"]').click()

            # Estimado cliente: la Clave Digital introducida es inválida
            if (test_for_presence(driver, By.XPATH, '//*[@id="MENSAJE"]', 1)):
                err_msg = find_by_xpath(driver, '//*[@id="MENSAJE"]').text
                logger.error(err_msg)
                screenshot_path, sshot = _take_error_screenshot(session)
                scr_msg = ''
                if screenshot_path is not None:
                    scr_msg = f' Error screenshot save at {screenshot_path}.'
                logger.error(f'Exception getting transfer done.{scr_msg}')
                return TransferError(sshot, t.id, err_msg)

            name = recipient.name
            t_id = t.id
            t_result = _get_transfer_result_obt(session, t_id, name)
            return t_result

        else:
            # No way it fell in here
            # This happens!!!!
            screenshot_path, sshot = _take_error_screenshot(session)
            scr_msg = ''
            if screenshot_path is not None:
                scr_msg = f' Error screenshot save at {screenshot_path}.'
            logger.debug(f'Exception making transfers. Operation not available at the moment.{scr_msg}')
            return TransferError(sshot, t.id, 'Error con el Banco Provincial. No se pudo procesar la operación.')

    except Exception:
        screenshot_path, sshot = _take_error_screenshot(session)
        scr_msg = ''
        if screenshot_path is not None:
            scr_msg = f' Error screenshot save at {screenshot_path}.'
        logger.exception(f'Exception making transfers.{scr_msg}')
        return TransferError(sshot, t.id, 'Error con la conexión de Internet')


###############################################################################
# transfer mobile
###############################################################################

# There are no mobile transfers in Provincial Online :(

###############################################################################
# Get balance
###############################################################################


def _get_balance(session: ProvincialJSession) -> Tuple[str, str, str]:
    driver, frame_stk, logger = session._get()
    logger.debug('waiting for the balance')
    session.report_all(ConsultStatus())
    frame_stk.switch_to_default(driver)
    frame_stk.switch_to(driver, By.NAME, 'Fmenu')
    wait_for_xpath(driver, '//*[@id="divMenu"]/table/tbody/tr[4]/td/a').click()

    time.sleep(1)
    frame_stk.switch_to_default(driver)
    frame_stk.switch_to(driver, By.NAME, 'Fprincipal')
    wait_for_xpath(
        driver,
        '//*[@id="cuentasPersonales"]/tbody/tr[3]/td[1]/div/nobr/a').click()
    logger.debug('getting the balance')

    time.sleep(1)
    deferred = wait_for_xpath(driver, '//*[@id="lbDIFERIDO"]').text
    deferred_prov = find_by_xpath(driver, '//*[@id="lbDIFERIDOPROV"]').text
    balance = find_by_xpath(driver, '//*[@id="lbDISPONIBLE"]').text
    #logger.info('Diferido ' + deferred + ' Diferido provincial ' +
    # deferred_prov + ' Disponible ' + balance)

    logger.debug('got balances')
    return (balance, deferred, deferred_prov)


def _get_last_30_transactions(session: ProvincialJSession) -> List[Transaction]:
    # TODO
    driver, frame_stk, logger = session._get()
    logger.debug('waiting for the transaction')
    frame_stk.switch_to_default(driver)
    frame_stk.switch_to(driver, By.NAME, 'Fmenu')
    # Framenname: Fmenu
    find_by_xpath(driver, '//*[@id="divMenu"]/table/tbody/tr[4]/td/a').click()

    time.sleep(2)
    # Framename: Fprincipal
    frame_stk.switch_to_default(driver)
    frame_stk.switch_to(driver, By.NAME, 'Fprincipal')
    wait_for_xpath(
        driver,
        '//*[@id="cuentasPersonales"]/tbody/tr[3]/td[1]/div/nobr/a').click()

    logger.debug('getting the transactions')
    time.sleep(2)
    # Tabla //*[@id="movimientos"]
    transactions_table = find_by_xpath(driver, '//*[@id="ultMovimi"]/table ')
    soup = BeautifulSoup(transactions_table.get_attribute('innerHTML'),
                         features="html.parser")
    t_rows = soup.find_all('tr')
    transactions = []
    count = 0
    for t_row in t_rows:
        count = count + 1
        try:
            if (count > 2):
                tds = t_row.find_all('td')
                fecha = tds[0].getText().strip()

                if (fecha in "No hay movimientos"):
                    logger.debug('there are no transactions in this interval')
                    break

                if ("-" in tds[4].getText().strip()):
                    direc = "-"
                else:
                    direc = "+"

                date = datetime.strptime(fecha, '%d-%m-%Y').date()
                description = tds[2].getText().strip()
                reference = tds[1].getText().strip()
                amount = tds[4].getText().strip().replace("-", "")

                t = Transaction(date=date,
                                description=description,
                                reference=reference,
                                amount=amount,
                                direction=direc)
                # logger.debug(t)
                transactions.append(t)

        except Exception:
            logger.debug(
                "Skipped an specific transaction. It was probably an empty cell"
            )
            pass

    logger.debug('got transactions')
    return transactions


def _get_balance_and_transactions(session: ProvincialJSession,
                                  query_interval: QueryInterval) -> QueryResult:
    driver, frame_stk, logger = session._get()
    frame_stk.switch_to_default(driver)
    logger.debug('Getting balance and transactions')
    balances = ("", "", "")
    try:
        balances = _get_balance(session)
    except Exception:  # FIXME except Exception
        screenshot_path, sshot = _take_error_screenshot(session)
        scr_msg = ''
        if screenshot_path is not None:
            scr_msg = f' Error screenshot save at {screenshot_path}.'
        logger.error('Couldnt get Account Balance' + scr_msg)
        balances = None

    transactions = []

    if query_interval == QueryInterval.LAST_DAY:
        try:
            transactions = _get_transactions_interval(session, "LAST_DAY")
        except Exception:  # FIXME except Exception
            screenshot_path, sshot = _take_error_screenshot(session)
            scr_msg = ''
            if screenshot_path is not None:
                scr_msg = f' Error screenshot save at {screenshot_path}.'
            logger.error('Couldnt get transactions' + scr_msg)
            transactions = None

    elif query_interval == QueryInterval.LAST_TWO_DAYS:
        try:
            transactions = _get_transactions_interval(session, "LAST_TWO_DAYS")
        except Exception:  # FIXME except Exception
            screenshot_path, sshot = _take_error_screenshot(session)
            scr_msg = ''
            if screenshot_path is not None:
                scr_msg = f' Error screenshot save at {screenshot_path}.'
            logger.error('Couldnt get transactions' + scr_msg)
            transactions = None

    elif query_interval == QueryInterval.LAST_MONTH:
        try:
            transactions = _get_transactions_interval(session, "LAST_MONTH")
        except Exception:  # FIXME except Exception
            screenshot_path, sshot = _take_error_screenshot(session)
            scr_msg = ''
            if screenshot_path is not None:
                scr_msg = f' Error screenshot save at {screenshot_path}.'
            logger.error('Couldnt get transactions' + scr_msg)
            transactions = None

    elif query_interval == QueryInterval.LAST_TWO_MONTHS:
        try:
            transactions = _get_transactions_interval(
                session, "LAST_MONTH") + _get_transactions_interval(
                    session, "PREV_MONTH")
        except Exception:  # FIXME except Exception
            screenshot_path, sshot = _take_error_screenshot(session)
            scr_msg = ''
            if screenshot_path is not None:
                scr_msg = f' Error screenshot save at {screenshot_path}.'
            logger.error('Couldnt get transactions' + scr_msg)
            transactions = None

    return (balances, transactions)


def _get_transactions_interval(session: ProvincialJSession,
                               interval: str,
                               date_ini: datetime = None,
                               date_end: datetime = None) -> List[Transaction]:
    # TODO
    driver, frame_stk, logger = session._get()
    logger.debug('waiting for the transaction')
    frame_stk.switch_to_default(driver)
    frame_stk.switch_to(driver, By.NAME, 'Fmenu')
    # Framenname: Fmenu
    find_by_xpath(driver, '//*[@id="divMenu"]/table/tbody/tr[4]/td/a').click()

    time.sleep(1)
    # Framename: Fprincipal
    frame_stk.switch_to_default(driver)
    frame_stk.switch_to(driver, By.NAME, 'Fprincipal')
    wait_for_xpath(
        driver,
        '//*[@id="cuentasPersonales"]/tbody/tr[3]/td[1]/div/nobr/a').click()

    logger.debug('getting the transactions')
    time.sleep(1)

    if (interval == "LAST_DAY"):

        wait_for_xpath(
            driver, '//*[@id="filtros"]/table/tbody/tr[3]/td[1]/input').click()

        find_by_xpath(
            driver,
            '//*[@id="filtros"]/table/tbody/tr[3]/td[3]/div/select/option[text()="Día actual"]'
        ).click()
        logger.debug('Clicked día actual')
        find_by_xpath(driver, '//*[@id="btnConsultar"]').click()

    # Options on Provincial: Día actual, Últimos 7 días, Últimos 15 días, Últimos 30 días
    elif (interval == "LAST_TWO_DAYS"):

        wait_for_xpath(
            driver, '//*[@id="filtros"]/table/tbody/tr[3]/td[1]/input').click()

        find_by_xpath(
            driver,
            '//*[@id="filtros"]/table/tbody/tr[3]/td[3]/div/select/option[contains(text(), "7")]'
        ).click()
        logger.debug('Clicked últimos 7 días')
        find_by_xpath(driver, '//*[@id="btnConsultar"]').click()

    elif (interval == "LAST_MONTH"):

        wait_for_xpath(
            driver, '//*[@id="filtros"]/table/tbody/tr[4]/td[1]/input').click()

        (num, cur_month) = _get_VE_cur_month()

        find_by_xpath(
            driver,
            f'//*[@id="cmbMeses"]/option[contains(text(), "{cur_month}")]'
        ).click()

        logger.debug('Clicked last/current month')
        find_by_xpath(driver, '//*[@id="btnConsultar"]').click()

    elif (interval == "PREV_MONTH"):

        wait_for_xpath(
            driver, '//*[@id="filtros"]/table/tbody/tr[4]/td[1]/input').click()

        (num, prev_month) = _get_VE_prev_month()

        find_by_xpath(
            driver,
            f'//*[@id="cmbMeses"]/option[contains(text(), "{prev_month}")]'
        ).click()
        logger.debug('Clicked previous month')
        find_by_xpath(driver, '//*[@id="btnConsultar"]').click()

    elif (interval == "INTERVAL"):

        time.sleep(2)
        wait_for_xpath(
            driver, '//*[@id="filtros"]/table/tbody/tr[5]/td[1]/input').click()
        time.sleep(2)
        find_by_xpath(driver, '//*[@id="btnCal1"]').click()

        months = [
            "", "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep",
            "Oct", "Nov", "Dic"
        ]

        time.sleep(2)
        # Datetimes not comparing well so I had to do it to em
        if ((date_end.month < date_ini.month) or
            ((date_end.month == date_ini.month) and
             (date_end.day < date_ini.day))):
            logger.error(
                'Start date is greater than End date. Cant check calendars')
            raise Exception  #FIXME

        ini_month_num = date_ini.month
        end_month_num = date_end.month
        ini_month = months[ini_month_num]
        end_month = months[end_month_num]
        ini_day = str(date_ini.day)
        end_day = str(date_end.day)

        # Lets check this
        logger.debug('Checking first date: ' + ini_day + " " + ini_month)
        try:
            wait_for_xpath(
                driver,
                f'//*[@id="cal1_mainheading"]/select[1]/option[contains(text(), "{ini_month}")]'
            ).click()

            for x in range(1, 7):
                if (test_for_xpath(
                        driver,
                        f'//*[@id="cal1_calcells"]/tbody/tr[{x}]/td[text()="{ini_day}"]'
                )):
                    logger.debug('Found first date')
                    wait_for_xpath(
                        driver,
                        f'//*[@id="cal1_calcells"]/tbody/tr[{x}]/td[text()="{ini_day}"]'
                    ).click()
                    break

            logger.debug('Clicked on first Calendar')

            find_by_xpath(driver, '//*[@id="btnCal2"]').click()
            time.sleep(1)

            logger.debug('Checking second date: ' + end_day + " " + end_month)
            wait_for_xpath(
                driver,
                f'//*[@id="cal2_mainheading"]/select[1]/option[contains(text(), "{end_month}")]'
            ).click()

            for x in range(1, 7):
                if (test_for_xpath(
                        driver,
                        f'//*[@id="cal2_calcells"]/tbody/tr[{x}]/td[text()="{end_day}"]'
                )):
                    logger.debug('Found second date')
                    wait_for_xpath(
                        driver,
                        f'//*[@id="cal2_calcells"]/tbody/tr[{x}]/td[text()="{end_day}"]'
                    ).click()
                    break

            logger.debug('Clicked on second Calendar')
            wait_for_xpath(driver, '//*[@id="btnConsultar"]').click()
        except Exception:
            logger.exception('Error while clicking the Calendars')
            return []

    # Regresar button //*[@id="regresarListado"]/table/tbody/tr[2]/td[2]/input

    transactions_table = wait_for_xpath(driver, '//*[@id="movimientos"]')
    soup = BeautifulSoup(transactions_table.get_attribute('innerHTML'),
                         features="html.parser")
    t_rows = soup.find_all('tr')
    transactions = []
    tod = _today()
    start_date = ''
    one_days_ago = _get_delta_day(1)
    count = 0
    for t_row in t_rows:
        count = count + 1
        try:
            if (count > 2):
                tds = t_row.find_all('td')
                # Saving start date just in case
                if (count == 3):
                    start_date = tds[0].getText().strip()
                fecha = tds[0].getText().strip()

                # CASE FOR NO MOVEMENTS
                if (fecha in "No hay movimientos"):
                    logger.debug('there are no transactions in this interval')
                    break

                # CASE FOR LAST_DAY
                elif (interval == "LAST_DAY"):
                    if ((tod not in fecha)):
                        logger.debug(
                            'This is impossible, unless you (I?) clicked the wrong button'
                        )
                        break

                # CASE FOR LAST_TWO_DAYS
                elif (interval == "LAST_TWO_DAYS"):
                    if ((tod not in fecha) and (one_days_ago not in fecha)):
                        logger.debug('finished interval LAST_TWO_DAYS')
                        break

                # There are no specific cases for months or intervals since the page is already only showing transactions from there.
                if ("-" in tds[4].getText().strip()):
                    direc = "-"
                else:
                    direc = "+"

                date = datetime.strptime(fecha, '%d-%m-%Y').date()
                description = tds[2].getText().strip()
                reference = tds[1].getText().strip()
                amount = tds[4].getText().strip().replace("-", "")

                t = Transaction(date=date,
                                description=description,
                                reference=reference,
                                amount=amount,
                                direction=direc)
                # logger.info(t)
                transactions.append(t)

                # Case for more than 99 movements and need to call the function again
                if ((count == 101) and ((interval == "LAST_TWO_DAYS") or
                                        ((interval == "LAST_DAY")))):
                    logger.debug(
                        "Some transactions might have been lost due to same day with 100+ transactions"
                    )
                elif (count == 101):
                    logger.debug(
                        'There are more than 99 movements. Need to cycle up')

                    if (start_date == fecha):
                        # I haven't tested this case (impossible with current account), so be careful handling this
                        logger.debug(
                            "Some transactions might have been lost due to same day with 100+ transactions. Moving date to yesterday so it continues cycling"
                        )
                        date_in = date_ini
                        date = date - timedelta(days=1)
                    if ((interval == "LAST_MONTH") or
                        (interval == "PREV_MONTH")):

                        date_in = date.replace(day=1)
                    elif (interval == "INTERVAL"):
                        logger.debug("Going for another interval")
                        date_in = date_ini

                    transactions2 = _get_transactions_interval(
                        session, "INTERVAL", date_in, date)

                    # Adding new transactions to the list, checking for repeated elements
                    for t2 in transactions2:
                        if t2 not in transactions:
                            transactions.append(t2)

        except Exception:
            logger.debug(
                "Skipped an specific transaction. It was probably an empty cell"
            )
            pass
        # logger.debug(count)

    logger.debug('finish processing transactions')
    return transactions
    # Click de Regresar, not necessary
    # find_by_xpath(driver, '//*[@id="regresarListado"]/table/tbody/tr[2]/td[2]/input').click()


# Report Placeholders for testing Status Update
def get_reports(
    transfers: List[Transfer]
) -> Tuple[Callable[[List[str], Status], None], Callable[[Status], None]]:

    def report(t_id: List[str], status: Status) -> None:
        print(f'reporting transfer {t_id}, status msg: {status}')

    def report_all(status: Status) -> None:
        report(list(map(lambda t: t.id, transfers)), status)

    return report, report_all


if __name__ == '__main__':
    from ..testing_recipients import silvia_provincial, jesus_provincial, alex_mercantil, augusto_mercantil
    pw = 'danielvarela'
    creds = Credentials.from_encrypted_YAML_file(
        'credentials/credentials_prov.yaml.encr', pw)
    #creds = Credentials.from_YAML_file('credentials/credentials_prov.yaml')

    acc_id = 103
    bank = "PROVINCIAL"
    device_token = "fpIZeuirSU2P0pJtrrqvLC:APA91bFUFe4HfbtLVN8ugJRnHqSZqNR2CRzYwx1iUYDpBZfd3FDSZYA4nmXypKqYvTzqXhhiW4o1niE3OiMbePpbf7ehDel6olZ12df866XCzJkMfiD3xEFTEEaLOopV4nwVKGcnikGK"
    #ack_checked = check_ack(acc_id, bank, device_token, None, None)
    ack_checked = True

    obt = Transfer(amount='123,69',
                   recipient=alex_mercantil,
                   concept='prueba',
                   id='provobt1')
    obt2 = Transfer(amount='123,69',
                    recipient=augusto_mercantil,
                    concept='prueba',
                    id='provobt2')
    transfers_obt = [obt, obt2]
    tsprovres2: List[TransferResult] = [
        TransferNotStarted('provobt1'),
        TransferNotStarted('provobt2')
    ]

    sbt = Transfer(amount='123,69',
                   recipient=silvia_provincial,
                   concept='prueba2',
                   id='prov1')
    sbt2 = Transfer(amount='124,62',
                    recipient=jesus_provincial,
                    concept='prueba3',
                    id='prov2')
    transfers_sbt = [sbt]
    tsprovres: List[TransferResult] = [
        TransferNotStarted('prov1'),
        TransferNotStarted('prov2')
    ]

    (report, report_all) = get_reports(transfers_obt + transfers_sbt)

    if (ack_checked):
        get_otp = get_get_otp(acc_id, bank, device_token, None, None)
        with ProvincialJSession(creds, get_otp, report, report_all) as session:
            # bals, trans = session.get_balance_and_transactions(QueryInterval.LAST_DAY)
            # _keep_alive_test(session)
            #bals_ini = _get_balance(session)
            #print(bals_ini)
            # trans_30 = _get_last_30_transactions(session)
            # print(trans_30)
            # trans = _get_transactions_interval(session, "LAST_DAY")
            # print(trans)
            # trans = _get_transactions_interval(session, "LAST_TWO_DAYS")
            # print(trans)
            # trans = _get_transactions_interval(session, "LAST_MONTH")
            # print(trans)
            # trans = _get_transactions_interval(session, "PREV_MONTH")
            # print(trans)
            # fin = datetime.now()- timedelta(days=1)
            # ini = fin.replace(month=7)
            # trans = _get_transactions_interval(session, "INTERVAL", ini, fin)
            # print(trans)

            # (bals, trans) = session.get_balance_and_transactions(QueryInterval.LAST_TWO_DAYS)
            #print(bals)
            #print(trans)
            #print(len(trans))
            #print("En la session mi pana!")

            #_SBT_get_transfers(session, transfers_sbt, tsprovres)

            # EL CONCEPTO EN PROVINCIAL NO ACEPTA NUMEROS/SOLO ALFA

            _OBT_get_transfers(session, transfers_obt, tsprovres2)

    # def _get_otp():
    #     otp = input('otp: ')
    #     if otp == '':
    #         raise OTPTimeoutError()
    #     return otp

    # py -m venebankservice.venebank.selenium.provincialjur
