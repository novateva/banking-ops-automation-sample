"""MercantilSession."""

from selenium.webdriver.chrome.webdriver import WebDriver as Chrome
from selenium.webdriver.common.by import By
from selenium.common.exceptions import (  # noqa: F401
    ElementNotInteractableException, ElementClickInterceptedException,
    TimeoutException, InvalidSessionIdException)
from bs4 import BeautifulSoup

import time
import os
import threading
from datetime import datetime, timezone, timedelta

from typing import List, Tuple, Optional, Callable, Union
import logging

from ..exceptions import (DriverError, LoginError, OTPTimeoutError,
                          OTPOtherError, OTPIncorrectError)
from ..transfer import (  # noqa: F401
    Transfer, TransferNotStarted, TransferResult, TransferSuccess,
    TransferError, TransferNotCompleted)
from ..status import (Status, LoginStatus, LogoutStatus, ConsultStatus,
                      OTPStatus, TransferStartStatus, TransferDoneStatus,
                      WaitingTransfersStatus)
from ..query import Transaction, QueryInterval, QueryResult
from ..credentials import Credentials
from ..utils import get_VE_time
from .logger import get_logger
from .selenium_utils import (find_by_xpath, find_by_id, test_for_id,
                             wait_for_id, wait_for_xpath, find_by_name,
                             wait_for_case, test_for_xpath)
from . import selenium_utils
from .frame_stack import FrameStack
from .bank_session import BankSession


class MercantilSession(BankSession):
    """Mercantil Session."""

    credentials: Credentials
    logger: logging.Logger
    frame_stk: FrameStack
    driver: Chrome
    get_otp: Callable[[], str]
    report: Callable[[List[str], Status], None]
    report_all: Callable[[Status], None]

    def __init__(self, credentials: Credentials, get_otp: Callable[[], str],
                 report: Callable[[List[str], Status], None],
                 report_all: Callable[[Status], None]) -> None:
        """MercantilSession, use as context manager."""
        self.credentials = credentials
        uid = credentials.uid
        if uid.isnumeric():
            uid = uid[-6:]
        self.logger = get_logger(f'MER-{uid}', logging.DEBUG)
        self.frame_stk = FrameStack()
        self.get_otp = get_otp
        self.report = report
        self.report_all = report_all

    def __enter__(self) -> "MercantilSession":
        """Open session, see open()."""
        for tries in range(2):
            try:
                self.open()
                return self
            except Exception:
                if tries == 1:
                    raise
                time.sleep(5)
                try:
                    self.driver.close()
                except (InvalidSessionIdException, AttributeError):
                    pass
        raise Exception('This is impossible.')

    # Literal[True] would be better return type but it's not in 3.7
    def __exit__(self, type, value, traceback) -> Optional[bool]:
        """Close session, see close()."""
        if type is None:
            self.close()
            return True
        else:
            screenshot_path, sshot = _take_error_screenshot(self)

            scr_msg = ''
            if screenshot_path is not None:
                scr_msg = f' Error screenshot save at {screenshot_path}.'

            self.logger.error('trying to quit session after error')
            try:
                _quit_session_and_close(self)
            except Exception:  # FIXME except Exception
                pass
            self.logger.exception(
                f'Exception getting balance and transactions.{scr_msg}')
            return None

    def open(self) -> None:
        """Get a webdriver and login."""
        try:
            self.driver = _get_driver(self.logger)
            _login(self)
            self.report_all(WaitingTransfersStatus())
        except (DriverError, LoginError) as e:
            self.logger.error(e)
            raise
        except Exception:  # FIXME except exception
            self.logger.exception('catch specific exception')
            raise

    def close(self):
        """Logout and quit session."""
        self.logger.debug('quitting session')
        _quit_session_and_close(self)

    def _get(self) -> Tuple[Chrome, FrameStack, logging.Logger]:
        return (self.driver, self.frame_stk, self.logger)

    def get_balance_and_transactions(
            self, query_interval: QueryInterval) -> QueryResult:
        """Get balance and transactions."""
        return _get_balance_and_transactions(self, query_interval)

    def make_same_bank_transfer(self, transfers: List[Transfer],
                                transfer_results: List[TransferResult]) -> None:
        """Make a list of transfers."""
        get_otp = self.get_otp
        _SBT_get_transfer(self, get_otp, transfers, transfer_results)

    def make_other_bank_transfer(
            self, transfers: List[Transfer],
            transfer_results: List[TransferResult]) -> None:
        """Make a list of transfers."""
        get_otp = self.get_otp
        _OBT_get_transfer(self, get_otp, transfers, transfer_results)

    def make_mobile_transfer(self, transfers: List[Transfer],
                             transfer_results: List[TransferResult]) -> None:
        """Make a list of transfers.

        In mercantil we don't need to get otp for mobile transfers,
        it uses mobile password instead.
        """
        _MT_get_transfer(self, transfers, transfer_results)


###############################################################################
###############################################################################
# Internal stuff starts here
###############################################################################
###############################################################################


def _get_VE_prev_month() -> Tuple[int, str]:
    months = [
        'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',
        'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
    ]
    tz = timezone(timedelta(hours=-4))
    cur_month = int(datetime.now(tz).strftime("%m")) - 1
    prev_month = (cur_month - 1 + 12) % 12
    return (prev_month, months[prev_month])


###############################################################################
# Login, Quit session, etc
###############################################################################


def _get_otp_while_keep_alive(session, get_otp: Callable[[], str]) -> str:
    otp_received = threading.Event()

    def keep_alive_while_waiting():
        cnt = 0
        while not otp_received.is_set():
            cnt += 1
            if cnt == 5:
                _keep_alive(session)
                cnt = 0
            time.sleep(1)

    keep_alive_thread = threading.Thread(target=keep_alive_while_waiting)
    keep_alive_thread.start()

    try:
        otp = get_otp()
    except (OTPTimeoutError, OTPOtherError):
        otp_received.set()
        keep_alive_thread.join()
        raise

    session.logger.info(f'received otp {otp}')

    otp_received.set()
    keep_alive_thread.join()
    return otp


def _keep_alive(session: MercantilSession) -> None:
    """Check if we need to click 'keep session alive'."""
    driver, frame_stk, logger = session._get()
    logger.debug('checking if need to keep alive')
    driver.switch_to.default_content()
    try:
        driver.switch_to.frame(find_by_id(driver, 'frameBody'))
        # changed?
        # driver.switch_to.frame(
        # find_by_xpath(driver, '/html/body/table[3]/tbody/tr/td/iframe'))
        driver.switch_to.frame(find_by_id(driver, 'TB_iframeContent'))
        find_by_xpath(driver,
                      '/html/body/table/tbody/tr/td/div[3]/a[1]').click()
        logger.debug('maintained alive')
    except Exception:  # FIXME except Exception
        pass
    frame_stk.restore(driver)


def _quit_session(session: MercantilSession) -> None:
    driver, frame_stk, logger = session._get()
    logger.info('log out')
    session.report_all(LogoutStatus())
    frame_stk.switch_to_default(driver)
    frame_stk.switch_to(driver, By.NAME, 'adFrame')
    frame_stk.switch_to(driver, By.NAME, 'rightLogo')
    find_by_xpath(driver, ('/html/body/table/tbody/tr[1]/td/table/'
                           'tbody/tr[3]/td/table/tbody/tr/td[2]/a')).click()


def _quit_session_and_close(session: MercantilSession) -> None:
    driver, _frame_stk, _logger = session._get()
    _quit_session(session)
    # TODO
    time.sleep(2)
    driver.close()


def _take_error_screenshot(
        session: MercantilSession,
        msg: str = '') -> Tuple[Optional[str], Optional[str]]:
    """Take screenshot of current screen.

    Args:
        session (MercantilSession)
        msg (str, optional): small description on filename. Defaults to ''.

    Returns:
        Tuple[Optional[str], Optional[str]]: if screenshot was successful,
        return a pair of strings (file_relative_path, base64_screenshot)
    """
    driver, _frame_stk, _logger = session._get()
    directory = 'logs/screenshots'
    file_name = f'{get_VE_time()}-{os.getpid()}{msg}-mercantil.png'  # TODO
    os.makedirs(directory, exist_ok=True)
    screenshot_path = f'{directory}/{file_name}'
    try:
        screenshot = driver.get_screenshot_as_file(screenshot_path)
        sshot = driver.get_screenshot_as_base64()
    except Exception:  # FIXME except Exception
        screenshot = False
        sshot = None
    if screenshot:
        return screenshot_path, sshot
    else:
        return None, sshot


def _get_driver(logger: logging.Logger) -> Chrome:
    try:
        logger.debug('opening driver')
        driver = selenium_utils.get_driver()
        driver.get("https://www30.mercantilbanco.com/olb/InitMerc?from=index")
        return driver
    except Exception:  # FIXME except Exception
        logger.exception('Couldn\'t open driver')
        raise DriverError()


def _login(session: MercantilSession) -> None:
    driver, frame_stk, logger = session._get()
    credentials = session.credentials
    try:
        logger.info('login')
        session.report_all(LoginStatus())
        uid = credentials.uid
        pw = credentials.pw
        answers = credentials.answers
        # uid and pw
        frame_stk.switch_to(driver, By.NAME, 'mainFrame')
        find_by_id(driver, "userId").send_keys(uid)
        find_by_id(driver, "password").send_keys(pw)
        find_by_id(driver, "accept_btn").click()

        # questions
        def answerQuestion(driver, n):
            question = (find_by_id(driver,
                                   f'lresponse{n}').find_element_by_xpath(
                                       "following-sibling::*[1]").text)
            for (q, a) in answers:
                if q in question:
                    find_by_name(driver, f'response{n}').send_keys(a)
                    return
            # TODO
            # print(f'Don\'t know how to answer: {question}')
            driver.close()

        frame_stk.switch_to(driver, By.ID, 'frameBody')
        answerQuestion(driver, '0')
        answerQuestion(driver, '1')
        find_by_xpath(driver, "//td[@id='tdN']/input[1]").click()
        find_by_id(driver, 'accept_btn').click()
        frame_stk.switch_to_default(driver)
    except Exception:  # FIXME except Exception
        # TODO
        screenshot_path, sshot = _take_error_screenshot(session)
        scr_msg = ''
        if screenshot_path is not None:
            scr_msg = f' Error screenshot save at {screenshot_path}.'

        logger.exception(f'Couldn\'t log in.{scr_msg}')
        raise LoginError(sshot, 'No se pudo iniciar sesion')


###############################################################################
# transfers
###############################################################################


def _go_to_transfer(session: MercantilSession, lnk1_num: int,
                    lnk2_num: int) -> None:
    tries = 0
    while tries < 5:
        driver, frame_stk, logger = session._get()
        try:
            frame_stk.switch_to_default(driver)
            frame_stk.switch_to(driver, By.ID, 'leftFrame')
            find_by_id(driver, 'lnk3').click()
            find_by_id(driver, f'lnk{lnk1_num}').click()
            frame_stk.switch_to_default(driver)
            frame_stk.switch_to(driver, By.ID, 'frameBody')
            time.sleep(tries)
            find_by_id(driver, f'lnk{lnk2_num}').click()
            return
        except (ElementNotInteractableException,
                ElementClickInterceptedException) as e:
            logger.warning(
                f'Couldnt click go to transfer button, trying again {e}')
            time.sleep(0.5)
    # TODO raise some exception


def _write_single_transfer_details(
    session: MercantilSession,
    t: Transfer,
    cur: int,
    is_last: bool,
) -> TransferResult:
    driver, _frame_stk, logger = session._get()

    t_i = str(cur)
    if cur == 0:
        t_i = ''

    amount = t.amount
    concept = t.concept
    recipient = t.recipient

    def details_error(msg):
        screenshot, sshot = _take_error_screenshot(session, f'-{t.id}')
        if screenshot is None:
            screenshot = 'Couldn\'t take screenshot.'
        res = TransferError(sshot, t.id, msg)
        find_by_id(driver, f'papeleraActiva{t_i}').click()
        if not is_last:
            find_by_id(driver, 'addrow').click()
        return res

    try:
        find_by_id(driver, f'accountFromId{t_i}').find_elements_by_tag_name(
            'option')[1].click()
        find_by_id(
            driver,
            f'accountToId{t_i}').find_elements_by_tag_name('option')[1].click()

        find_by_id(driver, f'amount{t_i}').send_keys(amount)
        # TODO Check the actual amount

        find_by_id(driver, f'benefAccount{t_i}').send_keys(recipient.number)

        id_type_found = False
        for i in range(1, 7):
            e = find_by_xpath(driver,
                              f'//*[@id="benefTypeId{t_i}"]/option[{i}]')
            if recipient.id_type.name == e.text[0]:
                id_type_found = True
                e.click()
                break

        if not id_type_found:
            return details_error(f'IdType={recipient.id_type!r} not found')

        find_by_id(driver, f'benefId{t_i}').send_keys(recipient.identification)
        if test_for_id(driver, f'benefName{t_i}'):
            find_by_id(driver, f'benefName{t_i}').send_keys(recipient.name)
        find_by_id(driver, f'concept{t_i}').send_keys(concept)

        # NOTE we click addrow and then erase it if it was the last one,
        # this way we can consistently see if the transfer is rejected here.
        find_by_id(driver, 'addrow').click()
        time.sleep(0.2)
        msg = find_by_id(driver, 'summary').text
        logger.debug(f'summary = {msg}')
        if msg == '':
            if is_last:
                find_by_id(driver, f'papeleraActiva{cur+1}').click()
            return TransferNotCompleted(t.id)
        return details_error(msg)
    except Exception as e:  # FIXME except Exception
        logger.exception('last resource except Exception in '
                         '_write_single_transfer_details in mercantil')
        return details_error(f'Unhandled error: {e}')


def _write_transfer_details(session: MercantilSession,
                            transfers: List[Transfer],
                            transfer_results: List[TransferResult]) -> None:
    _driver, _frame_stk, _logger = session._get()
    cur = 0
    for i, t in enumerate(transfers):
        is_last = (i + 1) == len(transfers)
        transfer_results[i] = _write_single_transfer_details(
            session, t, cur, is_last)
        # session.report(transfers[i].id, TransferDoneStatus())
        if cur > 0 or isinstance(transfer_results[i], TransferNotCompleted):
            cur += 1


def _get_transfers_result(session: MercantilSession, res: List[TransferResult],
                          name: str) -> None:
    driver, frame_stk, logger = session._get()
    logger.debug('get results')

    def get_error_msg(cur: int) -> str:

        if not test_for_xpath(
                driver,
                f'/html/body/table/tbody/tr/td/table[3]/tbody/tr[{cur}]/td[5]/a'
        ):
            return ''
        e = find_by_xpath(
            driver,
            f'/html/body/table/tbody/tr/td/table[3]/tbody/tr[{cur}]/td[5]/a')
        s = e.get_attribute('onmouseover')

        showHelp_args = s[s.find('showHelp(') + len('showHelp('):-(len(');'))]
        ret = ' - '.join(showHelp_args.split(',')[1:3]).replace('\'',
                                                                '').strip()
        logger.error(f'error msg: {ret}')
        # turns something like:
        # "getMouseXY( event ); showHelp('error_popup', 'Error','El monto
        # máximo diario ...', 175 );"
        # onmouseout="hideHelp('error_popup')"
        #
        # into: "Error - El monto máximo diario ..."
        return ret

    cur = 1
    for i in range(len(res)):
        logger.debug(res[i])
        id = res[i].id
        if isinstance(res[i], TransferError):
            continue
        try:
            ref = find_by_xpath(driver,
                                ('/html/body/table/tbody/tr/td/table[3]/'
                                 f'tbody/tr[{cur}]/td[5]/p'),
                                timeout=2).text

            if 'Error' in ref:
                msg = 'Error al completar la transferencia. ' + get_error_msg(
                    cur)
                # FIXME Please, please, make a screenshot function, please
                e = find_by_xpath(
                    driver,
                    f'/html/body/table/tbody/tr/td/table[3]/tbody/tr[{cur}]')
                screenshot_path = (f'screenshots/mercantil_{name}-'
                                   f'{ref}-{res[i].id}.png')
                os.makedirs('screenshots', exist_ok=True)
                e.screenshot(screenshot_path)
                sshot = None
                try:
                    sshot = e.screenshot_as_base64
                except Exception:  # FIXME except Exception
                    pass
                res[i] = TransferError(sshot, id, msg)
                session.report([id], TransferDoneStatus())
            else:
                logger.debug(f'click share result: idShareEnabled{cur-1}')
                find_by_id(driver, f'idShareEnabled{cur-1}').click()

                logger.debug('switch frame')
                wait_for_id(driver, 'TB_iframeContent')
                frame_stk.switch_to(driver, By.ID, 'TB_iframeContent')

                # option 1: xpath /html/body (whole modal)
                # option 2: xpath /html/body/table (just the info)
                logger.debug('find element.')
                e = find_by_xpath(driver, '/html/body/table')
                screenshot_path = (f'screenshots/mercantil_{name}-'
                                   f'{ref}-{res[i].id}.png')
                os.makedirs('screenshots', exist_ok=True)
                e.screenshot(screenshot_path)
                time.sleep(1)
                logger.debug(screenshot_path)
                sshot = None
                logger.debug('take screenshot')
                try:
                    sshot = e.screenshot_as_base64
                    time.sleep(1)
                except Exception:  # FIXME except Exception
                    pass
                res[i] = TransferSuccess(sshot, id, ref)
                session.report([id], TransferDoneStatus())

                logger.debug('close modal')
                find_by_xpath(driver, '/html/body/div[13]/div').click()
                logger.debug('restore frame')

                # pop top
                frame_stk.frame_stack = frame_stk.frame_stack[:-1]
                frame_stk.restore(driver)

                logger.debug('cool')

        except Exception as e:  # FIXME except Exception
            logger.exception('error getting result')
            screenshot, sshot = _take_error_screenshot(session, f'-result-{i}')
            if screenshot is None:
                screenshot = 'Couldn\'t take screenshot.'
            res[i] = TransferError(sshot, id, f'Error getting result: {e}')

        cur += 1


def _write_otp(session: MercantilSession, otp):
    driver, _frame_stk, logger = session._get()
    logger.debug('writing otp')
    e = find_by_id(driver, 'keyInput')
    time.sleep(0.5)
    e.send_keys(otp)
    time.sleep(0.5)
    e = find_by_id(driver, 'accept_btn')
    e.click()
    logger.debug('otp written')


def _select_to_phone(session: MercantilSession) -> None:
    driver, _frame_stk, _logger = session._get()
    e = find_by_xpath(
        driver, '/html/body/table/tbody/tr/td/table[2]/tbody/tr[2]/td/input[1]')
    # >>> e.get_attribute('value')
    # 'C'
    e.click()
    e = find_by_id(driver, 'next_btn')
    e.click()


def _catch_OTPError(e: Union[OTPOtherError, OTPTimeoutError],
                    results: List[TransferResult]) -> None:
    for i in range(len(results)):
        tr = results[i]
        if isinstance(tr, TransferNotCompleted):
            results[i] = TransferError(None, tr.id, e.msg())


def _catch_OTPIncorrectError(e: OTPIncorrectError,
                             results: List[TransferResult]) -> None:
    for i in range(len(results)):
        tr = results[i]
        if isinstance(tr, TransferNotCompleted):
            results[i] = TransferError(None, tr.id, e.msg())


###############################################################################
# transfer same bank
###############################################################################


def _SBT_finish_writing_details(session: MercantilSession):
    driver, _frame_stk, _logger = session._get()
    find_by_xpath(
        driver,
        '//*[@id="transferTercerosForm"]/table[3]/tbody/tr/td/input').click()


def _SBT_confirm_transfer(session: MercantilSession):
    driver, _frame_stk, _logger = session._get()
    try:
        e = find_by_xpath(
            driver, '/html/body/table/tbody/tr/td/table[3]/tbody/tr[2]/td/a[2]')
        e.click()
    except TimeoutException:
        if test_for_id(driver, 'summary'):
            raise OTPIncorrectError()


def _SBT_get_transfer(session: MercantilSession, get_otp: Callable[[], str],
                      transfers: List[Transfer],
                      results: List[TransferResult]) -> None:
    _driver, _frame_stk, logger = session._get()
    try:
        # BUG need to catch go_to_transfer
        session.report(list(map(lambda t: t.id, transfers)),
                       TransferStartStatus())

        _go_to_transfer(session, 20, 43)

        _write_transfer_details(session, transfers, results)
        errors = len(
            list(filter(lambda x: isinstance(x, TransferError), results)))
        if len(results) == errors:
            return

        _SBT_finish_writing_details(session)
        _select_to_phone(session)

        try:
            for t in transfers:
                session.report([t.id], OTPStatus())
            otp = _get_otp_while_keep_alive(session, get_otp)
        except (OTPTimeoutError, OTPOtherError) as e:
            logger.error(f'OTP Timeout Error {e}')
            _catch_OTPError(e, results)
            return

        _write_otp(session, otp)

        try:
            _SBT_confirm_transfer(session)
        except OTPIncorrectError as e:
            logger.error(f'OTP Incorrect Error {e}')
            return _catch_OTPIncorrectError(e, results)

        _get_transfers_result(session, results, 'mismo_banco')

    except Exception:  # FIXME except Exception
        screenshot_path, sshot = _take_error_screenshot(session)
        scr_msg = ''
        if screenshot_path is not None:
            scr_msg = f' Error screenshot save at {screenshot_path}.'
        logger.info('trying to quit session after error')
        try:
            _quit_session_and_close(session)
        except Exception:  # FIXME except Exception
            pass
        logger.exception(f'Exception making same bank transfer.{scr_msg}')
        raise
        # TODO


###############################################################################
# transfer other banks
###############################################################################


def _OBT_finish_writing_details(session: MercantilSession) -> None:
    driver, _frame_stk, _logger = session._get()
    find_by_xpath(driver, ('//*[@id="transferOtherBanksForm"]/'
                           'table[3]/tbody/tr/td/input')).click()


def _OBT_confirm_transfer(session: MercantilSession):
    driver, _frame_stk, _logger = session._get()
    try:
        e = find_by_xpath(
            driver, '/html/body/table/tbody/tr/td/table[3]/tbody/tr[4]/td/a[2]')
        e.click()
    except TimeoutException:
        if test_for_id(driver, 'summary'):
            raise OTPIncorrectError()


def _OBT_get_transfer(session: MercantilSession, get_otp: Callable[[], str],
                      transfers: List[Transfer],
                      results: List[TransferResult]) -> None:
    _driver, _frame_stk, logger = session._get()
    try:
        # BUG need to catch go_to_transfer
        session.report(list(map(lambda t: t.id, transfers)),
                       TransferStartStatus())

        _go_to_transfer(session, 20, 44)

        _write_transfer_details(session, transfers, results)
        errors = len(
            list(filter(lambda x: isinstance(x, TransferError), results)))
        if len(results) == errors:
            return

        _OBT_finish_writing_details(session)
        _select_to_phone(session)

        try:
            for t in transfers:
                session.report([t.id], OTPStatus())
            otp = _get_otp_while_keep_alive(session, get_otp)
        except (OTPTimeoutError, OTPOtherError) as e:
            logger.error(f'OTP Timeout Error {e}')
            return _catch_OTPError(e, results)

        _write_otp(session, otp)

        try:
            _OBT_confirm_transfer(session)
        except OTPIncorrectError as e:
            logger.error(f'OTP Incorrect Error {e}')
            return _catch_OTPIncorrectError(e, results)

        _get_transfers_result(session, results, 'otro_banco')

    except Exception:  # FIXME except Exception
        screenshot_path, sshot = _take_error_screenshot(session)
        scr_msg = ''
        if screenshot_path is not None:
            scr_msg = f' Error screenshot save at {screenshot_path}.'
        logger.info('trying to quit session after error')
        try:
            _quit_session_and_close(session)
        except Exception:  # FIXME except Exception
            pass
        logger.exception(f'Exception making same bank transfer.{scr_msg}')
        raise
        # TODO


###############################################################################
# transfer mobile
###############################################################################


def _MT_fill_details(session: MercantilSession, t: Transfer) -> None:
    driver, _frame_stk, _logger = session._get()
    amount = t.amount
    concept = t.concept
    recipient = t.recipient

    # a un movil
    find_by_xpath(driver, '//*[@id="tipoPago"]/option[2]').click()

    # prefijo
    pref_found = False
    for i in range(2, 7):
        e = find_by_xpath(driver, f'//*[@id="precelphone"]/option[{i}]')
        if e.text == recipient.number_pref:
            e.click()
            pref_found = True
            break

    assert (pref_found)  # TODO

    find_by_id(driver, 'celphone').send_keys(recipient.number)

    list_of_banks = find_by_id(driver, 'bancos')
    soup = BeautifulSoup(list_of_banks.get_attribute('innerHTML'),
                         features="html.parser")
    options = soup.find_all('option')
    bank_found = False
    bank_code = recipient.bank.name[1:]
    for i, op in enumerate(options):
        if op.contents == []:
            continue
        if bank_code in op.attrs['value']:
            bank_found = True
            find_by_xpath(driver, f'//*[@id="bancos"]/option[{i+1}]').click()
            break

    if not bank_found:
        error_msg = f'Bank={recipient.bank} not found'
        raise Exception(error_msg)  # FIXME riase Exception

    # idType
    id_type_found = False
    for i in range(2, 8):
        e = find_by_xpath(driver, f'//*[@id="idType"]/option[{i}]')
        e.text
        if e.text == recipient.id_type.name:
            e.click()
            id_type_found = True
            break

    assert (id_type_found)  # TODO improve error

    find_by_id(driver, 'idNumber').send_keys(recipient.identification)

    find_by_xpath(driver, '//*[@id="accountNumber"]/option[2]').click()

    find_by_id(driver, 'amount').send_keys(amount)
    find_by_id(driver, 'concept').send_keys(concept)

    find_by_id(driver, 'accept_btn').click()


def _MT_single_transfer(session: MercantilSession, t: Transfer,
                        t_id: int) -> TransferResult:
    driver, _frame_stk, _logger = session._get()
    try:
        session.report([t.id], TransferStartStatus())
        _go_to_transfer(session, 22, 65)

        _MT_fill_details(session, t)
        find_by_id(driver, 'accept_btn').click()

        if wait_for_case(driver, By.ID, 'keyInput', By.ID, 'sendPagoForm', 15):
            # clave telefonica
            mobile_pw = session.credentials.mobile_pw
            if mobile_pw is None:
                return TransferError(None, t.id,
                                     'mobile_pw was needed and wasn\'t given')
            find_by_id(driver, 'keyInput').send_keys(mobile_pw)
            find_by_id(driver, 'accept_btn').click()

        ref = find_by_xpath(
            driver, '//*[@id="sendPagoForm"]/table[2]/tbody/tr[2]/td[2]').text
        e = find_by_id(driver, 'sendPagoForm')

        os.makedirs('screenshots', exist_ok=True)
        screenshot_path = f'screenshots/pantallazo_mercantil_movil_{ref}.png'
        e.screenshot(screenshot_path)
        sshot = None
        try:
            sshot = e.screenshot_as_base64
        except Exception:  # FIXME except Exception
            pass
        session.report([t.id], TransferDoneStatus())
        return TransferSuccess(sshot, t.id, ref)
    except Exception as e:  # FIXME except Exception
        screenshot, sshot = _take_error_screenshot(session, f'-{t_id}')
        if screenshot is None:
            screenshot = 'Couldn\'t take screenshot.'
        session.report([t.id], TransferDoneStatus())
        res = TransferError(sshot, t.id, str(e))
        return res


def _MT_get_transfer(session: MercantilSession, transfers: List[Transfer],
                     results: List[TransferResult]) -> None:
    _driver, _frame_stk, logger = session._get()
    try:
        for i, t in enumerate(transfers):
            results[i] = _MT_single_transfer(session, t, i)
            session.report([t.id], TransferDoneStatus())

    except Exception:  # FIXME except Exception
        screenshot_path, sshot = _take_error_screenshot(session)
        scr_msg = ''
        if screenshot_path is not None:
            scr_msg = f' Error screenshot save at {screenshot_path}.'
        logger.info('trying to quit session after error')
        try:
            _quit_session_and_close(session)
        except Exception:  # FIXME except Exception
            pass
        logger.exception(f'Exception making transfers.{scr_msg}')


###############################################################################
# Get balance
###############################################################################


def _moveToTabAndEnterState(session: MercantilSession) -> None:
    driver, frame_stk, logger = session._get()
    logger.info('go to main account')
    logger.debug('switch to leftFrame')
    frame_stk.switch_to(driver, By.ID, 'leftFrame')
    logger.debug('click id=lnk1')
    find_by_xpath(driver, "//*[@id='lnk1']").click()
    frame_stk.switch_to_default(driver)

    logger.debug('switch to frame body')
    frame_stk.switch_to(driver, By.ID, 'frameBody')

    logger.debug(
        '> wait for /html/body/table/tbody/tr/td/table[3]/tbody/tr[3]/td[4]/a')
    # FIXME this seems to raise a
    # selenium.common.exceptions.NoSuchWindowException: Message: no such window
    # the misteries
    for i in range(2):
        try:
            wait_for_xpath(
                driver,
                '/html/body/table/tbody/tr/td/table[3]/tbody/tr[3]/td[4]/a'
            ).click()
            logger.debug('click it')
            break
        except Exception:
            if i == 1:
                raise
            time.sleep(2)


def _get_balance(session: MercantilSession) -> Tuple[str, str, str]:
    driver, _frame_stk, logger = session._get()
    logger.debug('waiting for the balance')
    wait_for_xpath(
        driver, '/html/body/table/tbody/tr/td/form/table[2]/tbody/tr[2]/td[1]')

    def aux_get(x):
        return find_by_xpath(
            driver,
            f'/html/body/table/tbody/tr/td/form/table[2]/tbody/tr[2]/td[{x}]'
        ).text

    logger.debug('getting the balance')
    balance = aux_get(1)
    deferred = aux_get(3)
    blocked = aux_get(4)
    logger.info('got balances')
    return (balance, deferred, blocked)


def _select_previous_month(session: MercantilSession) -> None:
    driver, _frame_stk, _logger = session._get()
    i, month = _get_VE_prev_month()
    e = find_by_xpath(driver, f'//*[@id="tdSelect2"]/select/option[{i+1}]')
    assert (e.text == month)
    e.click()


def _get_transactions(session: MercantilSession) -> List[Transaction]:
    driver, _frame_stk, logger = session._get()
    logger.info('waiting for transactions')
    transactions_table = wait_for_xpath(driver, '//*[@id="tblDatos"]/tbody')
    logger.debug('processing transactions')
    soup = BeautifulSoup(transactions_table.get_attribute('innerHTML'),
                         features="html.parser")
    t_rows = soup.find_all('tr')
    transactions = []
    for t_row in t_rows:
        try:
            tds = t_row.find_all('td')
            a_out = tds[3].contents[0].strip()
            a_in = tds[4].contents[0].strip()
            if a_out == '':
                amount = a_in
                direc = '+'
            else:
                assert a_out[0] == '-'
                amount = a_out[1:]
                direc = '-'
            transactions.append(
                Transaction(
                    date=datetime.strptime(tds[0].contents[0].strip(),
                                           '%d/%m/%Y').date(),
                    description=tds[2].contents[0].strip(),
                    reference=tds[1].find_all('a')[0].contents[0].strip(),
                    amount=amount,
                    direction=direc))
        except Exception:  # FIXME except Exception
            pass
            # TODO I think sometimes the last <tr> is empty, I have to re-check
            # maybe only when one page(?)
            # upd: seems like it's never empty,

    logger.debug('finish processing transactions')
    return list(reversed(transactions))


def _get_balance_and_transactions(session: MercantilSession,
                                  query_interval: QueryInterval) -> QueryResult:
    driver, frame_stk, logger = session._get()
    frame_stk.switch_to_default(driver)
    logger.debug('move to account')
    session.report_all(ConsultStatus())
    _moveToTabAndEnterState(session)

    logger.info('getting balance')
    balances = _get_balance(session)

    logger.debug('getting movements')
    try:
        transactions = _get_transactions(session)
    except Exception:  # FIXME except Exception
        transactions = []

    if query_interval == QueryInterval.LAST_TWO_MONTHS:
        _select_previous_month(session)
        try:
            older_transactions = _get_transactions(session)
            transactions = older_transactions + transactions
        except Exception:  # FIXME except Exception
            pass
    return (balances, transactions)


if __name__ == '__main__':
    # print("que bueno pana!")
    # pw = 'danielvarela'
    # creds = Credentials.from_encrypted_YAML_file(
    #     'credentials/credentials_alex.yaml.encr', pw)
    # print("todo fino mi pana!")

    # with MercantilSession(creds) as session:
    #    bals, trans = session.get_balance_and_transactions(
    #        QueryInterval.LAST_DAY)
    #    print("terminamos mi pana!")
    #    print(bals)

    pw = os.environ['CRED_ENCR_PW']
    creds = Credentials.from_encrypted_YAML_file(
        'credentials/credentials_mercantil.yaml.encr', pw)

    # from ..testing_recipients import sara_mercantil, papa_banesco

    # tsb = Transfer(amount='123,69',
    #                recipient=sara_mercantil,
    #                concept='prueba1',
    #                id='mert1')
    # tsbres: List[TransferResult] = [TransferNotStarted('mert1')]
    # tob = Transfer(amount='123,69',
    #                recipient=papa_banesco,
    #                concept='prueba2',
    #                id='mert2')
    # tobres: List[TransferResult] = [TransferNotStarted('mert2')]

    # def _get_otp():
    #     otp = input('otp: ')
    #     if otp == '':
    #         raise OTPTimeoutError()
    #     return otp

    # with MercantilSession(creds, lambda: 'foo') as session:
    # session.make_same_bank_transfer([tsb], _get_otp, tsbres)
    # session.make_other_bank_transfer([tob], _get_otp, tobres)
    # bals, trans = session.get_balance_and_transactions(
    #     QueryInterval.LAST_DAY)
    # print(bals)
    # ln = 0
    # if trans is not None:
    #     ln = len(trans)
    # print(f'{ln} transaction')
