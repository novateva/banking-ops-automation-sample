"""BanescoSession."""

from selenium.webdriver.chrome.webdriver import WebDriver as Chrome
from selenium.webdriver.common.by import By
from selenium.common.exceptions import (  # noqa: F401
    ElementNotInteractableException, ElementClickInterceptedException,
    TimeoutException, InvalidSessionIdException)
from bs4 import BeautifulSoup

import time
import os
import threading
from datetime import datetime

from typing import List, Tuple, Optional, Callable
import logging

from ..exceptions import (DriverError, LoginError, OTPTimeoutError,
                          OTPOtherError, OTPIncorrectError, BankSpecificError)
from .logger import get_logger
from .selenium_utils import (find_by_xpath, find_by_id, test_for_id,
                             wait_for_xpath, wait_for_id, wait_for_case,
                             test_for_xpath, wait_for_presence,
                             test_for_presence)
from . import selenium_utils
from .frame_stack import FrameStack
from .bank_session import BankSession

from ..transfer import (Transfer, TransferNotStarted, TransferResult,
                        TransferSuccess, TransferError)
from ..status import (Status, LoginStatus, LogoutStatus, ConsultStatus,
                      OTPStatus, TransferStartStatus, TransferDoneStatus,
                      WaitingTransfersStatus)
from ..query import Transaction, QueryInterval, QueryResult
from ..credentials import Credentials
from ..utils import get_VE_time


class BanescoSession(BankSession):
    """Class to handle banesco sessions with selenium.

    Use as a context manager ::
        with BanescoSession(credentials) as session:
            ...
    """

    credentials: Credentials
    logger: logging.Logger
    frame_stk: FrameStack
    driver: Chrome
    otp: Optional[str]
    get_otp: Callable[[], str]
    report: Callable[[List[str], Status], None]
    report_all: Callable[[Status], None]

    def __init__(self, credentials: Credentials, get_otp: Callable[[], str],
                 report: Callable[[List[str], Status], None],
                 report_all: Callable[[Status], None]) -> None:
        """BanescoSession, use as context manager."""
        self.credentials = credentials
        self.logger = get_logger(f'BAN-{credentials.uid}', logging.DEBUG)
        self.frame_stk = FrameStack()
        self.otp = None
        self.get_otp = self._make_get_otp_persistent(get_otp)
        self.report = report
        self.report_all = report_all

    def __enter__(self) -> "BanescoSession":
        """Open session, see open()."""
        for tries in range(2):
            try:
                self.open()
                return self
            except Exception:
                if tries == 1:
                    raise
                time.sleep(5)
                try:
                    self.driver.close()
                except (InvalidSessionIdException, AttributeError):
                    pass
        raise Exception('This is impossible.')

    # Literal[True] would be better return type but it's not in 3.7
    def __exit__(self, type, value, traceback) -> Optional[bool]:
        """Close session, see close()."""
        if type is None:
            self.close()
            return True
        else:
            screenshot_path, sshot = _take_error_screenshot(self)

            scr_msg = ''
            if screenshot_path is not None:
                scr_msg = f' Error screenshot save at {screenshot_path}.'

            self.logger.error('trying to quit session after error.')
            try:
                _quit_session_and_close(self)
            except Exception:  # FIXME: except Exception
                pass
            self.logger.exception(
                f'Exception getting balance and transactions.{scr_msg}')
            return None

    def open(self) -> None:
        """Get a webdriver and login."""
        try:
            self.driver = _get_driver(self.logger)
            _login(self)
            self.report_all(WaitingTransfersStatus())
        except (DriverError, LoginError) as e:
            self.logger.error(e)
            raise
        except Exception:  # FIXME except exception
            self.logger.exception('catch specific exception')
            raise

    def close(self):
        """Logout and quit session."""
        self.logger.debug('quitting session')
        _quit_session_and_close(self)

    def _get(self) -> Tuple[Chrome, FrameStack, logging.Logger]:
        return (self.driver, self.frame_stk, self.logger)

    def get_balance_and_transactions(
            self, query_interval: QueryInterval) -> QueryResult:
        """Get balance and transactions."""
        return _get_balance_and_transactions(self, query_interval)

    def _make_get_otp_persistent(
            self, get_otp: Callable[[], str]) -> Callable[[], str]:

        def persistent_get_otp():
            if self.otp is None:
                self.otp = get_otp()
                self.logger.info(f'received otp {self.otp}')
            return self.otp

        return persistent_get_otp

    def make_same_bank_transfer(self, transfers: List[Transfer],
                                transfer_results: List[TransferResult]) -> None:
        """Make a list of transfers."""
        _SBT_get_transfers(self, transfers, transfer_results)

    def make_other_bank_transfer(
            self, transfers: List[Transfer],
            transfer_results: List[TransferResult]) -> None:
        """Make a list of transfers."""
        _OBT_get_transfers(self, transfers, transfer_results)

    def make_mobile_transfer(self, transfers: List[Transfer],
                             transfer_results: List[TransferResult]) -> None:
        """Make a list of transfers."""
        _MT_get_transfers(self, transfers, transfer_results)


###############################################################################
###############################################################################
# Internal stuff starts here
###############################################################################
###############################################################################

###############################################################################
# Login, Quit session, etc
###############################################################################


def _get_otp_while_keep_alive(session: BanescoSession,
                              get_otp: Callable[[], str]) -> str:
    """Keep the session alive while waiting for otp.

    Call the function keep_alive every 10 seconds, while waiting for
    get_otp to return the wanted otp.
    """
    otp_received = threading.Event()

    def keep_alive_while_waiting():
        cnt = 0
        while not otp_received.is_set():
            cnt += 1
            if cnt == 10:
                _keep_alive(session)
                cnt = 0
            time.sleep(1)

    keep_alive_thread = threading.Thread(target=keep_alive_while_waiting)
    keep_alive_thread.start()

    try:
        otp = get_otp()
    except (OTPTimeoutError, OTPOtherError):
        otp_received.set()
        keep_alive_thread.join()
        raise
    except Exception as e:  # FIXME except Exception (maybe valid)
        session.logger.exception(f'Unexpected exception getting otp: {e}')
        otp_received.set()
        keep_alive_thread.join()
        raise

    otp_received.set()
    keep_alive_thread.join()
    return otp


def _keep_alive(session: BanescoSession) -> None:
    """Check if page is asking keep alive dialog and click yes if so."""
    driver, frame_stk, _logger = session._get()
    if (len(driver.window_handles) == 1):
        return
    driver.switch_to.window(driver.window_handles[1])
    find_by_xpath(
        driver, '//*[@id="formBOL"]/center/table[2]/tbody/tr/td/input').click()
    driver.switch_to.window(driver.window_handles[0])
    frame_stk.restore(driver)


def _quit_session_and_close(session: BanescoSession) -> None:
    driver, frame_stk, logger = session._get()
    """Click exit."""
    session.report_all(LogoutStatus())
    frame_stk.switch_to_default(driver)
    wait_for_id(driver, 'ctl00_btnSalir_lkButton').click()
    wait_for_id(driver, 'Cuadro_btnOk')
    logger.info('log out')
    driver.close()


def _take_error_screenshot(
        session: BanescoSession,
        msg: str = '') -> Tuple[Optional[str], Optional[str]]:
    driver, _frame_stk, _logger = session._get()
    directory = 'logs/screenshots'
    file_name = f'{get_VE_time()}-{os.getpid()}{msg}-banesco.png'
    os.makedirs(directory, exist_ok=True)
    screenshot_path = f'{directory}/{file_name}'
    try:
        screenshot = driver.get_screenshot_as_file(screenshot_path)
        sshot = driver.get_screenshot_as_base64()
    except Exception:  # FIXME: Except exception
        screenshot = False
        sshot = None
    if screenshot:
        return screenshot_path, sshot
    else:
        return None, sshot


def _get_driver(logger: logging.Logger) -> Chrome:
    """Create a new chrome driver and open banesco."""
    try:
        logger.info('opening driver')
        driver = selenium_utils.get_driver()
        driver.get("https://www.banesconline.com/mantis/WebSite/Default.aspx")
        return driver
    except Exception:  # FIXME: Except exception
        logger.exception('Couldn\'t open driver')
        raise DriverError()


def _login(session: BanescoSession) -> None:
    """Login to banesco, answering secret questions."""
    driver, frame_stk, logger = session._get()
    credentials = session.credentials
    try:
        logger.info('login')
        session.report_all(LoginStatus())
        uid = credentials.uid
        pw = credentials.pw
        answers = credentials.answers
        frame_stk.switch_to(driver, By.ID, 'ctl00_cp_frmAplicacion')

        # user page
        logger.debug('username')
        find_by_id(driver, 'txtUsuario').send_keys(uid)
        find_by_id(driver, 'bAceptar').click()

        # questions page
        def answerQuestion(driver, qid, aid):
            qtext = find_by_id(driver, qid).text
            for (q, a) in answers:
                if q in qtext:
                    find_by_id(driver, aid).send_keys(a)
                    return
            logger.error(f'dont know aswer to {qtext}')
            raise LoginError(f'dont know aswer to {qtext}')

        if wait_for_case(driver, By.ID, 'lblPrimeraP', By.ID, 'txtClave'):
            logger.debug('questions')
            answerQuestion(driver, 'lblPrimeraP', 'txtPrimeraR')
            answerQuestion(driver, 'lblSegundaP', 'txtSegundaR')
            find_by_id(driver, 'bAceptar').click()

        # password page
        logger.debug('password')
        wait_for_id(driver, 'txtClave')

        find_by_id(driver, 'txtClave').send_keys(pw)
        find_by_id(driver, 'bAceptar').click()

        frame_stk.switch_to_default(driver)
    except Exception:  # FIXME: except Exception
        logger.exception('Couldn\'t log in')
        raise


###############################################################################
# transfers
###############################################################################


def _go_to_transfer(session: BanescoSession, link_pos) -> None:
    driver, _frame_stk, _logger = session._get()
    was_open = False
    try:
        driver.find_element_by_xpath(
            f'//*[@id="ctl00_FastMenu"]/div[2]/a[{link_pos}]').click()
        was_open = True
    except Exception:  # FIXME: except Exception
        pass
    if not was_open:
        find_by_id(driver, 'm_1').click()
        time.sleep(1)
        find_by_xpath(
            driver, f'//*[@id="ctl00_FastMenu"]/div[2]/a[{link_pos}]').click()


def _finish_transfer(session: BanescoSession):
    driver, _frame_stk, logger = session._get()
    logger.debug('Finish transfer')
    find_by_id(driver, 'ctl00_cp_wz_CRbo_btnReg').click()


def _get_transfer_result(session: BanescoSession, t_i: str,
                         name: str) -> TransferResult:
    """Take screenshot and get reference.

    Works for same bank and other banks
    """
    driver, _frame_stk, _logger = session._get()
    if wait_for_case(driver, By.ID, 'ctl00_cp_wz_validarCoe_wzCoeOtp_txtCoeOtp',
                     By.ID, 'ctl00_cp_wz_CRbo_lblRboNroRecibo'):
        raise OTPIncorrectError()

    ref = wait_for_id(driver, 'ctl00_cp_wz_CRbo_lblRboNroRecibo').text
    os.makedirs('screenshots', exist_ok=True)
    screenshot_path = f'screenshots/pantallazo_banesco_{name}_{ref}.png'
    e = find_by_xpath(driver, '//*[@id="ctl00_cp_wz"]/tbody/tr/td/div')
    e.screenshot(screenshot_path)
    sshot = None
    try:
        sshot = e.screenshot_as_base64
    except Exception:  # FIXME: except Exception
        pass
    return TransferSuccess(sshot, t_i, ref)


GetSingleTransfer = Callable[[BanescoSession, str, Transfer], TransferResult]


def _get_many_transfers(session: BanescoSession,
                        get_single_transfer: GetSingleTransfer,
                        transfers: List[Transfer],
                        results: List[TransferResult]) -> None:
    _driver, _frame_stk, logger = session._get()
    try:
        for i, t in enumerate(transfers):
            results[i] = get_single_transfer(session, t.id, t)
            session.report([t.id], TransferDoneStatus())

    except Exception:  # FIXME: except Exception
        screenshot_path, sshot = _take_error_screenshot(session)
        scr_msg = ''
        if screenshot_path is not None:
            scr_msg = f' Error screenshot save at {screenshot_path}.'
        # FIXME: confirm_refs is not empty?
        try:
            _quit_session_and_close(session)
        except Exception:  # FIXME: except Exception
            pass
        logger.exception(f'Exception making transfers.{scr_msg}')

        logger.info('trying to quit session after error')


def _get_transfer_error(session, t_i: str, msg: str) -> TransferError:
    screenshot, sshot = _take_error_screenshot(session, f'-{t_i}')
    if screenshot is None:
        screenshot = 'Couldn\'t take screenshot.'
    res = TransferError(sshot, t_i, msg)
    return res


def _catch_specific_error(session: BanescoSession, tid: str,
                          e: BankSpecificError) -> TransferError:
    driver, _frame_stk, logger = session._get()
    sshot = None
    by, loc = e.by, e.loc
    if by and loc:
        directory = 'logs/screenshots'
        file_name = f'{get_VE_time()}-banesco.png'
        screenshot_path = f'{directory}/{file_name}'
        try:
            os.makedirs(directory, exist_ok=True)
            if test_for_presence(driver, by, loc, timeout=1):
                el = wait_for_presence(driver, by, loc, timeout=1)
                el.screenshot(screenshot_path)
                sshot = el.screenshot_as_base64
                logger.debug(f'screenshot saved at {screenshot_path}')
        except Exception:
            logger.exception(
                'Error trying to take screenshot of specific error.')

    return TransferError(sshot, tid, e.message)


###############################################################################
# transfer same bank
###############################################################################


def _SBT_write_transfer_details(session: BanescoSession,
                                transfer: Transfer) -> None:
    driver, _frame_stk, _logger = session._get()
    amount = transfer.amount
    concept = transfer.concept
    recipient = transfer.recipient

    find_by_xpath(driver,
                  '//*[@id="ctl00_cp_wz_ddlCuentaDebitar"]/option[2]').click()
    find_by_id(driver,
               'ctl00_cp_wz_txtCuentaTransferir').send_keys(recipient.number)

    id_type_found = False
    # TODO Use BeautifulSoup to get this option
    for i in range(2, 7):
        elem = find_by_xpath(driver,
                             f'//*[@id="ctl00_cp_wz_ddlNac"]/option[{i}]')
        if elem.text == recipient.id_type.name:
            id_type_found = True
            elem.click()
            break

    if not id_type_found:
        error_msg = f'IdType={recipient.id_type!r} not found'
        raise Exception(error_msg)  # FIXME: raise Exception

    find_by_id(driver,
               'ctl00_cp_wz_txtCedula').send_keys(recipient.identification)

    find_by_id(driver, 'ctl00_cp_wz_txtMonto').send_keys(amount)
    find_by_id(driver, 'ctl00_cp_wz_txtConcepto').send_keys(concept)

    find_by_id(
        driver,
        'ctl00_cp_wz_StartNavigationTemplateContainerID_btnNext').click()
    time.sleep(1)
    if test_for_id(driver, 'ctl00_cp_wz_ctl12'):
        if find_by_id(driver, 'ctl00_cp_wz_ctl12').text != '':
            raise BankSpecificError(
                By.ID, 'ctl00_cp_wz_ctl12',
                'Error escribiendo datos de transferencia.')


def _confirm_transfer(session: BanescoSession) -> None:
    driver, _frame_stk, _logger = session._get()
    wait_for_id(
        driver,
        'ctl00_cp_wz_StepNavigationTemplateContainerID_btnNext').click()


def _SBT_make_single_transfer(session: BanescoSession, get_otp: Callable[[],
                                                                         str],
                              transfer: Transfer) -> None:
    driver, _frame_stk, _logger = session._get()
    session.report([transfer.id], TransferStartStatus())
    _go_to_transfer(session, 2)
    _SBT_write_transfer_details(session, transfer)

    if wait_for_case(driver, By.ID,
                     'ctl00_cp_wz_StartNavigationTemplateContainerID_btnNext',
                     By.ID,
                     'ctl00_cp_wz_StepNavigationTemplateContainerID_btnNext'):
        need_otp = True
    else:
        need_otp = False

    if not need_otp:
        _confirm_transfer(session)
        return

    # extra confirmar cuando no esta registrado
    wait_for_id(
        driver,
        'ctl00_cp_wz_StartNavigationTemplateContainerID_btnNext').click()

    _confirm_transfer(session)

    # guardar nombre
    wait_for_id(driver,
                'ctl00_cp_wz_validarCoe_wzCoeOtp_DirData_txtAlias').send_keys(
                    transfer.recipient.description)
    btnNext_id = ('ctl00_cp_wz_validarCoe_wzCoeOtp_'
                  'StepNavigationTemplateContainerID_btnNext')
    find_by_id(driver, btnNext_id).click()

    # confirmar anadir a libreta
    wait_for_id(driver, btnNext_id).click()

    # buscar sms
    wait_for_id(driver, btnNext_id).click()

    session.report([transfer.id], OTPStatus())
    otp = _get_otp_while_keep_alive(session, get_otp)

    # escribir sms
    find_by_id(driver,
               'ctl00_cp_wz_validarCoe_wzCoeOtp_txtCoeOtp').send_keys(otp)
    find_by_id(driver, ('ctl00_cp_wz_FinishNavigationTemplateContainerID_'
                        'btnFinishComp')).click()


def _SBT_get_single_transfer(session: BanescoSession, get_otp: Callable[[],
                                                                        str],
                             t_i: str, transfer: Transfer) -> TransferResult:
    driver, _frame_stk, logger = session._get()
    try:
        _SBT_make_single_transfer(session, get_otp, transfer)
        transfer_result = _get_transfer_result(session, t_i, 'mismo_banco')
        _finish_transfer(session)
        return transfer_result
    except (OTPTimeoutError, OTPOtherError) as e:
        logger.error(f'OTP Error: {e}')
        return TransferError(None, transfer.id, e.msg())
    except OTPIncorrectError as e:
        session.otp = None
        logger.error(f'OTP Incorrect Error {e}')
        return TransferError(None, transfer.id, e.msg())
    except BankSpecificError as e:
        logger.error(f'Specific Error {e}')
        return _catch_specific_error(session, transfer.id, e)

    except Exception as e:  # FIXME: except Exception
        return _get_transfer_error(session, t_i, str(e))


def _SBT_get_transfers(session: BanescoSession, transfers: List[Transfer],
                       results: List[TransferResult]) -> None:
    get_otp = session.get_otp

    def f(s, i, t):
        return _SBT_get_single_transfer(s, get_otp, i, t)

    return _get_many_transfers(session, f, transfers, results)


###############################################################################
# transfer other banks
###############################################################################


def _OBT_fill_details(session: BanescoSession, transfer: Transfer) -> None:
    driver, _frame_stk, logger = session._get()
    logger.debug('Fill other bank transfer details')
    amount = transfer.amount
    concept = transfer.concept
    recipient = transfer.recipient
    find_by_xpath(driver,
                  '//*[@id="ctl00_cp_wz_ddlCuentaDebitar"]/option[2]').click()
    find_by_id(driver,
               'ctl00_cp_wz_txtCuentaTransferir').send_keys(recipient.number)
    find_by_id(driver, 'ctl00_cp_wz_txtMonto').send_keys(amount)
    find_by_id(driver, 'ctl00_cp_wz_txtConcepto').send_keys(concept)
    id_type_found = False
    # TODO Use BeautifulSoup to get this option
    for i in range(2, 7):
        elem = find_by_xpath(driver,
                             f'//*[@id="ctl00_cp_wz_ddlNac"]/option[{i}]')
        if elem.text == recipient.id_type.name:
            id_type_found = True
            elem.click()
            break
    if not id_type_found:
        error_msg = f'IdType={recipient.id_type} not found'
        raise Exception(error_msg)  # FIXME: raise Exception
    find_by_id(driver,
               'ctl00_cp_wz_txtCedula').send_keys(recipient.identification)
    find_by_id(driver, 'ctl00_cp_wz_txtBen').send_keys(recipient.name)
    bank_found = False
    list_of_banks = find_by_id(driver, 'ctl00_cp_wz_ddlBancos')
    soup = BeautifulSoup(list_of_banks.get_attribute('innerHTML'),
                         features="html.parser")
    options = soup.find_all('option')
    bank_code = recipient.bank.name[1:]
    for i, op in enumerate(options):
        if op.contents == []:
            continue
        if bank_code in op.attrs['value']:
            bank_found = True
            find_by_xpath(
                driver,
                f'//*[@id="ctl00_cp_wz_ddlBancos"]/option[{i+1}]').click()
            break
    if not bank_found:
        error_msg = f'Bank={recipient.bank} not found'
        raise Exception(error_msg)  # FIXME: raise Exception
    find_by_id(
        driver,
        'ctl00_cp_wz_StartNavigationTemplateContainerID_btnNext').click()

    time.sleep(1)
    if test_for_id(driver, 'ctl00_cp_wz_ctl17'):
        if find_by_id(driver, 'ctl00_cp_wz_ctl17').text != '':
            raise BankSpecificError(
                By.ID, 'ctl00_cp_wz_ctl17',
                'Error escribiendo datos de transferencia.')


def _OBT_make_single_transfer(session: BanescoSession, get_otp: Callable[[],
                                                                         str],
                              transfer: Transfer) -> None:
    driver, _frame_stk, logger = session._get()
    logger.debug('Go to other bank transfers')
    _go_to_transfer(session, 5)
    _OBT_fill_details(session, transfer)

    # confirmar datos
    find_by_id(driver,
               'ctl00_cp_wz_StepNavigationTemplateContainerID_btnNext').click()

    # test_for_id(driver, 'ctl00_cp_wz_validarCoe_wzCoeOtp_DirData_txtAlias')
    if wait_for_case(driver, By.ID,
                     'ctl00_cp_wz_validarCoe_wzCoeOtp_DirData_txtAlias', By.ID,
                     'ctl00_cp_wz_CRbo_lblRboNroRecibo'):
        logger.debug('We need otp')
        # add to directory
        find_by_id(
            driver,
            'ctl00_cp_wz_validarCoe_wzCoeOtp_DirData_txtAlias').send_keys(
                transfer.recipient.description)

        find_by_id(
            driver,
            ('ctl00_cp_wz_validarCoe_'
             'wzCoeOtp_StepNavigationTemplateContainerID_btnNext')).click()

        # confirmar anadir a directorio
        find_by_id(
            driver,
            ('ctl00_cp_wz_validarCoe_'
             'wzCoeOtp_StepNavigationTemplateContainerID_btnNext')).click()

        # confirmar buscar clave
        find_by_id(
            driver,
            ('ctl00_cp_wz_validarCoe_'
             'wzCoeOtp_StepNavigationTemplateContainerID_btnNext')).click()

        logger.debug('Ask otp')
        session.report([transfer.id], OTPStatus())
        otp = _get_otp_while_keep_alive(session, get_otp)
        # poner clave
        find_by_id(driver,
                   'ctl00_cp_wz_validarCoe_wzCoeOtp_txtCoeOtp').send_keys(otp)
        find_by_id(
            driver,
            'ctl00_cp_wz_FinishNavigationTemplateContainerID_btnFinishComp'
        ).click()
    else:
        logger.debug('We don\'t need otp')


def _OBT_get_single_transfer(session: BanescoSession, get_otp: Callable[[],
                                                                        str],
                             t_i: str, transfer: Transfer) -> TransferResult:
    driver, _frame_stk, logger = session._get()
    try:
        session.report([transfer.id], TransferStartStatus())
        _OBT_make_single_transfer(session, get_otp, transfer)
        transfer_result = _get_transfer_result(session, t_i, 'otro_banco')
        _finish_transfer(session)
        return transfer_result
    except (OTPTimeoutError, OTPOtherError) as e:
        logger.error(f'OTP Error {e}')
        return TransferError(None, transfer.id, e.msg())
    except OTPIncorrectError as e:
        session.otp = None
        logger.error(f'OTP Incorrect Error {e}')
        return TransferError(None, transfer.id, e.msg())
    except BankSpecificError as e:
        logger.error(f'Specific Error {e}')
        return _catch_specific_error(session, transfer.id, e)

    except Exception as e:  # FIXME: except Exception
        return _get_transfer_error(session, t_i, str(e))


def _OBT_get_transfers(session: BanescoSession, transfers: List[Transfer],
                       results: List[TransferResult]) -> None:
    get_otp = session.get_otp

    def f(s, i, t):
        return _OBT_get_single_transfer(s, get_otp, i, t)

    return _get_many_transfers(session, f, transfers, results)


###############################################################################
# transfer pago movil
###############################################################################


def _MT_get_result(session: BanescoSession, t_i) -> TransferResult:
    driver, _frame_stk, _logger = session._get()
    if wait_for_case(driver, By.XPATH, '/html/body/div[2]/div/div[1]/img',
                     By.ID, 'ctl00_cp_wz_CRbo_lblRboNroRecibo'):
        raise OTPIncorrectError()

    ref = find_by_id(driver, 'ctl00_cp_wz_CRbo_lblRboNroRecibo').text
    os.makedirs('screenshots', exist_ok=True)
    screenshot_path = f'screenshots/pantallazo_banesco_movil_{ref}.png'
    e = find_by_id(driver, 'areaImprimir')
    e.screenshot(screenshot_path)
    sshot = None
    try:
        sshot = e.screenshot_as_base64
    except Exception:  # FIXME: except Exception
        pass
    return TransferSuccess(sshot, t_i, ref)


def _MT_finish_transfer(session: BanescoSession) -> None:
    driver, _frame_stk, _logger = session._get()
    find_by_xpath(
        driver,
        '//*[@id="body"]/div/div[2]/div/div/table[2]/tbody/tr/td/input[2]'
    ).click()


def _MT_fill_details(session: BanescoSession, transfer: Transfer) -> None:
    driver, frame_stk, logger = session._get()
    amount = transfer.amount
    concept = transfer.concept
    recipient = transfer.recipient

    logger.debug('switch frame')
    frame_stk.switch_to(driver, By.ID, 'ctl00_cp_frmAplicacion')
    # TODO Use BeautifulSoup to get this option
    for i in range(1, 6):
        e = find_by_xpath(driver, f'//*[@id="pref"]/option[{i}]')
        if recipient.number_pref in e.get_attribute('value'):
            logger.debug('number pref found')
            e.click()

    logger.debug('write phone')
    find_by_id(driver, 'tel').send_keys(recipient.number)

    # TODO Use BeautifulSoup to get this option
    id_type_found = False
    for i in range(1, 5):
        e = find_by_xpath(driver, f'//*[@id="NacCli"]/option[{i}]')
        if recipient.id_type.name == e.get_attribute('value'):
            id_type_found = True
            e.click()
            break

    if not id_type_found:
        error_msg = f'IdType={recipient.id_type!r} not found'
        raise Exception(error_msg)  # FIXME: Raise Exception

    find_by_id(driver, 'ced').send_keys(recipient.identification)

    bank_found = False

    list_of_banks = find_by_id(driver, 'banco')
    soup = BeautifulSoup(list_of_banks.get_attribute('innerHTML'),
                         features="html.parser")
    options = soup.find_all('option')
    bank_code = recipient.bank.name[1:]

    for i, op in enumerate(options):
        if op.contents == []:
            continue
        if bank_code in op.attrs['value']:
            bank_found = True
            find_by_xpath(driver, f'//*[@id="banco"]/option[{i+1}]').click()
            break

    if not bank_found:
        error_msg = f'Bank={recipient.bank!r} not found'
        raise Exception(error_msg)  # FIXME: raise Exception

    find_by_id(driver, 'monto').send_keys(amount)
    find_by_id(driver, 'concepto').send_keys(concept)

    find_by_id(driver, 'enviar').click()

    time.sleep(1)
    if test_for_id(driver, 'enviar'):
        raise BankSpecificError(By.ID, 'form',
                                'Error escribiendo datos de transferencia.')


def _MT_make_single_transfer(session: BanescoSession, get_otp: Callable[[],
                                                                        str],
                             transfer: Transfer) -> None:
    driver, _frame_stk, logger = session._get()
    was_open = False
    try:
        driver.find_element_by_xpath(
            '//*[@id="ctl00_FastMenu"]/div[7]/a[3]').click()
        was_open = True
    except Exception:  # FIXME: except Exception
        pass

    if not was_open:
        find_by_id(driver, 'm_7').click()
        time.sleep(1)
        find_by_xpath(driver, '//*[@id="ctl00_FastMenu"]/div[7]/a[3]').click()

    logger.debug('go fill_details')
    _MT_fill_details(session, transfer)

    time.sleep(1)
    # confirm transfer
    find_by_id(driver, 'butonPago').click()

    if wait_for_case(driver, By.ID, 'butonOTP', By.ID,
                     'ctl00_cp_wz_CRbo_lblRboNroRecibo'):
        session.report([transfer.id], OTPStatus())
        otp = _get_otp_while_keep_alive(session, get_otp)
        driver.find_element_by_id('butonOTP').click()
        driver.find_element_by_xpath(
            '//*[@id="form"]/div[1]/div/input').send_keys(otp)
        driver.find_element_by_xpath(
            '//*[@id="form"]/div[2]/div/button[2]').click()


def _MT_get_single_transfer(session: BanescoSession, get_otp: Callable[[], str],
                            t_i: int, transfer: Transfer) -> TransferResult:
    driver, frame_stk, logger = session._get()
    try:
        _MT_make_single_transfer(session, get_otp, transfer)
        ret = _MT_get_result(session, transfer.id)
        _MT_finish_transfer(session)
        frame_stk.switch_to_default(driver)
        return ret
    except (OTPTimeoutError, OTPOtherError) as e:
        logger.error(f'OTP Error {e}')
        frame_stk.switch_to_default(driver)
        return TransferError(None, transfer.id, e.msg())
    except OTPIncorrectError as e:
        session.otp = None
        logger.error(f'OTP Incorrect Error {e}')
        frame_stk.switch_to_default(driver)
        return TransferError(None, transfer.id, e.msg())
    except BankSpecificError as e:
        logger.error(f'Specific Error {e}')
        ret = _catch_specific_error(session, transfer.id, e)
        frame_stk.switch_to_default(driver)
        return ret

    except Exception as e:  # FIXME: except Exception
        frame_stk.switch_to_default(driver)
        return _get_transfer_error(session, transfer.id, str(e))


def _MT_get_transfers(session: BanescoSession, transfers: List[Transfer],
                      results: List[TransferResult]) -> None:
    get_otp = session.get_otp

    def f(s, i, t):
        return _MT_get_single_transfer(s, get_otp, i, t)

    return _get_many_transfers(session, f, transfers, results)


###############################################################################
# Get balance
###############################################################################


def _get_balance(session: BanescoSession) -> Tuple[str, str, str]:
    """Get balance of main account."""
    driver, _frame_stk, logger = session._get()
    session.report_all(ConsultStatus())
    wait_for_xpath(driver, '//*[@id="content-right"]/table[3]/tbody/tr/td[3]')

    def aux_get(x):
        return find_by_xpath(
            driver,
            f'//*[@id="content-right"]/table[6]/tbody/tr[{x}]/td[3]').text

    bal = aux_get(1)
    deferred = aux_get(2)
    blocked = aux_get(3)
    logger.info('got balances')
    return (bal, deferred, blocked)


def _go_to_main_account(session: BanescoSession) -> None:
    driver, _frame_stk, logger = session._get()
    logger.info('go to main account')
    """Go to main account."""

    if not test_for_xpath(driver,
                          '//*[@id="content-right"]/table[3]/tbody/tr/td[2]'):
        was_open = False
        try:
            wait_for_xpath(driver,
                           '//*[@id="ctl00_FastMenu"]/div[1]/a[1]').click()
            was_open = True
        except Exception:  # FIXME: except Exception
            pass
        logger.debug(f'was_open = {was_open}')

        # was open = False
        # ERROR - BAN-Horacio83 - 15:19:27 - Exception getting balance and transactions.
        # Traceback (most recent call last):
        #   File "/venebankservice/venebankservice/venebank/bank_account.py", line 175, in get_transfers_and_movements
        #     QueryInterval.LAST_TWO_DAYS)
        #   File "/venebankservice/venebankservice/venebank/selenium/banesco.py", line 130, in get_balance_and_transactions
        #     return _get_balance_and_transactions(self, query_interval)
        #   File "/venebankservice/venebankservice/venebank/selenium/banesco.py", line 1002, in _get_balance_and_transactions
        #     _go_to_main_account(session)
        #   File "/venebankservice/venebankservice/venebank/selenium/banesco.py", line 921, in _go_to_main_account
        #     find_by_id(driver, 'm_0').click()
        #   File "/venebankservice/venebankservice/venebank/selenium/selenium_utils.py", line 103, in find_by_id
        #     return wait_for_presence(driver, By.ID, elem_id, timeout)
        #   File "/venebankservice/venebankservice/venebank/selenium/selenium_utils.py", line 69, in wait_for_presence
        #     EC.presence_of_element_located((by, location)))
        #   File "/usr/local/lib/python3.7/dist-packages/selenium/webdriver/support/wait.py", line 71, in until
        #     value = method(self._driver)
        #   File "/usr/local/lib/python3.7/dist-packages/selenium/webdriver/support/expected_conditions.py", line 64, in __call__
        #     return _find_element(driver, self.locator)
        #   File "/usr/local/lib/python3.7/dist-packages/selenium/webdriver/support/expected_conditions.py", line 415, in _find_element
        #     raise e
        #   File "/usr/local/lib/python3.7/dist-packages/selenium/webdriver/support/expected_conditions.py", line 411, in _find_element
        #     return driver.find_element(*by)
        #   File "/usr/local/lib/python3.7/dist-packages/selenium/webdriver/remote/webdriver.py", line 978, in find_element
        #     'value': value})['value']
        #   File "/usr/local/lib/python3.7/dist-packages/selenium/webdriver/remote/webdriver.py", line 321, in execute
        #     self.error_handler.check_response(response)
        #   File "/usr/local/lib/python3.7/dist-packages/selenium/webdriver/remote/errorhandler.py", line 242, in check_response
        #     raise exception_class(message, screen, stacktrace)
        # selenium.common.exceptions.InvalidSessionIdException: Message: invalid session id

        if not was_open:
            wait_for_id(driver, 'm_0').click()
            time.sleep(1)
            find_by_xpath(driver,
                          '//*[@id="ctl00_FastMenu"]/div[1]/a[1]').click()

            logger.debug('click')

        find_by_id(driver, 'ctl00_btnTopHome_lkButton').click()

    wait_for_xpath(driver,
                   '//*[@id="content-right"]/table[3]/tbody/tr/td[2]').click()


def _select_interval(driver: Chrome, x) -> None:
    wait_for_xpath(driver,
                   f'//*[@id="ctl00_cp_ddlPeriodo"]/option[{x}]').click()
    find_by_id(driver, 'ctl00_cp_btnMostrar').click()


def _select_current_month(session: BanescoSession) -> None:
    """Select current month and clicks 'Consultar'."""
    driver, _frame_stk, logger = session._get()
    logger.info('go to current month')
    _select_interval(driver, 3)


def _select_previous_month(session: BanescoSession) -> None:
    """Select previous month and clicks 'Consultar'."""
    driver, _frame_stk, logger = session._get()
    logger.info('go to previous month')
    _select_interval(driver, 4)


def _select_current_day(session: BanescoSession) -> None:
    """Select current month and clicks 'Consultar'."""
    driver, _frame_stk, logger = session._get()
    logger.info('go to current day')
    _select_interval(driver, 1)


def _select_previous_day(session: BanescoSession) -> None:
    """Select previous month and clicks 'Consultar'."""
    driver, _frame_stk, logger = session._get()
    logger.info('go to previous day')
    _select_interval(driver, 2)


def _get_transactions(session: BanescoSession) -> List[Transaction]:
    """Get all transactions and build a dictionary with them."""
    driver, _frame_stk, logger = session._get()
    logger.info('get transactions')
    transactions = []
    while True:
        logger.debug('start transactions page')
        transactions_table = wait_for_xpath(
            driver, '//*[@id="content-right"]/table[11]/tbody')
        soup = BeautifulSoup(transactions_table.get_attribute('innerHTML'),
                             features="html.parser")
        t_rows = soup.find_all('tr')
        for t_row in t_rows:
            tds = t_row.find_all('td')
            transactions.append(
                Transaction(date=datetime.strptime(tds[0].contents[0],
                                                   '%d/%m/%Y').date(),
                            description=tds[1].contents[0],
                            reference=tds[2].contents[0],
                            amount=tds[3].contents[0],
                            direction=tds[4].contents[0]))
        if len(driver.find_elements_by_id('ctl00_cp_btnSig')) > 0:
            logger.info('go to next page')
            find_by_id(driver, 'ctl00_cp_btnSig').click()
        else:
            logger.info('no more pages')
            break
    return transactions


def _get_balance_and_transactions(session: BanescoSession,
                                  query_interval: QueryInterval) -> QueryResult:
    """Login to banesco and get transactions and balance."""
    _driver, _frame_stk, _logger = session._get()
    _go_to_main_account(session)

    balance = _get_balance(session)

    current_month = query_interval == QueryInterval.LAST_MONTH \
        or query_interval == QueryInterval.LAST_TWO_MONTHS
    current_day = query_interval == QueryInterval.LAST_DAY \
        or query_interval == QueryInterval.LAST_TWO_DAYS

    transactions: List[Transaction] = []

    def include_transactions(transactions) -> List[Transaction]:
        try:
            older_transactions = _get_transactions(session)
            return older_transactions + transactions
        except Exception:  # FIXME: except Exception
            return transactions

    if current_month:
        _select_current_month(session)
        transactions = include_transactions(transactions)

    if query_interval == QueryInterval.LAST_TWO_MONTHS:
        _select_previous_month(session)
        transactions = include_transactions(transactions)

    if current_day:
        _select_current_day(session)
        transactions = include_transactions(transactions)

    if query_interval == QueryInterval.LAST_TWO_DAYS:
        _select_previous_day(session)
        transactions = include_transactions(transactions)

    return (balance, transactions)


if __name__ == '__main__':
    pw = os.environ['CRED_ENCR_PW']
    creds = Credentials.from_encrypted_YAML_file(
        'credentials/credentials_banesco.yaml.encr', pw)

    from ..testing_recipients import (sara_mercantil, papa_banesco,
                                      augusto_pago_movil_mercantil)

    obt = Transfer(amount='123,69',
                   recipient=sara_mercantil,
                   concept='prueba1',
                   id='ban1')
    obtres: List[TransferResult] = [TransferNotStarted('ban1')]
    obtBAD = Transfer(amount='',
                      recipient=sara_mercantil,
                      concept='1',
                      id='badobt')

    sbt = Transfer(amount='123,69',
                   recipient=papa_banesco,
                   concept='prueba2',
                   id='ban2')
    sbtres: List[TransferResult] = [TransferNotStarted('ban2')]
    sbtBAD = Transfer(amount='0,00',
                      recipient=papa_banesco,
                      concept='2',
                      id='badsbt')

    mt = Transfer(amount='125,12',
                  recipient=augusto_pago_movil_mercantil,
                  concept='prueba2',
                  id='ban3')
    mtres: List[TransferResult] = [TransferNotStarted('ban3')]
    mtBAD = Transfer(amount='0,00',
                     recipient=augusto_pago_movil_mercantil,
                     concept='3',
                     id='badmt')

    def _get_otp():
        otp = input('otp: ')
        if otp == 't':
            raise OTPTimeoutError()
        if otp == 'e':
            raise OTPOtherError('Error desde debug.')
        return otp

    # This wont work until we update with report, report_all
    # with BanescoSession(creds, _get_otp) as session:
    # session.make_other_bank_transfer([obt], _get_otp, obtres)
    # session.make_mobile_transfer([mt], _get_otp, mtres)
    # session.make_same_bank_transfer([sbt], _get_otp, sbtres)
    # bals, trans = session.get_balance_and_transactions(
    #     QueryInterval.LAST_DAY)
    # print(bals)
    # print(f'{len(trans)} transaction')

    # for t in obtres + mtres + sbtres:
    #     print(repr(t))
