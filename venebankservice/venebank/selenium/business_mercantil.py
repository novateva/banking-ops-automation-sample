"""BusinessMercantilSession."""

from posix import EX_PROTOCOL
from selenium.webdriver.chrome.webdriver import WebDriver as Chrome
from selenium.webdriver.remote.webdriver import WebElement
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import (  # noqa: F401
    ElementNotInteractableException, ElementClickInterceptedException,
    TimeoutException, InvalidSessionIdException,
    UnexpectedAlertPresentException)

from bs4 import BeautifulSoup

import time
import os
import threading
from datetime import date, datetime, timezone, timedelta

from typing import List, Tuple, Optional, Callable, Union
import logging

from ..exceptions import (DriverError, LoginError, OTPTimeoutError,
                          OTPOtherError, OTPIncorrectError)
from ..transfer import (Recipient, Transfer, TransferNotStarted, TransferResult, TransferSuccess,
                        TransferError, TransferNotCompleted)
from ..query import Transaction, QueryInterval, QueryResult
from ..credentials import Credentials
from ..utils import get_VE_time
from .logger import get_logger
from .selenium_utils import (find_by_xpath, find_by_id, wait_for_id,
                             wait_for_xpath, find_by_name, wait_for_name,
                             wait_for_case, test_for_xpath, test_for_presence)
from . import selenium_utils
from .frame_stack import FrameStack
from .bank_session import BankSession
from ..status import (Status, LoginStatus, LogoutStatus, ConsultStatus,
                      OTPStatus, TransferStartStatus, TransferDoneStatus,
                      WaitingTransfersStatus, RegisterClientStatus,
                      TokenLoginStatus)

# NOTE Los mensajes de error son alertas.


class BusinessMercantilSession(BankSession):
    """Business Mercantil Session."""

    credentials: Credentials
    logger: logging.Logger
    frame_stk: FrameStack
    driver: Chrome
    get_otp: Optional[Callable[[], str]]
    report: Callable[[List[str], Status], None]
    report_all: Callable[[Status], None]

    def __init__(self,
                 credentials: Credentials,
                 get_otp: Callable[[], str],
                 report: Callable[[List[str], Status], None],
                 report_all: Callable[[Status], None]) -> None:
        """BusinessMercantilSession, use as context manager."""
        self.credentials = credentials
        self.get_otp = get_otp
        uid = credentials.uid
        if uid.isnumeric():
            uid = uid[-6:]
        self.logger = get_logger(f'BUSS_MER-{uid}', logging.DEBUG)
        self.frame_stk = FrameStack()
        self.report = report
        self.report_all = report_all

    def __enter__(self) -> "BusinessMercantilSession":
        """Open session, see open()."""
        for tries in range(2):
            try:
                self.open()
                return self
            except Exception:
                if tries == 1:
                    raise
                time.sleep(5)
                try:
                    self.driver.close()
                except (InvalidSessionIdException, AttributeError):
                    pass
        raise Exception('This is impossible.')

    # Literal[True] would be better return type but it's not in 3.7
    def __exit__(self, type, value, traceback) -> Optional[bool]:
        """Close session, see close()."""
        if type is None:
            self.close()
            return True
        else:
            screenshot_path, sshot = _take_error_screenshot(self)

            scr_msg = ''
            if screenshot_path is not None:
                scr_msg = f' Error screenshot save at {screenshot_path}.'

            self.logger.error('trying to quit session after error')
            try:
                _quit_session_and_close(self)
            except Exception:  # FIXME except Exception
                pass
            self.logger.exception(
                f'Exception getting balance and transactions.{scr_msg}')
            return None

    def open(self) -> None:
        """Get a webdriver and login."""
        try:
            self.driver = _get_driver(self.logger)
            _login(self)
            self.report_all(WaitingTransfersStatus())
        except (DriverError, LoginError) as e:
            self.logger.error(e)
            raise
        except Exception:  # FIXME except exception
            self.logger.exception('catch specific exception')
            raise

    def close(self):
        """Logout and quit session."""
        self.logger.debug('quitting session')
        _quit_session_and_close(self)

    def _get(self) -> Tuple[Chrome, FrameStack, logging.Logger]:
        return (self.driver, self.frame_stk, self.logger)

    def get_balance_and_transactions(
            self, query_interval: QueryInterval) -> QueryResult:
        """Get balance and transactions."""
        return _get_balance_and_transactions(self, query_interval)

    def make_same_bank_transfer(self, transfers: List[Transfer],
                                transfer_results: List[TransferResult]) -> None:
        """Make a list of transfers."""
        _SBT_make_transfers(self, transfers, transfer_results)

    def make_other_bank_transfer(
            self, transfers: List[Transfer],
            transfer_results: List[TransferResult]) -> None:
        """Make a list of transfers."""
        _OBT_make_transfers(self, transfers, transfer_results)

    def make_mobile_transfer(self, transfers: List[Transfer],
                             transfer_results: List[TransferResult]) -> None:
        """Make a list of transfers.

        In mercantil we don't need to get otp for mobile transfers,
        it uses mobile password instead.
        """
        # _MT_get_transfer(self, transfers, transfer_results)


###############################################################################
###############################################################################
# Internal stuff starts here
###############################################################################
###############################################################################


def _get_VE_prev_month() -> Tuple[int, str]:
    months = [
        'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',
        'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
    ]
    tz = timezone(timedelta(hours=-4))
    cur_month = int(datetime.now(tz).strftime("%m")) - 1
    prev_month = (cur_month - 1 + 12) % 12
    return (prev_month, months[prev_month])


###############################################################################
# Login, Quit session, etc
###############################################################################


def _keep_alive(session: BusinessMercantilSession,
                original_window: str) -> None:
    """Check if we need to maintain the session alive."""
    driver, frame_stk, logger = session._get()
    logger.debug('Checking if need to keep session alive.')

    # This is one kind of popout window,
    # it forces you to close the session instead of asking for
    # an extension (?).
    # //*[@id="Cerrar"]
    # //*[@id="Continuar"]
    try:
        for window_handle in driver.window_handles:
            if window_handle != original_window:
                driver.switch_to.window(window_handle)
                break
        find_by_id(driver, 'Continuar').click()
        logger.debug('Kept session alive.')
        driver.switch_to.window(original_window)
    except Exception:  # FIXME except Exception
        logger.debug('No need to keep session alive right now.')


def _get_otp_while_keep_alive(session: BusinessMercantilSession) -> str:
    """Keep the session alive while waiting for otp.

    Call the function keep_alive every 3 seconds, while waiting for
    get_otp to return the wanted otp.
    """
    driver, _frame_stk, _logger = session._get()
    otp_received = threading.Event()
    original_window = driver.current_window_handle

    def keep_alive_while_waiting():
        cnt = 0
        while not otp_received.is_set():
            cnt += 1
            if cnt == 3:
                _keep_alive(session, original_window)
                cnt = 0
            time.sleep(1)

    keep_alive_thread = threading.Thread(target=keep_alive_while_waiting)
    keep_alive_thread.start()

    try:
        otp = session.get_otp()
    except (OTPTimeoutError, OTPOtherError):
        otp_received.set()
        keep_alive_thread.join()
        raise
    except Exception:
        otp_received.set()
        keep_alive_thread.join()
        session.logger.exception('Exception ocurred while fetching OTP.')
        raise

    session.logger.info(f'received otp {otp}')

    otp_received.set()
    keep_alive_thread.join()
    return otp


def _quit_session(session: BusinessMercantilSession) -> None:
    driver, frame_stk, logger = session._get()
    logger.info('log out')
    session.report_all(LogoutStatus())
    # frame_stk.switch_to_default(driver)
    find_by_xpath(driver, '//*[text()="Salir"]').click()


def _quit_session_and_close(session: BusinessMercantilSession) -> None:
    driver, _frame_stk, _logger = session._get()
    _quit_session(session)
    # TODO
    time.sleep(2)
    driver.close()


def _take_error_screenshot(
        session: BusinessMercantilSession,
        msg: str = '') -> Tuple[Optional[str], Optional[str]]:
    """Take screenshot of current screen.

    Args:
        session (BusinessMercantilSession)
        msg (str, optional): small description on filename. Defaults to ''.

    Returns:
        Tuple[Optional[str], Optional[str]]: if screenshot was successful,
        return a pair of strings (file_relative_path, base64_screenshot)
    """
    driver, _frame_stk, _logger = session._get()
    directory = 'logs/screenshots'
    file_name = f'{get_VE_time()}-{os.getpid()}{msg}-mercantil.png'  # TODO
    os.makedirs(directory, exist_ok=True)
    screenshot_path = f'{directory}/{file_name}'
    try:
        screenshot = driver.get_screenshot_as_file(screenshot_path)
        sshot = driver.get_screenshot_as_base64()
    except Exception:  # FIXME except Exception
        screenshot = False
        sshot = None
    if screenshot:
        return screenshot_path, sshot
    else:
        return None, sshot


def _get_driver(logger: logging.Logger) -> Chrome:
    """Create a new chrome driver and open Bussiness Mercantil."""
    try:
        logger.info('opening driver')
        driver = selenium_utils.get_driver()
        driver.get('https://empresas.bancomercantil.com/MELE/control/BoleTransactional.mercantil')
        return driver
    except Exception:  # FIXME except Exception
        logger.exception('Couldn\'t open driver')
        raise DriverError()


def _login(session: BusinessMercantilSession) -> None:
    driver, frame_stk, logger = session._get()
    credentials = session.credentials

    try:
        logger.info('login')
        session.report_all(LoginStatus())
        uid = credentials.uid
        pw = credentials.pw
        answers = credentials.answers
        # User id.
        find_by_id(driver, "CLIENTID").send_keys(uid)
        find_by_id(driver, "submitButton").click()
        # User password.
        find_by_id(driver, "USERPASS").send_keys(pw)
        # Retrieve token generated by Entrust.
        session.report_all(TokenLoginStatus())
        token = _get_otp_while_keep_alive(session)
        find_by_id(driver, "USERTOKEN").send_keys(token)
        find_by_id(driver, "acceptButton").click()

        def answerQuestion(driver, n, i):
            """Answer secret questions."""
            question = (wait_for_xpath(
                driver,
                f'//*[@id="questionForm"]/table[1]/tbody/tr[{n}]/td/b').text)
            for (q, a) in answers:
                if q in question:
                    wait_for_id(driver, f'response{i}').send_keys(a)
                    return
            error_msg = f'Answer to secret question "{question}" not found.'
            raise Exception(error_msg)

        answerQuestion(driver, '3', '1')
        answerQuestion(driver, '6', '2')
        find_by_id(driver, 'aceptar').click()
        # Select 'no' when asked if we want to register the current machine
        # as trusted.
        find_by_id(driver, 'radioNo').click()
        find_by_id(driver, 'submitButton').click()
        # Confirm current machine is not registered as trusted.
        find_by_xpath(
            driver,
            '//*[@id="endRegisterForm"]/table[2]/tbody/tr/td/input').click()
        frame_stk.switch_to_default(driver)
    except Exception:  # FIXME except Exception
        # TODO
        screenshot_path, sshot = _take_error_screenshot(session)
        scr_msg = ''
        if screenshot_path is not None:
            scr_msg = f' Error screenshot saved at {screenshot_path}.'

        logger.exception(f'Couldn\'t log in.{scr_msg}')
        raise LoginError(sshot, 'No se pudo iniciar sesion')


###############################################################################
# Transfers work-flow helper functions.
###############################################################################

def _go_to_homepage(session: BusinessMercantilSession) -> None:
    """Switch to the Bussiness Mercantil homescreen."""
    driver, _frame_stk, logger = session._get()
    logger.debug('Returning to the home page...')

    find_by_id(driver, 'linkInicio').click()

    logger.debug('Sucessfuly returned to the home page.')


def _go_to_recipient_registration(session: BusinessMercantilSession,
                                  recipient: Recipient) -> None:
    """Switch to the recipient registration page."""
    driver, _frame_stk, logger = session._get()
    logger.debug('Switching to the recipient registration page...')

    # Start by clicking on 'administration and security'.
    # //*[@id="AdmSeguridad"]
    find_by_id(driver, 'AdmSeguridad').click()
    # Hover over to 'afiliations'.
    # //*[@id="AdmSeguridad"]/ul/li[2]/div/p
    option = find_by_xpath(driver, '//*[@id="AdmSeguridad"]/ul/li[2]/div/p', 2)
    ActionChains(driver).move_to_element(option).perform()
    # Select 'afiliation of products and services'
    # //*[@id="AdmSeguridad"]/ul/li[2]/ul/li[1]/div/a
    find_by_xpath(
        driver,
        '//*[@id="AdmSeguridad"]/ul/li[2]/ul/li[1]/div/a', 2).click()
    # Select product type to afiliate based on destination bank.
    # //*[@id="service"]
    wait_for_id(driver, 'service').click()
    if recipient.bank.value == 'BANCO MERCANTIL':
        # value="CC", Cuentas de Terceros en Mercantil
        # For Mercantil accounts.
        find_by_xpath(driver, '//*[@value="CC"]').click()
    else:
        # value="TXNACIONAL", Transferencias Nacionales
        # For other national banks accounts.
        find_by_xpath(driver, '//*[@value="TXNACIONAL"]').click()

    # Click 'continue'.
    # //*[@id="formService"]/table/tbody/tr[2]/td/input, name="Continuar"
    find_by_name(driver, 'Continuar').click()

    logger.debug('Succesfully switched to the recipient registration page.')


###############################################################################
# Same Bank Transfers : Transfers between the same bank (Mercantil).
###############################################################################


def _go_to_SBT_page(session: BusinessMercantilSession) -> None:
    """Switch to the 'third party Mercantil accounts' transfers page."""
    driver, frame_stk, logger = session._get()
    logger.debug('Switching to the "same bank" transfers page...')

    # Select 'transfers and payments'.
    wait_for_id(driver, 'PagosYTranferencias').click()
    # Select 'transfers to thirdparty Mercantil'.
    find_by_xpath(
        driver,
        '//*[@id="PagosYTranferencias"]/ul/li[3]/div/a').click()

    logger.debug('Succesfully switched to the "same bank" transfers page.')


def _SBT_check_recipient_registered(
        session: BusinessMercantilSession,
        recipient: Recipient) -> Optional[WebElement]:
    """Check if a recipient is already registered in the user's database."""
    driver, _frame_stk, logger = session._get()

    # Open registered recipient's accounts list.
    wait_for_name(driver, 'destAccountList').click()
    recipients = driver.find_elements_by_tag_name('option')
    # First 20 characters in the value attribute contain the
    # recipient's number.
    logger.debug('Searching if recipient is already registered...')
    for r in recipients:
        if r.get_attribute('value')[:20] == recipient.number:
            logger.debug('Registered recipient found.')
            return r

    logger.debug('Recipient not registered.')
    return None


def _SBT_register_recipient(session: BusinessMercantilSession,
                            recipient: Recipient, transfer: Transfer) -> None:
    """Register a same bank recipient into the user's database."""
    driver, _frame_stk, logger = session._get()
    logger.debug('Registering new same bank recipient...')

    # Switch to the recipient registration page.
    session.report([transfer.id], RegisterClientStatus())
    _go_to_recipient_registration(session, recipient)
    # Fetch an Entrust verification token.
    logger.debug('Waiting 35 seconds before OTP request...')
    time.sleep(35)  # FIXME Waiting 30s for Entrust to generate a new OTP.
    session.report([transfer.id], OTPStatus())
    otp = _get_otp_while_keep_alive(session)
    wait_for_id(driver, 'USERTOKEN').send_keys(otp)
    # Click 'continue'.
    find_by_id(driver, 'bContinuar').click()
    # Check for error messages concerning the OTP.
    if test_for_presence(driver, By.XPATH, '//*[@id="summary"]/tr/td[2]', 2):
        raise OTPIncorrectError

    # Fill in recipient details for afiliation.
    wait_for_id(driver, 'ACCTNUM').send_keys(recipient.number)
    find_by_id(driver, 'BENEF').send_keys(recipient.name)
    find_by_id(driver, 'DOCTYPE').click()
    find_by_xpath(driver, f'//*[@value="{recipient.id_type.name}"]').click()
    find_by_id(driver, 'ID').send_keys(recipient.identification)
    find_by_id(driver, 'EMAIL').send_keys(recipient.email)
    find_by_id(driver, 'EMAILC').send_keys(recipient.email)
    find_by_id(driver, 'DESC').send_keys(recipient.description)

    # Click 'continue'... again.
    find_by_id(driver, 'Continuar').click()

    # Input fields error management.
    # FIXME Make neaty pretty exceptions some day...
    time.sleep(1)
    try:
        error_string = find_by_xpath(
            driver,
            '//label[@class="error"]/a').get_attribute('onmouseover')
        # The 'onmouseover' attribute of the <a> tags holds the error message
        # in a format like this:
        # onmouseover="getMouseXY( event );
        # showHelp('error_popup', 'Correo Electrónico del Beneficiario',
        # 'Por favor, ingrese Correo Electrónico válido', 175 );"
        raise Exception(error_string.split("'")[-2])
    except TimeoutException:
        pass

    # Click 'confirm' to finalize the afiliation process.
    # TODO Uncomment this after testing error management.
    wait_for_name(driver, 'Confirmar').click()

    logger.debug('Succesfully registered same bank recipient')


def _SBT_write_transfer_details(session: BusinessMercantilSession,
                                transfer: Transfer) -> None:
    """Fill in the transfer details for third-party Mercantil accounts."""
    driver, _frame_stk, logger = session._get()
    logger.debug('Filling in transfer details...')

    # Choose user's account.
    wait_for_name(driver, 'sourceAccountList').click()
    driver.find_elements_by_tag_name('option')[1].click()

    # Transfer details.
    amount = transfer.amount
    concept = transfer.concept
    recipient = transfer.recipient
    find_by_id(driver, 'value').send_keys(amount)
    # By this point the recipient should already be registered.
    _SBT_check_recipient_registered(session, recipient).click()
    find_by_id(driver, 'motive').send_keys(concept)
    find_by_name(driver, 'Continuar').click()

    logger.debug('Succesfully filled in transfer details.')


def _SBT_get_transfer_result(session: BusinessMercantilSession,
                             transfer_id: str, name: str) -> TransferResult:
    """Take screenshot and retrieve reference for same bank transferences."""
    driver, _frame_stk, logger = session._get()
    logger.debug('Retrieving transaction results...')

    try:
        info = wait_for_id(driver, 'contentContainer')
        # Obtain the operation's reference number.
        ref = find_by_xpath(
            driver,
            '//*[@id="actionContainer"]/table/tbody/tr/td[2]').text
        # Generate and save screenshot.
        os.makedirs('screenshots', exist_ok=True)
        name = name.replace(' ', '_')
        screenshot_path = f'screenshots/mercantil_juridico_{name}_{ref}.png'
        info.screenshot(screenshot_path)
        sshot = None
        try:
            sshot = info.screenshot_as_base64
        except Exception:  # FIXME
            pass
        # Click the 'finalize' button.
        find_by_name(driver, 'Submit2').click()

        logger.debug('Succesfully retrieved transaction results.')
        return TransferSuccess(sshot, transfer_id, ref)
    except Exception:  # FIXME
        logger.exception("Coudn't retrieve transaction results.")
        screenshot_path, sshot = _take_error_screenshot(session)
        if screenshot_path is not None:
            scr_msg = f'Error Screenshot saved at: {screenshot_path}'
            logger.debug(f'{scr_msg}')
        return TransferNotCompleted(transfer_id)


def _SBT_transfer(session: BusinessMercantilSession,
                  transfer: Transfer) -> TransferResult:
    """Execute a single transfer to a Mercantil third-party account."""
    driver, frame_stk, logger = session._get()
    logger.info('Starting execution of transfer (Mercantil -> Mercantil)...')

    try:
        session.report([transfer.id], TransferStartStatus())
        _go_to_SBT_page(session)
        # Check if we need to register the recipient, because then we'll have to
        # go to another page.
        recipient = transfer.recipient
        recipient_location = _SBT_check_recipient_registered(session, recipient)
        if recipient_location is None:
            _SBT_register_recipient(session, recipient, transfer)
            _go_to_SBT_page(session)

        _SBT_write_transfer_details(session, transfer)

        # Click the 'confirm' button.
        find_by_name(driver, 'Confirmar').click()
        transfer_results = _SBT_get_transfer_result(session,
                                                    transfer.id,
                                                    transfer.recipient.name)
    except Exception as e:  # FIXME
        logger.exception("Coudn't execute transfer (Mercantil -> Mercantil).")
        error_msg = ('Exception ocurred during '
                     f'(Mercantil -> Mercantil) transfer execution: {str(e)}')
        screenshot_path, sshot = _take_error_screenshot(session)
        if screenshot_path is not None:
            scr_msg = f'Error Screenshot saved at: {screenshot_path}'
            logger.debug(f'{scr_msg}')
        transfer_results = TransferError(sshot, transfer.id, error_msg)
        _go_to_homepage(session)

    if isinstance(transfer_results, TransferSuccess):
        logger.info('Succesfully executed transfer (Mercantil -> Mercantil).')
    return transfer_results


def _SBT_make_transfers(session: BusinessMercantilSession,
                        transfers: List[Transfer],
                        transfers_results: List[TransferResult]) -> None:
    """Attemp to execute each of the requested transfers."""
    i = 0

    for transfer in transfers:
        transfers_results[i] = _SBT_transfer(session, transfer)
        session.report([transfer.id], TransferDoneStatus())
        i += 1


###############################################################################
# Other Banks Transfers : Transfers Mercantil and other banks.
###############################################################################


def _go_to_OBT_page(session: BusinessMercantilSession) -> None:
    """Switch to the 'other banks' transfers page."""
    driver, _frame_stk, logger = session._get()
    logger.debug('Switching to the "other banks" transfers page...')
    # //*[@id="PagosYTranferencias"]/ul/li[4]/div/a

    # Select 'transfers and payments'.
    wait_for_id(driver, 'PagosYTranferencias').click()
    # Select 'transfers to other national banks'
    find_by_xpath(
        driver,
        '//*[@id="PagosYTranferencias"]/ul/li[4]/div/a').click()

    logger.debug('Succesfully switched to the "other banks" transfers page.')


def _OBT_check_recipient_registered(
        session: BusinessMercantilSession,
        recipient: Recipient) -> Optional[WebElement]:
    """Check if a recipient is already registered in the user's database."""
    driver, _frame_stk, logger = session._get()

    # Open registered recipient's accounts list.
    wait_for_name(driver, 'destAccount').click()
    recipients = driver.find_elements_by_tag_name('option')
    # First 20 characters in the text attribute contain the
    # recipient's number.
    logger.debug('Searching if recipient is already registered...')
    for r in recipients:
        if r.text[:20] == recipient.number:
            logger.debug('Registered recipient found.')
            return r

    logger.debug('Recipient not registered.')
    return None


def _OBT_register_recipient(session: BusinessMercantilSession,
                            recipient: Recipient, transfer: Transfer) -> None:
    """Register a recipient into the user's database."""
    driver, _frame_stk, logger = session._get()
    logger.debug('Registering new other bank recipient...')

    # Switch to the recipient registration page.
    session.report([transfer.id], RegisterClientStatus())
    _go_to_recipient_registration(session, recipient)
    # Fetch an Entrust verification token.
    logger.debug('Waiting 35 seconds before OTP request...')
    time.sleep(35)  # FIXME Waiting 30s+ for Entrust to generate a new OTP.
    session.report([transfer.id], OTPStatus())
    otp = _get_otp_while_keep_alive(session)
    wait_for_id(driver, 'USERTOKEN').send_keys(otp)
    # Click 'continue'.
    find_by_id(driver, 'bContinuar').click()
    # Check for error messages concerning the OTP.
    if test_for_presence(driver, By.XPATH, '//*[@id="summary"]/tr/td[2]', 2):
        raise OTPIncorrectError
    # Select recipient's bank.
    # //*[@id="TARGETBANK", Banco destino
    # <option> donde @value es el código del banco.
    wait_for_id(driver, 'TARGETBANK').click()
    posible_banks = driver.find_elements_by_tag_name('option')
    bank_found = False
    for bank in posible_banks:
        if recipient.bank.name[1:] == bank.get_attribute('value'):
            bank.click()
            bank_found = True

    if not bank_found:
        raise Exception("Recipient's bank not found.")

    # Fill in recipient details for afiliation.
    find_by_id(driver, 'ACCTNUM').send_keys(recipient.number)
    find_by_id(driver, 'BENEFICIARY').send_keys(recipient.name)
    find_by_id(driver, 'DOCTYPE').click()
    find_by_xpath(driver, f'//*[@value="{recipient.id_type.name}"]').click()
    find_by_id(driver, 'ID').send_keys(recipient.identification)
    find_by_id(driver, 'EMAIL').send_keys(recipient.email)
    find_by_id(driver, 'DESCRIPTION').send_keys(recipient.description)
    # Click the 'continue' button.
    find_by_id(driver, 'Continuar').click()

    # Input fields error management.
    # FIXME Make neaty pretty exceptions some day...
    time.sleep(1)
    try:
        error_string = find_by_xpath(
            driver,
            '//label[@class="error"]/a').get_attribute('onmouseover')
        # The 'onmouseover' attribute of the <a> tags holds the error message
        # in a format like this:
        # onmouseover="getMouseXY( event );
        # showHelp('error_popup', 'Correo Electrónico del Beneficiario',
        # 'Por favor, ingrese Correo Electrónico válido', 175 );"
        raise Exception(error_string.split("'")[-2])
    except TimeoutException:
        pass

    # Click the 'confirm' button.
    # TODO Uncomment this after testing error management.
    wait_for_name(driver, 'Confirmar').click()

    logger.debug('Succesfully registered other bank recipient.')


def _OBT_write_transfer_details(session: BusinessMercantilSession,
                                transfer: Transfer) -> None:
    """Fill in the transfer details for other bank accounts."""
    driver, _frame_stk, logger = session._get()
    logger.debug('Filling in transfer details...')

    # Choose user's account.
    wait_for_id(driver, 'sourceAccount').click()
    driver.find_elements_by_tag_name('option')[1].click()

    # Transfer details.
    amount = transfer.amount
    concept = transfer.concept
    recipient = transfer.recipient
    # Monto, name="amount"
    find_by_name(driver, 'amount').send_keys(amount)
    # By this point the recipient should already be registered.
    _OBT_check_recipient_registered(session, recipient).click()
    # Nombre del beneficiario, name=beneficiario
    # NOTE Apparently Mercantil pulls the recipient's name from
    # the database and auto-fills the following field.
    # find_by_name(driver, 'beneficiario').send_keys(recipient.name)

    # value="CLI", Transferencia por instrucciones del cliente
    # value="TMT", Transferencias entre cuentas mismo titular
    # value="TPU" , Compra de bonos
    find_by_xpath(driver, '//*[@value="CLI"]').click()
    find_by_name(driver, 'comments').send_keys(concept)

    # //*[@id="fecha"] ?
    # //*[@id="formContainer"]/form/table/tbody/tr[8]/td[2]/span/img
    # //*[@id="datepicker_div"]/div[1]/table/tbody/tr[4]/td[3]/a, <- DIA es txt
    # Selector de mes //*[@id="datepicker_div"]/div[1]/div/select[1]
    # meses son <option> con @value = numero de mes
    # Selector de año //*[@id="datepicker_div"]/div[1]/div/select[2]
    # años son <option> con @value = int(año)
    # Find and click the calendar.
    find_by_xpath(
        driver,
        ('//*[@id="formContainer"]/form/'
         'table/tbody/tr[8]/td[2]/span/img')).click()

    def get_valid_date() -> Optional[WebElement]:
        # Find a valid date in the current month, it can be any really,
        # so we're choosing the first one we find, it's gonna be at
        # least one day from the current day because we don't want to
        # deal with the 9:00 AM transactions time limit.
        time.sleep(1)
        cell_xpath = '//*[@class="datepicker_daysCell undefined"]'
        try:
            return driver.find_element_by_xpath(cell_xpath)
        except TimeoutException:
            return None

    cell = get_valid_date()
    if cell:
        cell.click()
    else:
        # If there are no valid dates for the current month check
        # the next month, there SHOULD be a valid date.
        # But first check if we're at the end of the year, aka
        # in December, this means we'll have to switch years.
        current_month = int(datetime.today().strftime('%m'))
        if current_month == 12:
            next_year = int(datetime.today().strftime('%Y')) + 1
            # Click the 'month' selector in the calenday.
            find_by_xpath(
                driver,
                '//*[@id="datepicker_div"]/div[1]/div/select[1]').click()
            # Choose January.
            # FIXME looking merely for an element with matching value
            # could result in problems.
            wait_for_xpath(driver, '//*[@value="1"]').click()
            # Click the 'year' selector in the calendar.
            find_by_xpath(
                driver,
                '//*[@id="datepicker_div"]/div[1]/div/select[2]').click()
            # Choose the corresponding year, next year should be always
            # available in the calenday.
            # FIXME looking merely for an element with matching value
            # could result in problems.
            wait_for_xpath(driver, f'//*[@value="{next_year}"]').click()

            # Check for valid date again.
            cell = get_valid_date()
            if cell:
                cell.click()
            else:
                # Coudn't find a valid date in 2 months?. God saves us.
                raise Exception("Coudn't find a valid transaction date.")
        else:
            next_month = current_month + 1
            # Click the 'month' selector in the calenday.
            find_by_xpath(
                driver,
                '//*[@id="datepicker_div"]/div[1]/div/select[1]').click()
            # Choose the corresponding month.
            # FIXME looking merely for an element with matching value
            # could result in problems.
            wait_for_xpath(driver, f'//*[@value="{next_month}"]').click()

            # Check for valid date again.
            cell = get_valid_date()
            if cell:
                cell.click()
            else:
                # Again this shoudn't really happen but computers are weird.
                raise Exception("Coudn't find a valid transaction date.")

    find_by_name(driver, 'Continuar').click()

    logger.debug('Succesfully filled in transfer details.')


def _OBT_get_transfer_result(session: BusinessMercantilSession,
                             transfer_id: str, name: str) -> TransferResult:
    """Take screenshot and retrieve reference for other bank transferences."""
    driver, _frame_stk, logger = session._get()
    logger.debug('Retrieving transaction results...')

    # //*[@id="tableContainer"]
    info = wait_for_id(driver, 'tableContainer')
    # Obtain the operation's reference number.
    ref = find_by_xpath(
        driver,
        '//*[@id="tableContainer"]/table/tbody/tr[12]/td[2]').text
    # Generate and save screensshot.
    os.makedirs('screenshots', exist_ok=True)
    name = name.replace(' ', '_')
    screenshot_path = f'screenshots/mercantil_juridico_{name}_{ref}.png'
    info.screenshot(screenshot_path)
    sshot = None
    try:
        sshot = info.screenshot_as_base64
    except Exception:  # FIXME
        pass

    # Click the 'finish' button.
    find_by_name(driver, 'Terminar').click()

    logger.debug('Succesfully retrieved transaction results.')
    return TransferSuccess(sshot, transfer_id, ref)


def _OBT_transfer(session: BusinessMercantilSession,
                  transfer: Transfer) -> TransferResult:
    """Execute a single transfer from Mercantil to another bank."""
    driver, _frame_stk, logger = session._get()
    logger.debug('Starting execution of transfer (Mercantil -> Other bank)...')

    try:
        session.report([transfer.id], TransferStartStatus())
        _go_to_OBT_page(session)
        # Check if we need to register the recipient, because then we'll have to
        # go to another page.
        recipient = transfer.recipient
        recipient_location = _OBT_check_recipient_registered(session, recipient)
        if recipient_location is None:
            _OBT_register_recipient(session, recipient, transfer)
            _go_to_OBT_page(session)

        _OBT_write_transfer_details(session, transfer)
        # Click the 'confirm' button.
        find_by_name(driver, 'Confirmar').click()
        transfer_results = _OBT_get_transfer_result(session,
                                                    transfer.id,
                                                    transfer.recipient.name)
    except Exception as e:  # FIXME
        logger.exception("Coudn't execute transfer (Mercantil -> Other bank).")
        error_msg = ('Exception ocurred during '
                     f'(Mercantil -> Other bank) transfer execution: {str(e)}')
        screenshot_path, sshot = _take_error_screenshot(session)
        if screenshot_path is not None:
            scr_msg = f'Error Screenshot saved at: {screenshot_path}'
            logger.debug(f'{scr_msg}')
        transfer_results = TransferError(sshot, transfer.id, error_msg)
        _go_to_homepage(session)

    if isinstance(transfer_results, TransferSuccess):
        logger.info('Succesfully executed transfer (Mercantil -> Other bank).')

    return transfer_results


def _OBT_make_transfers(session: BusinessMercantilSession,
                        transfers: List[Transfer],
                        transfers_results: List[TransferResult]) -> None:
    """Attemp to execute each of the requested transfers."""
    i = 0
    for transfer in transfers:
        transfers_results[i] = _OBT_transfer(session, transfer)
        session.report([transfer.id], TransferDoneStatus())
        i += 1


###############################################################################
# Mobile Transfers
###############################################################################

# TODO 

###############################################################################
# Get balance
###############################################################################


def _get_balance(session: BusinessMercantilSession) -> Tuple[str, str, str]:
    driver, _frame_stk, logger = session._get()
    logger.debug('waiting for the balance')
    try:
        wait_for_id(driver, 'Consultas').click()
        wait_for_xpath(
            driver,
            '//*[@id="Consultas"]/ul/li[1]/div/a', 2).click()
    except Exception as e:
        logger.error('Unable to click button in consults dropdown. '
                     f'Error message: {e}')
        raise

    def aux_get(x):
        return wait_for_xpath(
            driver,
            f'//*[@id="tDataCorrientes"]/tbody/tr/td[{x}]/a' if x != 8 else
            f'//*[@id="tDataCorrientes"]/tbody/tr/td[{x}]/ins').text

    logger.debug('getting the balance')
    deferred = aux_get(6)
    blocked = aux_get(7)
    balance = aux_get(8)
    logger.info('got balances')
    return (balance, deferred, blocked)


def _get_transactions(session: BusinessMercantilSession,
                      query_interval: QueryInterval) -> List[Transaction]:
    driver, _frame_stk, logger = session._get()
    logger.info('waiting for transactions')
    try:
        wait_for_id(driver, 'Consultas').click()
        wait_for_xpath(
            driver,
            '//*[@id="Consultas"]/ul/li[6]/div/a', 2).click()
    except Exception as e:
        logger.error(f'Unable to click button in consults dropdown. {e}')
        return []

    find_by_id(driver, 'accountForDetail').click()
    find_by_id(
        driver,
        'accountForDetail').find_elements_by_tag_name('option')[1].click()

    find_by_id(driver, 'RAPIDA').click()

    if query_interval == QueryInterval.LAST_DAY:
        find_by_id(driver,
                   'RAPIDA').find_elements_by_tag_name('option')[1].click()
    elif query_interval == QueryInterval.LAST_TWO_DAYS:
        find_by_id(driver,
                   'RAPIDA').find_elements_by_tag_name('option')[2].click()
    elif query_interval == QueryInterval.LAST_MONTH:
        find_by_id(driver,
                   'RAPIDA').find_elements_by_tag_name('option')[5].click()

    search_btn = find_by_name(driver, 'bBuscar')
    driver.execute_script("arguments[0].scrollIntoView();", search_btn)
    search_btn.click()

    logger.debug('processing transactions')

    next_button_path = ('/html/body/table[2]/tbody/tr/td/'
                        'div[1]/div[2]/div[3]/div[1]/table/'
                        'tbody/tr[1]/td[3]/form/a')

    transactions = []
    is_last_page = False
    is_table_present = test_for_presence(driver, By.XPATH,
                                         '//*[@id="tableData"]/tbody', 2)

    if is_table_present:
        while not is_last_page:
            transactions_table = find_by_xpath(driver,
                                               '//*[@id="tableData"]/tbody')
            soup = BeautifulSoup(transactions_table.get_attribute('innerHTML'),
                                 features="html.parser")
            t_rows = soup.find_all('tr')
            for t_row in t_rows:
                try:
                    tds = t_row.find_all('td')
                    a_out = tds[5].contents[0].strip()
                    a_in = tds[6].contents[0].strip()
                    if a_out == '':
                        amount = a_in
                        direc = '+'
                    else:
                        amount = a_out
                        direc = '-'
                    transactions.append(
                        Transaction(date=datetime.strptime(
                            tds[1].contents[0].strip(), '%d/%m/%Y').date(),
                                    description=tds[4].contents[0].strip(),
                                    reference=tds[2].find_all('a')
                                    [0].contents[0].strip(),
                                    amount=amount,
                                    direction=direc))
                except Exception as e:  # FIXME except Exceptio
                    error_msg = ('Problem fetching data within '
                                 f'the transactions table: {str(e)}')
                    logger.exception(error_msg)

            is_last_page = not test_for_presence(driver, By.XPATH,
                                                 next_button_path, 1)
            if is_last_page:
                pass
            else:
                next_page_btn = find_by_xpath(driver, f'{next_button_path}/img')

                driver.execute_script("arguments[0].scrollIntoView();",
                                      next_page_btn)
                next_page_btn.click()

    logger.debug('finish processing transactions')
    return list(reversed(transactions))


def _get_balance_and_transactions(session: BusinessMercantilSession,
                                  query_interval: QueryInterval) -> QueryResult:
    driver, frame_stk, logger = session._get()
    frame_stk.switch_to_default(driver)
    # logger.debug('move to account')
    # _moveToTabAndEnterState(session)

    logger.info('getting balance')
    session.report_all(ConsultStatus())
    try:
        balances = _get_balance(session)
    except Exception:  # FIXME
        logger.error("Coudn't retrieve account balance.")
        balances = None

    logger.debug('getting movements')
    try:
        transactions = _get_transactions(session, query_interval)
    except Exception as e:  # FIXME except Exception
        transactions = []
        error_msg = f'Exception ocurred while retrieving transactions: {str(e)}'
        logger.exception(error_msg)

    return (balances, transactions)


###############################################################################

# Report Placeholders for testing Status Update
def get_reports(
    transfers: List[Transfer]
) -> Tuple[Callable[[List[str], Status], None], Callable[[Status], None]]:

    def report(t_id: List[str], status: Status) -> None:
        print(f'reporting transfer {t_id}, status msg: {status}')

    def report_all(status: Status) -> None:
        report(list(map(lambda t: t.id, transfers)), status)

    return report, report_all

if __name__ == '__main__':
    pass
    pw = os.environ['CRED_ENCR_PW4']
    creds = Credentials.from_YAML_file(
      'credentials/credentials_business_mercantil.yaml')

    # This is only for testing the main
    from ...otp_interaction import check_ack, get_get_otp
    from ..testing_recipients import (alex_mercantil,
                                      papa_daniel_bdv,
                                      wrong_mer)

    sbt1 = Transfer(recipient=alex_mercantil,
                    amount='1',
                    concept='Prueba Novateva 2',
                    id='sbt1-MER->MER')

    acc_id = 105
    bank = 'BUSINESS_MERCANTIL'
    device_token = ('ds8v2OpTRKaiQVNpcXKYNU:APA91bE0L'
                    'x5O1HmymFCPTHmOxpS1W7PYiUgIKcv7kqVV'
                    '4_-ivPfrcSRN4iJ8UloF4GyaBtxKmy9OGMt'
                    'yyVqx1R34ulNFm6HHAqCNg8BBDAR0CLSOn'
                    'WlDirXYHu12_MzK3WzcdtckINBx')

    obt1 = Transfer(recipient=papa_daniel_bdv,
                    amount='1',
                    concept='Prueba Novateva Transferencia exitosa 2',
                    id='obt1-MER->BDV')

    wt1 = Transfer(recipient=wrong_mer,
                   amount='1',
                   concept='Prueba Novateva transferencia erronea$$$.',
                   id='wt1->MER->Void')

    def _get_otp() -> str:
        """Manually recieve OTP."""
        otp = input('Please insert OTP: ')
        return otp

    transfers_all = [wt1]
    transfers_r: List[TransferResult] = [TransferNotStarted('mer1'),
                                         TransferNotStarted('wrong1'),
                                         TransferNotStarted('bdv1')]

    get_otp = get_get_otp(acc_id, bank, device_token, None,
                          get_logger(f'BUSS-MER-{acc_id}', logging.DEBUG))
    ack_checked = check_ack(acc_id, bank, device_token, None, None)

    (report, report_all) = get_reports(transfers_all)

    ack_checked = True
    if (ack_checked):
        with BusinessMercantilSession(creds, get_otp, report, report_all) as session:
            _SBT_register_recipient(session, wrong_mer, wt1)
            # _OBT_transfer(session, obt1)
            # time.sleep(10)  # For testing purposes only.
