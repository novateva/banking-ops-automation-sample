"""BankSession base class."""
import abc
from typing import List, Callable, Tuple

from ..query import QueryInterval, Transaction
from ..transfer import TransferResult
from ..credentials import Credentials
from ..transfer import Transfer
from ..status import Status


class BankSession(abc.ABC):

    @abc.abstractmethod
    def __init__(self, credentials: Credentials, get_otp: Callable[[], str],
                 report: Callable[[List[str], Status], None],
                 report_all: Callable[[Status], None]):
        raise NotImplementedError

    @abc.abstractmethod
    def __enter__(self):
        raise NotImplementedError

    @abc.abstractmethod
    def __exit__(self, type, value, traceback):
        raise NotImplementedError

    @abc.abstractmethod
    def open(self) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def close(self):
        raise NotImplementedError

    @abc.abstractmethod
    def get_balance_and_transactions(
        self, query_interval: QueryInterval
    ) -> Tuple[Tuple[str, str, str], List[Transaction]]:
        """Get balance and transactions."""
        raise NotImplementedError

    @abc.abstractmethod
    def make_same_bank_transfer(self, transfers: List[Transfer],
                                transfer_results: List[TransferResult]) -> None:
        """Make a list of transfers."""
        raise NotImplementedError

    @abc.abstractmethod
    def make_other_bank_transfer(
            self, transfers: List[Transfer],
            transfer_results: List[TransferResult]) -> None:
        """Make a list of transfers."""
        raise NotImplementedError

    @abc.abstractmethod
    def make_mobile_transfer(self, transfers: List[Transfer],
                             transfer_results: List[TransferResult]) -> None:
        """Make a list of transfers."""
        raise NotImplementedError
