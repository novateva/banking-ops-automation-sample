from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.chrome.webdriver import WebDriver as Chrome
from selenium.webdriver.common.by import By  # type: ignore
from selenium.webdriver.support.ui import WebDriverWait  # type: ignore
from selenium.webdriver.remote.webdriver import WebElement
from selenium.webdriver.support import (  # type: ignore
    expected_conditions as EC)

import os

# Fixed Conflict

def get_driver():
    chrome_options = ChromeOptions()

    # if in posix os, we can check if the system has no display, so it is
    # automatically run headless.
    if os.name == 'posix':
        headless = os.environ.get('DISPLAY') is None or os.environ.get(
            'HEADLESS') == '1'
    else:
        headless = os.environ.get('HEADLESS') == '1'

    if headless:
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('window-size=1366x768')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-dev-shm-usage')
    chrome_options.add_argument('--start-maximized')
    return Chrome(options=chrome_options)


def test_for_presence(driver, by, location: str, timeout) -> bool:
    try:
        WebDriverWait(driver, timeout).until(
            EC.presence_of_element_located((by, location)))
        return True
    except Exception:
        return False


def test_for_visibility(driver, element: WebElement, timeout) -> bool:
    try:
        WebDriverWait(driver, timeout).until(EC.visibility_of(element))
        return True
    except Exception:
        return False


def test_if_clickable(driver, by, location: str, timeout) -> bool:
    try:
        WebDriverWait(driver,
                      timeout).until(EC.element_to_be_clickable((by, location)))
        return True
    except Exception:
        return False


def test_for_alert(driver, timeout) -> bool:
    try:
        WebDriverWait(driver, timeout).until(EC.alert_is_present())
        return True
    except Exception:
        return False


def wait_for_presence(driver, by, location: str, timeout=50) -> WebElement:
    return WebDriverWait(driver, timeout).until(
        EC.presence_of_element_located((by, location)))


def wait_for_visibility(driver, element: WebElement, timeout=50) -> WebElement:
    return WebDriverWait(driver, timeout).until(EC.visibility_of(element))


def wait_if_clickable(driver, by, location: str, timeout=50) -> WebElement:
    return WebDriverWait(driver, timeout).until(
        EC.element_to_be_clickable((by, location)))


def wait_for_invisibility(driver, by, location: str, timeout=50) -> WebElement:
    return WebDriverWait(driver, timeout).until(
        EC.invisibility_of_element_located((by, location)))


def test_for_xpath(driver, elem_id: str, timeout=1) -> bool:
    return test_for_presence(driver, By.XPATH, elem_id, timeout)


def test_for_id(driver, elem_id: str, timeout=1) -> bool:
    return test_for_presence(driver, By.ID, elem_id, timeout)


def find_by_name(driver, elem_id: str, timeout=1) -> WebElement:
    return wait_for_presence(driver, By.NAME, elem_id, timeout)


def find_by_xpath(driver, elem_id: str, timeout=1) -> WebElement:
    return wait_for_presence(driver, By.XPATH, elem_id, timeout)


def find_by_id(driver, elem_id: str, timeout=1) -> WebElement:
    return wait_for_presence(driver, By.ID, elem_id, timeout)


def wait_for_name(driver, elem_id: str, timeout=50) -> WebElement:
    return wait_for_presence(driver, By.NAME, elem_id, timeout)


def wait_for_xpath(driver, elem_id: str, timeout=50) -> WebElement:
    return wait_for_presence(driver, By.XPATH, elem_id, timeout)


def wait_for_id(driver, elem_id: str, timeout=50) -> WebElement:
    return wait_for_presence(driver, By.ID, elem_id, timeout)


def wait_for_case(driver, byif, locif, byelse, locelse, timeout=50) -> bool:
    for _i in range(timeout // 2):
        if test_for_presence(driver, byif, locif, 1):
            return True
        if test_for_presence(driver, byelse, locelse, 1):
            return False
    raise Exception('wait for case timeout')


def wait_visible_xpath(driver, elem_id: str, timeout=50) -> WebElement:
    element = wait_for_xpath(driver, elem_id, timeout)
    return wait_for_visibility(driver, element, timeout)


def wait_clickable_xpath(driver, elem_id: str, timeout=50) -> WebElement:
    return wait_if_clickable(driver, By.XPATH, elem_id, timeout)


def wait_invisibility_xpath(driver, elem_id: str, timeout=50) -> WebElement:
    return wait_for_invisibility(driver, By.XPATH, elem_id, timeout)
