from typing import List, Tuple, Any

from .selenium_utils import (wait_for_presence)


class FrameStack:
    frame_stack: List[Tuple[Any, str]] = []

    def switch_to(self, driver, by, location):
        self.frame_stack.append((by, location))
        driver.switch_to.frame(wait_for_presence(driver, by, location, 10))

    def switch_to_default(self, driver):
        self.frame_stack.clear()
        driver.switch_to.default_content()

    def restore(self, driver):
        driver.switch_to.default_content()
        for by, loc in self.frame_stack:
            driver.switch_to.frame(wait_for_presence(driver, by, loc, 10))
