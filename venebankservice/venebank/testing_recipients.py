from .transfer import Recipient, IdType, DestBank

test_mercantil = Recipient('Descripcion', 'Nombre Apellido',
                           IdType.V, '00000000', DestBank.B0102,
                           '00000000000000000000')

sara_mercantil = Recipient('sara mercantil', 'Sarait Hernandez', IdType.V,
                           '23722515', DestBank.B0105, '01050672730672233436')

wrong_sara_mercantil = Recipient('sara mercantil', 'Sarait Hernandez',
                                 IdType.V, '2372215', DestBank.B0105,
                                 '0105067230672233436')

augusto_mercantil = Recipient('Augusto G Mercantil', 'Augusto Hidalgo',
                              IdType.V, '24042438', DestBank.B0105,
                              '01050614040614304903')

papa_banesco = Recipient('Augusto H Ch Banesco', 'Augusto Hidalgo', IdType.V,
                         '3552294', DestBank.B0134, '01340329543291018644')

wrong_papa_banesco = Recipient('Augusto H Ch Banesco', 'Augusto Hidalgo',
                               IdType.V, '352294', DestBank.B0134,
                               '01340329543291018644')

augusto_pago_movil_mercantil = Recipient('augusto pagomovil mercantil',
                                         'Augusto Hidalgo', IdType.V,
                                         '24042438', DestBank.B0105, '1506116',
                                         '0414')

sara_pago_movil_mercantil = Recipient('sara pagomovil mercantil',
                                      'Sarait Hernandez', IdType.V, '23722515',
                                      DestBank.B0105, '7264112', '0424')

wrong_sara_pago_movil_mercantil = Recipient('sara pagomovil mercantil',
                                            'Sarait Hernandez', IdType.V,
                                            '23722515', DestBank.B0105,
                                            '726112', '0424')

marina_pago_movil_banesco = Recipient('marina pagomovil banesco',
                                      'Marina Bravo', IdType.V, '4835216',
                                      DestBank.B0134, '1053990', '0414')

silvia_provincial = Recipient('Silvia Provincial', 'Silvia San Andres',
                              IdType.V, '14485231', DestBank.B0108,
                              '01080001330100186166')

silvia_provincial_wrong = Recipient('Silvia Provincial', 'Silvia San Andres',
                              IdType.V, '14485231', DestBank.B0108,
                              '01080001330100186165')

jesus_provincial = Recipient('Jesus Provincial', 'Jesus Hernandez',
                             IdType.V, '19894998', DestBank.B0108,
                             '01080067670100331727')

alex_mercantil = Recipient('Alex Provincial', 'Alexander Romero',
                           IdType.V, '24440508', DestBank.B0105,
                           '01050614040614314976')

alex_mercantil_wrong = Recipient('Alex Provincial', 'Alexander Romero',
                           IdType.V, 'aa', DestBank.B0105,
                           '01050614040614314976')

alex_mercantil_wrong2 = Recipient('Alex Provincial', 'Alexander Romero',
                           IdType.V, '24440508', DestBank.B0105,
                           '0105061404061431497')