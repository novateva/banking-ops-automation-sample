"""Definitions of possible errors useful for selenium use."""

from typing import Optional

################################################################################
# OTP
################################################################################


class OTPTimeoutError(Exception):
    """Didn't receive OTP before timeout."""

    def msg(self) -> str:
        """Message to show to user."""
        return 'No se recibió la clave especial a tiempo.'


class OTPOtherError(Exception):
    """Other possible error."""

    def msg(self) -> str:
        """Message to show to user."""
        return str(self)


class OTPIncorrectError(Exception):
    """OTP was incorrect."""

    def msg(self) -> str:
        """Message to show to user."""
        return 'La clave especial fue rechazada.'


################################################################################
# Other (?)
################################################################################

class BankSpecificError(Exception):
    """General specific (what?) error."""

    message: str
    by: str
    loc: str

    def __init__(self, by: str, loc: str, message: str):
        """General specific (what?) error."""
        super().__init__(message)
        self.message = message
        self.by = by
        self.loc = loc

    def msg(self) -> str:
        """Message to show to user."""
        return self.message


class LoginError(Exception):
    """Couldn't login error."""

    screenshot: Optional[str]

    def __init__(self, screenshot: Optional[str], *args):
        """Couldn't login error.

        Args:
            screenshot (str)
        """
        super().__init__(args)
        self.screenshot = screenshot

    def msg(self) -> str:
        """Message to show to user."""
        return str(self)


class DriverError(Exception):
    """Couldn't open driver error."""

    def msg(self) -> str:
        """Message to show to user."""
        return 'No se pudo abrir el driver de selenium.'
