# Consultas y Manejo de Cuentas Bancarias - Mesada

<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->

<!-- code_chunk_output -->

- [Consultas y Manejo de Cuentas Bancarias - Mesada](#consultas-y-manejo-de-cuentas-bancarias-mesada)
  - [Entorno](#entorno)
    - [python](#python)
    - [Sistema](#sistema)
    - [Desarrollo](#desarrollo)
  - [Módulos `venebank`](#módulos-venebank)
    - [`venebank.credentials`](#venebankcredentials)
    - [`venebank.query`](#venebankquery)
    - [`venebank.transfer`](#venebanktransfer)
    - [`venebank.utils`](#venebankutils)
    - [`venebank.exceptions`](#venebankexceptions)
    - [`venebank.bank_account`](#venebankbank_account)
  - [Módulos `venebank.selenium`](#módulos-venebankselenium)
    - [`venebank.selenium.selenium_utils`](#venebankseleniumselenium_utils)
    - [`venebank.selenium.frame_stack`](#venebankseleniumframe_stack)
    - [`venebank.selenium.logger`](#venebankseleniumlogger)
    - [`venebank.selenium.bank_session`](#venebankseleniumbank_session)
    - [`venebank.selenium.banesco` y `venebank.selenium.mercantil`](#venebankseleniumbanesco-y-venebankseleniummercantil)
  - [Notas](#notas)
    - [Resultados de transferencias](#resultados-de-transferencias)
    - [Estado actual](#estado-actual)
      - [Manajo de errores](#manajo-de-errores)
      - [Capturas de pantalla](#capturas-de-pantalla)
      - [Logging](#logging)

<!-- /code_chunk_output -->

## Entorno
### python
Probado con python 3.8, debería servir con 3.5+ (?)

Librerías incluidas en el archivo requirements.txt
```
$ pip install -r requirements.txt
```
### Sistema
- Un explorador web, por ahora necesariamente Chrome, y el driver para la versión correspondiente, ver [drivers](https://selenium-python.readthedocs.io/installation.html#drivers).
- xvfb: no necesario, pero se puede usar para probar el funcionamiento dentro de xvfb, como sería ejecutado por el servicio.

### Desarrollo
Herramientas y estilo utilizado en el módulo, para mantener consistencia.

- Análisis de tipos: `pyright` o `mypy`, análisis estático de anotaciones de tipos. Ayuda a encontrar muchos bugs fácilmente, refactorizar, hacer el código más legible, y mantener la cordura. `mypy` es el más standard pero después de probar ambos, `pyright` me gustó más: parece un poco más inteligente y genera stubs automáticamente para librerías sin anotaciones de tipos.
Este artículo me parece buena introducción: [Python Type Checking](https://realpython.com/python-type-checking).
- Linting: `pydocstyle` y `flake8`.
- Formatter: `yapf`.

Todo con la configuración por defecto excepto por la longitud de líneas, cambiada a 80 (definido en `setup.cfg`).

En `vscode`/`Code - OSS` basta con instalar las extensiones `ms-python.python` y `ms-pyright.pyright` y tener lo siguiente en la configuración:

```json
    "python.linting.pydocstyleEnabled": true,
    "python.linting.flake8Enabled": true,

    "python.formatting.provider": "yapf",
```


## Módulos `venebank`

### `venebank.credentials`
Define la clase `Credentials` así como funciones para construir `Credentials` desde archivos YAML, y funcionalidades para encriptar/desencriptar éstos, útil para el desarrollo. Ver `/credentials/credentials_example.yaml` para un ejemplo del formato del YAML.

### `venebank.query`
Define clases para representar los datos de las consultas bancarias.

### `venebank.transfer`
Define clases para representar los datos de las transferencias bancarias.

### `venebank.utils`
Pequeñas utilidades variadas.

### `venebank.exceptions`
Representa los distintos tipos de errores.

### `venebank.bank_account`
Define BankAccount, una interfaz bastante delgada sobre BankSession, más fácil de usar a nivel del servicio. Ver como se usan las clases `BankSession`.


## Módulos `venebank.selenium`
La mayoría del desarrollo para implementar un nuevo banco ocurre en este paquete.

### `venebank.selenium.selenium_utils`
Define una capa delgada sobre la interfaz de selenium:
- Las funciones de `find_by_*`, tienen un timeout de 1 segundo
- Define las funciones `test_for_*`
- Define la función `wait_for_case`, muy útil cuando la página pueda operar de distintas formas dependiendo de _cualquier cosa_.

Se pueden ver ejemplos de uso en los módules de banesco o mercantil.

### `venebank.selenium.frame_stack`
Para moverse con selenium, hace falta saltar explícitamente entre los frames del html (que yo sepa). Esto es fastidioso en sí pero se vuelve problemático para implementar funciones que necesiten ver otra parte de la página y volver al frame anterior (como las funciones `keep_alive`). En este módulo se define una clase `FrameStack` que mantiene la pila de frames actual.

Notar que se deben usar las funciones de switch de la clase para que ésta actualice la pila.

Se pueden ver ejemplos de uso en los módulos de banesco o mercantil, en particular en las funciones `keep_alive`.

### `venebank.selenium.logger`
Funcionalidades de logging.

### `venebank.selenium.bank_session`
Clase abstracta `BankSession` que define la interfaz de una sesión de un banco. Se debe heredar y concretar esta clase en cada uno de los bancos que se quiera implementar.

La clase `BankSession` está pensada para ser usada como un context manager:
```python
with BancoTucupitaSession(credentials) as session:
    # do all the things
```
Esto permite manejar, en teoría, los errores de una forma mucho más limplia (aseguramos siempre llamar `__exit__`, que puede cerrar la sesión y manejar errores esperados), además de permitir realizar operaciones en secuencia de forma limpia (ver `venebank.bank_account`).

### `venebank.selenium.banesco` y `venebank.selenium.mercantil`
Ejemplos de subclases de `BankSession`.


## Notas
### Resultados de transferencias
Notar que las funciones de transferencias reciben una lista de resultados que actua como _place holder_ para los resultados finales, en vez de simplemente retornar el resultado. El sentido de esto es aprovechar la mutabilidad de las listas y no depender de que la función termine exitosamente. Simplificando que los resultados parciales no se deben perder.

Además permite ir actualizando el estado de cada una de las transferencias, actualmente los posibles estados (definidos en `venebank.transfer`) son:

- `TransferNotStarted`
- `TransferNotCompleted` (una que ya se llenaron los datos y se continuó pero no se ha confirmado como erronea o exitosa)
- `TransferSuccess`
- `TransferError`

### Estado actual
El módulo ha pasó por un par de transformaciones considerables, así que se percibe cierto espíritú de Frankenstein.

#### Manajo de errores
El manejo de errores puede mejorar:

- Los módulos de banesco y mercantil tienen varios `except Exception`, estos pueden ser necesarios como último recurso pero no en tal cantidad.
- En los módulos de banesco y mercantil, los errores en el inicio de sesión no son manejadas _de forma ideal_.
- Todos los errores significativos para nosotros deberían estar definidos en `venebank.exception` (o quizás `venebank.selenium.exception`). Esto se empezó a hacer pero puede mejorarse.
- Cerrar sesión en caso de error insalvable debe ser revisado.

#### Capturas de pantalla
Frankenstein vibes

#### Logging
Lots of Frankenstein vibes

#### Desarrollo/depuración
El módulo (y el servicio) fue creciendo y montándole más capas a la funcionalidad de selenium, que son ideales (bueno, útiles) para su uso pero no del todo para el desarrollo de estas funcionalidades.

En los `__main__` de los módulos de banesco y mercantil pueden ver como ejecutaba esos módulos para depurarlos y probarlos. Utilizando el debugger podía poner breakpoints para ver lo que estaba pasando.

Aquí la config que usa vscode por si les es útil.

```json
{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            "env": {
                "CRED_ENCR_PW": "secretito",
                "DISPLAY": ":420",
            },
            "name": "mercantil",
            "type": "python",
            "request": "launch",
            "module": "venebankservice.venebank.selenium.mercantil",
            "console": "integratedTerminal"
        },
        {
            "env": {
                "CRED_ENCR_PW": "secretito",
                "DISPLAY": ":420",
            },
            "name": "banesco",
            "type": "python",
            "request": "launch",
            "module": "venebankservice.venebank.selenium.banesco",
            "console": "integratedTerminal"
        },
    ]
}
```

Para desarrollar nuevas funcionalidades pueden llamar directamente a las funciones de `__enter__` desde un intérprete y jurungar desde ahí. Si encuentran una herramienta o flujo mejor, compártanlo :)


## Tareas principales (además de desarrollar otros bancos)
### Cosas que arreglar/mejorar
Relacionado a la sección anterior:

- Mejorar flujo de errores, en especial en el inicio de sesión.
- Mejorar y completar definiciones de errores esperados.
- Hacer más preciso los estados de los resultados de las transferencias.
- Coordura en el logger.
- Coordura en los screenshots.

### Posibles features

- Alguna especie de señalización que permita al módulo dar feedback externo del estado actual de la operación sin necesidad de terminar
  - Util para dar feedback en el frontend
  - También para mantener el estado de las transferencias fuera del módulo. Si se va el internet o la luz, no importa qué tan bonita fue la solución de las listas para los resultados, se perdieron.