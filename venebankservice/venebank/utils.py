from datetime import datetime, timezone, timedelta


def get_VE_time() -> str:
    """Get current time in Venezuela."""
    # Had to change format because of Windows
    tz = timezone(timedelta(hours=-4))
    return datetime.now(tz).strftime("%Y-%m-%d %H-%M-%S")


def hide(s: str):
    """Hide important information."""
    return '**' + s[-4:]


def correct_amount_format(amount: str) -> bool:
    """Tell if the given amount conforms to format spected by venebank.

    Should have exactly 2 decimal positions, separated by a comma.

    Args:
        amount (str)

    Returns:
        bool: True iff the string is in correct format
    """
    if amount.count(',') != 1:
        return False
    integer, decimal = amount.split(',')
    try:
        ret = len(decimal) == 2
        integer, decimal = int(integer), int(decimal)
        return ret and integer > 0
    except ValueError:
        return False
