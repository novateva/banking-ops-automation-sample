"""BankAccount class, to manage bank accounts.

Very thin layer over BankSession.
"""
from enum import Enum, auto
from typing import List, Tuple, Optional, Union, Callable, Dict, Type

from .credentials import Credentials
from .transfer import (Transfer, TransferNotStarted, TransferResult)
from .selenium import (BankSession, BusinessMercantilSession, MercantilSession,
                       BanescoSession, ProvincialJSession, BDVSession)
from .query import QueryInterval, Transaction
from .status import Status


class Bank(Enum):
    """Different banks for which the manager is implemented."""

    MERCANTIL_PERSONAS = auto()
    BANESCO_PERSONAS = auto()
    BDV_PERSONAS = auto()
    PROVINCIAL_JURIDICO = auto()
    MERCANTIL_JURIDICO = auto()


QueryResult = Tuple[Optional[Tuple[str, str, str]], Optional[List[Transaction]]]


def get_report_all(
        transfers: List[Transfer],
        report: Callable[[List[str], Status],
                         None]) -> Callable[[Status], None]:

    def report_all(status: Status) -> None:
        report(list(map(lambda t: t.id, transfers)), status)

    return report_all


class BankAccount():
    bank: Bank
    credentials: Credentials

    def __init__(self,
                 bank: Bank,
                 credentials: Union[str, Credentials],
                 pw: str = None) -> None:
        """Create a BankAccount to manage the given Bank."""
        self.bank = bank

        if isinstance(credentials, Credentials):
            self.credentials = credentials
        else:
            if credentials[-5:] == '.encr':
                self.credentials = Credentials.from_encrypted_YAML_file(
                    credentials, pw)
            else:
                self.credentials = Credentials.from_YAML_file(credentials)

    def __repr__(self) -> str:
        """Representation of BankAccount."""
        return "BankAccount(bank=%r, credentials=%r)" % (self.bank,
                                                         self.credentials)

    def _get_bank_code(self) -> str:
        bank_codes: Dict[Bank, str] = {
            Bank.MERCANTIL_PERSONAS: '0105',
            Bank.MERCANTIL_JURIDICO: '0105',
            Bank.BANESCO_PERSONAS: '0134',
            Bank.PROVINCIAL_JURIDICO: '0108',
            Bank.BDV_PERSONAS: '0102'
        }
        return bank_codes[self.bank]

    def session(self, get_otp: Callable[[], str],
                report: Callable[[List[str], Status], None],
                report_all: Callable[[Status], None]) -> BankSession:
        """Open a new session, to be used as a context manager."""
        bank_to_session: Dict[Bank, Type[BankSession]] = {
            Bank.MERCANTIL_PERSONAS: MercantilSession,
            Bank.MERCANTIL_JURIDICO: BusinessMercantilSession,
            Bank.BANESCO_PERSONAS: BanescoSession,
            Bank.PROVINCIAL_JURIDICO: ProvincialJSession,
            Bank.BDV_PERSONAS: BDVSession
        }
        Session = bank_to_session.get(self.bank)
        if Session is None:
            raise ValueError(f'{self.bank!r} is not a recognized bank.')

        return Session(self.credentials, get_otp, report, report_all)

    def get_balance_and_transactions(
            self,
            get_otp: Callable[[], str],
            report: Callable[[List[str], Status], None],
            query_interval: QueryInterval = QueryInterval.LAST_TWO_DAYS,
            logging_level: int = 51  # TODO
    ) -> QueryResult:
        """Get the balance and last transactions."""
        try:
            report_all = get_report_all([], report)
            with self.session(get_otp, report, report_all) as session:
                result = session.get_balance_and_transactions(query_interval)
            return result
        except Exception:
            return (None, None)

    def _partition_transfers(
        self, transfers: List[Transfer]
    ) -> Tuple[List[Transfer], List[Transfer], List[Transfer]]:
        """Partition transfers in same-bank, other-bank, and mobile.

        Args:
            transfers (List[Transfer])

        Returns:
            Tuple[List[Transfer], List[Transfer], List[Transfer]]
        """
        sbt, obt, mt = [], [], []
        for transfer in transfers:
            recip = transfer.recipient
            if recip.number_pref:
                mt.append(transfer)
                continue
            code = self._get_bank_code()
            if recip.number[:4] == code:
                sbt.append(transfer)
            else:
                obt.append(transfer)
        return sbt, obt, mt

    def _get_results_holder(self,
                            transfers: List[Transfer]) -> List[TransferResult]:
        return [TransferNotStarted(t.id) for t in transfers]

    def get_transfers_and_movements(
        self,
        transfers: List[Transfer],
        get_otp: Callable[[], str],
        report: Callable[[List[str], Status], None],
    ) -> Tuple[List[TransferResult], QueryResult]:
        """Make a list of transfer and then do a query.

        This will ALWAYS return a list of results, even when we can't log in
        (ok maybe if the device is turned off it won't return anything)

        Args:
            transfers (List[Transfer])
            get_otp (Callable[[], str]): Function used to get an otp when sent

        Returns:
            Tuple[List[TransferResult], QueryResult]
        """
        sbt, obt, mt = self._partition_transfers(transfers)
        trans_res: List[List[TransferResult]] = [
            self._get_results_holder(sbt),
            self._get_results_holder(obt),
            self._get_results_holder(mt),
        ]
        try:
            report_all = get_report_all(transfers, report)
            with self.session(get_otp, report, report_all) as session:
                session: BankSession
                print(f'obt: {obt}')
                if obt:
                    session.make_other_bank_transfer(obt, trans_res[1])
                print(f'mt: {mt}')
                if mt:
                    session.make_mobile_transfer(mt, trans_res[2])
                print(f'sbt: {sbt}')
                if sbt:
                    session.make_same_bank_transfer(sbt, trans_res[0])

                (bal, movs) = session.get_balance_and_transactions(
                    QueryInterval.LAST_TWO_DAYS)
                res = trans_res[0] + trans_res[1] + trans_res[2]
                return res, (bal, movs)
        except Exception:
            res = trans_res[0] + trans_res[1] + trans_res[2]
            return res, (None, None)


if __name__ == '__main__':
    # For use only with Alex or Daniel's saved credentials and settings
    # DELETE EVERYTHING LATER
    pw = 'danielvarela'
    # Como esto funcionaba sin definir pw? borrar al terminar xD
    # Si la definia pero no lo pushee lol
    creds_prov = Credentials.from_encrypted_YAML_file(
        'credentials/credentials_prov.yaml.encr', pw)
    creds_bdv = Credentials.from_encrypted_YAML_file(
        'credentials/credentials_alex_BDV.yaml.encr', pw)
    bank_account = BankAccount(Bank.PROVINCIAL_JURIDICO, creds_prov)

    # This is only for testing the main
    from ..otp_interaction import check_ack, get_get_otp

    acc_id = 103
    bank = "PROVINCIAL"
    device_token = ("dkqRJxEaS9egu4NrSTenQh:APA91bEDMJxZ_C74VNkHlQdkGocfi4Oj"
                    "KDfYoYStToEkSl-5posojPcHqwlRk6qaWCjqiSR53cbJwQumvKaQIXb"
                    "nWPgmr1TU-P2UQxDMetI1Ualn-dUib08wroITkk72-fvNuws1ph9v")
    get_otp = get_get_otp(acc_id, bank, device_token, None, None)

    # (bals, trans) = bank_account.get_balance_and_transactions(get_otp)
    # print(bals)
    # print(trans)
    # bank_account = BankAccount(Bank.BDV_PERSONAS, creds_bdv)
    # (bals, trans) = bank_account.get_balance_and_transactions(get_otp)
    # print(bals)
    # print(trans)

    from .testing_recipients import (silvia_provincial, jesus_provincial,
                                     alex_mercantil, augusto_mercantil)
    from .testing_recipients import (alex_mercantil_wrong,
                                     alex_mercantil_wrong2,
                                     silvia_provincial_wrong)

    sbt = Transfer(amount='123,69',
                   recipient=silvia_provincial,
                   concept='prueba',
                   id='prov1')
    sbt2 = Transfer(amount='124,62',
                    recipient=jesus_provincial,
                    concept='prueba',
                    id='prov2')

    sbt_wrong1 = Transfer(amount='aa',
                          recipient=jesus_provincial,
                          concept='prueba',
                          id='prov2w')
    sbt_wrong2 = Transfer(amount='123',
                          recipient=silvia_provincial_wrong,
                          concept='prueba',
                          id='prov2w2')

    obt = Transfer(amount='123,69',
                   recipient=alex_mercantil,
                   concept='prueba',
                   id='provobt1')
    obt2 = Transfer(amount='123,69',
                    recipient=augusto_mercantil,
                    concept='prueba',
                    id='provobt2')

    obt_wrong1 = Transfer(amount='120',
                          recipient=alex_mercantil_wrong,
                          concept='prueba',
                          id='provobt2w')
    obt_wrong2 = Transfer(amount='120',
                          recipient=alex_mercantil_wrong2,
                          concept='prueba',
                          id='provobt2w2')

    # All succesful
    transfers_obt = [sbt]
    # All errors
    # transfers_obt = [obt_wrong1, obt_wrong2, sbt_wrong1, sbt_wrong2]
    # Mixed Detodito
    # transfers_obt = [sbt_wrong1, sbt, sbt_wrong2, obt, obt_wrong1, sbt2, obt2]

    ack_checked = check_ack(acc_id, bank, device_token, None, None)

    def report(t_id: List[str], status: Status) -> None:
        print(f'reporting transfer {t_id}, status msg: {status}')

    if (ack_checked):
        (transr, (bals, trans)) = bank_account.get_transfers_and_movements(
            transfers_obt, get_otp, report)
        print(transr)
        print(bals)
        print(trans)

    # py -m venebankservice.venebank.bank_account
