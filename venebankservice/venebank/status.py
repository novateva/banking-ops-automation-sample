"""Classes to represent Status to report during execution."""
from dataclasses import dataclass
import abc


class Status(abc.ABC):
    """Base Transfer Status Class."""

    msg: str

    def __init__(self):
        """Build Base Transfer Status Class."""
        raise TypeError("Can't instantiate abstract class Status")

    def __str__(self):
        """Return string representation of status."""
        return self.msg


###############################################################################
# Multiple Transfers Status
###############################################################################


@dataclass
class ACKStatus(Status):
    """ACK Status of a list of transfers."""

    attempt: int
    who: str  # maestro or rapserry(nombre)
    msg: str = 'Estableciendo conexión con el dispotivo'

    def __str__(self):
        """Override string representation."""
        return f'{self.msg} desde {self.who}. Intento {self.attempt}.'


@dataclass
class ACKSuccess(Status):
    """ACK Status of a list of transfers."""

    who: str  # maestro or rapserry(nombre)
    msg: str = 'Conexión con dispositivo exitosa'

    def __str__(self):
        """Override string representation."""
        return f'{self.msg} desde {self.who}.'


@dataclass
class LoginStatus(Status):
    """Login Status of a list of transfers."""

    msg: str = 'Iniciando sesión.'


@dataclass
class LogoutStatus(Status):
    """Logout Status of a list of transfers."""

    msg: str = 'Cerrando sesión.'


@dataclass
class ConsultStatus(Status):
    """Consult Status of a list of transfers.

    Both balance and movements after making transfers
    """

    msg: str = 'Haciendo consulta de balance y movimientos.'


@dataclass
class WaitingTransfersStatus(Status):
    """WaitingTransfer Status of a list of transfers.

    This for the case after login, and while it's making transfers
    one at a time
    """

    msg: str = 'Esperando por otras transferencias.'


# Mercantil Empresas
@dataclass
class TokenLoginStatus(Status):
    """Token Login Status of a list of transfers."""

    msg: str = 'Esperando el Token para inicio de sesión.'


###############################################################################
# Single Transfers Status
###############################################################################


# Caso para Banesco y BDV
@dataclass
class RegisterClientStatus(Status):
    """RegisterClient Status of a single of transfer."""

    msg: str = 'Registrando al beneficiario para transferir.'


@dataclass
class OTPStatus(Status):
    """OTP Status of a single transfer."""

    msg: str = 'Esperando OTP del dispositivo.'


# Mercantil Empresas
@dataclass
class TokenStatus(Status):
    """Token Status of a single transfer."""

    msg: str = 'Esperando Token del dispositivo.'


@dataclass
class TransferStartStatus(Status):
    """TransferStart Status of a single transfer."""

    msg: str = 'Comenzando transferencia.'


# This is the last status to show on a transfer
@dataclass
class TransferDoneStatus(Status):
    """TransferDone Status of a single transfer."""

    # TODO put TransferResult or something like that here?
    msg: str = 'Transferencia terminada.'
