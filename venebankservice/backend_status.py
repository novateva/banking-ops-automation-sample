"""Logic for updating and getting backend's status for transfers."""
import requests

from typing import List, Union, Optional
from logging import Logger
import time

from .venebank.status import Status


# TODO this also returns the cancelled/progress status
def report_status(transfer_ids: List[str],
                  status: Status,
                  logger: Optional[Logger] = None) -> list:
    for tries in range(5):
        try:
            if logger:
                logger.debug(f'reporting status... {transfer_ids}: {status}')
            status_url = ('http://mesada-webapp-prod.eba-wvhnim3u.us-east-1.'
                          'elasticbeanstalk.com/movements/transfer/status')
            body = {
                'transfers': [{
                    'id': t_id,
                    'status': str(status)
                } for t_id in transfer_ids]
            }
            if logger:
                logger.debug(f'body: {body}')

            r = requests.post(status_url, json=body)
            if logger:
                logger.debug(f'status returned: {r}')

            return r.json()['status']
        except Exception:
            if logger:
                logger.exception('error in report status')
            if tries == 4:
                raise
            time.sleep(5)
    raise ConnectionError()


def get_status(transfer_ids: List[str]) -> Union[str, bytes, None]:
    status_url = ('http://mesada-webapp-prod.eba-wvhnim3u.us-east-1.'
                  'elasticbeanstalk.com/movements/transfer/status')
    return requests.get(status_url).request.body
