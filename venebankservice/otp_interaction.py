"""Utilities for getting the otp from devices.

You want to use check_ack and get_get_otp
"""

import requests
import json
import random
import logging
from typing import Optional, Callable
from datetime import datetime

from .venebank import (OTPTimeoutError, OTPOtherError)
from .sqs_handler import SQSHandler

SERVER_TOKEN = ('AAAAwiwlzfY:APA91bED7cmX3kR2R3VAtUfJGzczoSvgJ9r20nMhUFrmmzCvo'
                'K1SWlzPVApAO04Sb4riiFTgYRZunVOmgGNgDlhBkXOJTeYTNFqXoBmYMmtG3J'
                'PNSXTP2r3XkczDablE9vvNwPNiHvVQ')


def _maybelogger_info(logger, msg):
    if logger is not None:
        logger.info(msg)
    else:
        print(msg)


def _maybelogger_warning(logger, msg):
    if logger is not None:
        logger.warning(msg)
    else:
        print(msg)


def _maybelogger_error(logger, msg):
    if logger is not None:
        logger.error(msg)
    else:
        print(msg)


def _send_push(body: dict) -> requests.Response:
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'key=' + SERVER_TOKEN,
    }
    response = requests.post("https://fcm.googleapis.com/fcm/send",
                             headers=headers,
                             data=json.dumps(body))

    return response


def get_get_otp(account_id: int, bank: str, device_token: Optional[str],
                sms_hint: Optional[str],
                logger: Optional[logging.Logger]) -> Callable[[], str]:
    if bank == 'VENEZUELA':
        bank = 'BDV'
    sqs_queue_name: str = f'otp-{bank}-{account_id}'

    def get_otp():
        if device_token == 'MANUAL':
            pass
        elif device_token is None:
            _maybelogger_error(logger, 'No device_token')
            raise OTPOtherError('Se requiere clave especial y la cuenta no'
                                'tiene ningún dispositivo asociado.')
        else:
            _maybelogger_info(logger, 'Sending push asking for otp')
            send_ask_otp(sqs_queue_name, device_token, sms_hint, logger)

        otp = receive_otp(sqs_queue_name, logger)
        return otp

    return get_otp


def receive_otp(sqs_queue_name, logger: Optional[logging.Logger]) -> str:
    # FIXME better logger for module
    sqshandler = SQSHandler(sqs_queue_name, receive_timeout=60 * 4)

    def is_otp(body: dict):
        good = body.get('type') == 'otp' and body.get('otp') is not None
        if not good:
            _maybelogger_warning(
                logger, f'Received bad msg while waiting for otp: {body}')
        return good

    received_message = sqshandler.receive_message_with_predicate(is_otp)

    if received_message is None:
        _maybelogger_error(logger, 'otp timeout')
        raise OTPTimeoutError()

    otp = received_message['otp']
    _maybelogger_info(logger, f'otp received: {otp}')
    return otp


def check_ack(account_id: int, bank: str, device_token: str,
              sms_hint: Optional[str],
              logger: Optional[logging.Logger]) -> bool:
    if bank == 'VENEZUELA':
        bank = 'BDV'
    sqs_name = f'otp-{bank}-{account_id}'
    randid = str(random.randrange(100000)) + str(datetime.today().timestamp())
    if sms_hint is None:
        sms_hint = 'no sms hint'
    body = {
        'to': device_token,
        'priority': 'high',
        'data': {
            'option': '0',
            'queue_name': sqs_name,
            'id': randid,
            'bank_name': bank,
            'sms_hint': sms_hint
        },
    }
    response = _send_push(body)
    # _maybelogger_info(logger, f'ack push body:{body}\nresponse:{response}')

    sqshandler = SQSHandler(sqs_name, receive_timeout=45)

    def is_my_ack(body: dict):
        return body.get('type') == 'ack' and body.get('id') == randid

    _maybelogger_info(logger, 'waiting for ack response')
    received_message = sqshandler.receive_message_with_predicate(is_my_ack)
    _maybelogger_info(logger, f'ack response received: {received_message}')

    return (received_message is not None)


def send_ask_otp(sqs_name: str, device_token: str, sms_hint: Optional[str],
                 logger: Optional[logging.Logger]) -> None:
    if sms_hint is None:
        sms_hint = 'no sms hint'
    if 'BANESCO' in sqs_name:
        option = '4'
    elif 'BUSINESS_MERCANTIL' in sqs_name:
        option = '2'
    elif 'MERCANTIL' in sqs_name:
        option = '3'
    elif 'PROVINCIAL' in sqs_name:
        option = '5'
    elif 'BDV' in sqs_name:
        option = '6'
    else:
        raise OTPOtherError('bank not recognized from sqs name')

    body = {
        'to': device_token,
        'priority': 'high',
        'data': {
            'queue_name': sqs_name,
            'option': option,  # 2, 3, 4 or 5
            'sms_hint': sms_hint
        },
    }

    response = _send_push(body)
    _maybelogger_info(logger, f'otp push body: {body}\nresponse: {response}')

    # TODO check response status
    if response.status_code != 200:
        raise OTPOtherError('No se pudo realizar el push al teléfono para'
                            'perdir la clave especial.')
