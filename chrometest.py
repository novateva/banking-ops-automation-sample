from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.chrome.webdriver import WebDriver as Chrome

chrome_options = ChromeOptions()
chrome_options.add_argument('--headless')
chrome_options.add_argument('window-size=1366x768')
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-dev-shm-usage')
chrome_options.add_argument('--start-maximized')
driver = Chrome(options=chrome_options)
driver.get("https://www.banesconline.com/mantis/WebSite/Default.aspx")

driver.get_screenshot_as_file('prueba.png')
